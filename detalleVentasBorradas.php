<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1))
    header('Location: /index.php');

include('conexion.php');
require('includes/header.php');

// SQL Query
$comando = $pdo->prepare("SELECT id, nombre FROM listausuarios");
$comando->execute();

// array data 
$user_list = $comando->fetchAll(PDO::FETCH_ASSOC);

// SQL Query
$comando = $pdo->prepare("SELECT idturno, turno FROM turnos");
$comando->execute();

// array data 
$lista_turno = $comando->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container" id="wraphome">
    <h4 id="tituloDetalleVentas">Detalle de Ventas borradas</h4>
    <form class="form-inline">
        <div class="form-group mb-2" id="selUsers">
            <label for="user_select">
                <i class="fas fa-user"></i> Usuario
            </label>
            <select class="form-control form-control-sm" id="user_select">
                <option value="-1" selected>Todos</option>
                <?php
                foreach ($user_list as $user) {
                    $user_id = $user['id'];
                    $user_name = $user['nombre'];
                    echo "<option value=\"$user_id\">$user_name</option>\r\n";
                }
                ?>
            </select>
            <label for="turno_select">
                <i class="fas fa-user"></i> Turno
            </label>
            <select class="form-control form-control-sm" id="turno_select">
                <option value="-1" selected>Todos</option>
                <?php
                foreach ($lista_turno as $turno) {
                    $turno_id = $turno['idturno'];
                    $turno_name = $turno['turno'];
                    echo "<option value=\"$turno_id\">$turno_name</option>\r\n";
                }
                ?>
            </select>
        </div>
    </form>
    <input type="text" class="contanier form-control text-center" name="rangepicker" id="calendario" />
    <button type="button" class="btn btn-danger" id="delete_range">Borrar rango</button>
    <br>
    <table id="rptDeVtas" class="display responsive nowrap table table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Fecha</th>
                <th>Vendedor</th>
                <th>Borrado</th>
                <th>Turno</th>
                <th>Número</th>
                <th>Monto</th>
            </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        var dateInputFormat = 'YYYY-MM-DD HH:mm:ss';
        var dateRender = function(url, type, full) {
            var formated = moment(url, dateInputFormat);
            return (formated.isValid() ? formated.format('dddd DD MMMM YYYY | hh:mm:ssa') : url);
        };

        // Date picker
        var date_range = $('input[name="rangepicker"]').daterangepicker({
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Rango Personalizado",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
        }, function(start, end, label) {
            table.ajax.reload();
            console.log(start, end, label);
        });

        $("#user_select").change(function() {
            table.ajax.reload();
        });
        $("#turno_select").change(function() {
            table.ajax.reload();
        });

        $('#delete_range').click(function() {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: '¿Estás seguro de borrar esta información?',
                text: "¡No podrás revertir esta acción!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, Borrarla!',
                cancelButtonText: 'No, Cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    // Aciones si 
                    let date_input_format = 'YYYY-MM-DD';
                    $.post('obtVtasTotalesBorradas.php', {
                        delete: true,
                        start_date: date_range.data('daterangepicker').startDate.format(date_input_format),
                        end_date: date_range.data('daterangepicker').endDate.format(date_input_format),
                        user_id: $('#user_select').val(),
                        turno_id: $('#turno_select').val()
                    }, function(res) {
                        console.log('bye bye peaple');
                    });
                    swalWithBootstrapButtons.fire(
                        '¡Eliminados!',
                        'Tus registros han sido eliminados.',
                        'success'
                    ).then((result) => {
                        table.ajax.reload();
                    });
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    swalWithBootstrapButtons.fire(
                        'Cancelado',
                        'Tus datos están seguros :)',
                        'error'
                    )
                }
            })
        });

        var table = $('#rptDeVtas').DataTable({
            fixedHeader: true,
            language: {
                "lengthMenu": "Ver _MENU_ Archivos por Página",
                "zeroRecords": "Lo sentimos, no tenemos resultados",
                "info": "Mostrando Página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin Registros para Mostrar",
                "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
                processing: "Cargando, esto podria tomar unos segundos....",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                },
                "search": "Buscar",
                buttons: {
                    colvisRestore: 'Restaurar',
                }
            },
            processing: true,
            aServerSide: true,
            dom: 'Blfrtip',
            buttons: [{
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel'
                },
                {
                    extend: 'pdf',
                    text: 'PDF'
                },
                {
                    extend: 'print',
                    text: 'Imprimir',
                    messageTop: '<h3 class="text-center">DETALLE DE VENTAS POR VENDEDOR Y FECHA</h3>',
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '12pt')
                            .prepend(
                                '<img src="https://www.mzdevocotal.com/rifaarteaga/img/logoArInforme.png" style="position:absolute; top:0; left:0; opacity:0.8; width:400px; height:250px;" />'
                            );

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                },
                {
                    extend: 'colvis',
                    text: 'Ver/Ocultar',
                    postfixButtons: ['colvisRestore']
                }
            ],
            responsive: {
                details: {
                    type: 'column'
                }
            },
            bDestroy: true,
            iDisplayLength: 10,
            order: [
                [5, "asc"]
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Todos"]
            ],
            ajax: {
                url: 'obtVtasTotalesBorradas.php',
                type: "POST",
                dataType: "json",
                error: function(e) {
                    console.log(e.responseText);
                },
                data: function(d) {
                    let date_input_format = 'YYYY-MM-DD';
                    var start_date = date_range.data('daterangepicker').startDate.format(date_input_format);
                    var end_date = date_range.data('daterangepicker').endDate.format(date_input_format);
                    var user_id = $('#user_select').val();
                    var turno_id = $('#turno_select').val();

                    console.log(start_date, end_date, user_id, turno_id);

                    d.start_date = start_date;
                    d.end_date = end_date;
                    d.user_id = user_id;
                    d.turno_id = turno_id;
                }
            },
            columns: [
                // 0 | Responsive
                {
                    className: 'control noVis notexport',
                    data: null,
                    orderable: false,
                    defaultContent: '',
                    width: "5%"
                },
                // Fecha
                {
                    name: 'fecha',
                    data: 'fecha',
                    render: dateRender,
                    width: "5%"
                },
                // Nombre vendedor
                {
                    name: 'nombre',
                    data: 'nombre_usuario'
                },
                // Borrada por
                {
                    name: 'borrado_por',
                    data: 'borrado_por'
                },
                // Turno
                {
                    name: 'turno',
                    data: 'nombre_turno',
                    className: 'aligtxtc'
                },
                // Numero
                {
                    name: 'numero',
                    data: 'numero',
                    className: 'aligtxtc'
                },
                // Premio
                {
                    name: 'premio',
                    data: 'premio',
                    className: 'aligtxtd'
                }
            ]
        });
    });
</script>


<?php
require('includes/footer.php');
?>