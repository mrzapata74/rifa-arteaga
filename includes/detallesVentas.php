<?php

require_once __DIR__ . '/../conexion.php';


function getVentasPorTurnoVendedor($pdo, $turno, $vendedor_id, $fecha = null)
{
    $fecha = $fecha ?: date('Y-m-d');
    $comando = $pdo->prepare("SELECT idvendedor, turno, fecha, sum(recaudado) as recaudado FROM detalleventas WHERE idvendedor = :vendedorId and turno = :turno and cast(fecha as date) = :fecha GROUP BY idvendedor, turno");
    $comando->bindParam(':vendedorId', $vendedor_id);
    $comando->bindParam(':turno', $turno);
    $comando->bindParam(':fecha', $fecha);
    $comando->execute();
    $resultado = $comando->fetchAll(PDO::FETCH_ASSOC);
    return $resultado;
}
