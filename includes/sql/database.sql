-- Tablas

-- Estado de usuarios
CREATE TABLE IF NOT EXISTS `estadousers` (
  `idEstado` int NOT NULL AUTO_INCREMENT,
  `nombreEstado` text NOT NULL,
  PRIMARY KEY (`idEstado`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `tipousers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  `codusuario` varchar(30) NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `direccion` varchar(300) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `password` varchar(300) NOT NULL,
  `correo` varchar(80) NOT NULL,
  `token` varchar(300) NOT NULL,
  `precio_venta` DECIMAL(6,2) NOT NULL DEFAULT '13.00',
  `estado` int NOT NULL,
  `id_tipo` int NOT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`id_tipo`) REFERENCES `tipousers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FOREIGN KEY (`estado`) REFERENCES `estadousers` (`idEstado`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `estado_bloqueo` (
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `turnos` (
  `idturno` int NOT NULL AUTO_INCREMENT,
  `turno` varchar(20) NOT NULL,
  `rango` varchar(20) NOT NULL,
  PRIMARY KEY (`idturno`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `estado_turno` (
  `id` int NOT NULL,
  KEY `id` (`id`),
  CONSTRAINT FOREIGN KEY (`id`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `historial_bloqueos` (
  `idBloqueo` bigint NOT NULL AUTO_INCREMENT,
  `bloqueado_por` int NOT NULL,
  `turno_bloqueo` int NOT NULL,
  `bloqueo_accion` int NOT NULL,
  `bloqueo_date` datetime NOT NULL,
  PRIMARY KEY (`idBloqueo`),
  CONSTRAINT FOREIGN KEY (`turno_bloqueo`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FOREIGN KEY (`bloqueado_por`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `maximos_numeros` (
  `numero` int NOT NULL,
  `cantidadMaxima` bigint NOT NULL DEFAULT '0',
  `turno` int NOT NULL,
  `usuario` int NOT NULL,
  UNIQUE KEY (`numero`,`turno`,`usuario`),
  CONSTRAINT FOREIGN KEY (`turno`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `maximos_generales` (
	`numero` INT NOT NULL,
	`cantidadMaxima` BIGINT NOT NULL DEFAULT '0',
	UNIQUE INDEX (`numero`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `ventas` (
  `idventa` bigint NOT NULL AUTO_INCREMENT,
  `idvendedor` int NOT NULL,
  `fecha` datetime NOT NULL,
  `turno` int NOT NULL,
  `numero` int NOT NULL,
  `premio` decimal(6,2) NOT NULL,
  `precio_venta` DECIMAL(6,2) NOT NULL DEFAULT '13.00',
  `idadmin` int DEFAULT NULL,
  PRIMARY KEY (`idventa`),
  CONSTRAINT FOREIGN KEY (`idadmin`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FOREIGN KEY (`idvendedor`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FOREIGN KEY (`turno`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `ventas_borradas` (
  `idvendedor` int NOT NULL,
  `turno` int NOT NULL DEFAULT '0',
  `numero` int NOT NULL,
  `premio` decimal(6,2) NOT NULL,
  `fecha` datetime NOT NULL,
  CONSTRAINT FOREIGN KEY (`turno`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FOREIGN KEY (`idvendedor`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
