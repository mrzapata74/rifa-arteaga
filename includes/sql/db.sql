-- --------------------------------------------------------
-- Host:                         192.99.144.155
-- Versión del servidor:         8.0.22-0ubuntu0.20.04.2 - (Ubuntu)
-- SO del servidor:              Linux
-- HeidiSQL Versión:             11.3.0.6336
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para vista rifaarteaga.calcular
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `calcular` (
	`idvendedor` INT(10) NOT NULL,
	`idventa` BIGINT(19) NOT NULL,
	`turno` INT(10) NOT NULL,
	`fecha` DATETIME NOT NULL,
	`numero` INT(10) NOT NULL,
	`premio` DECIMAL(6,2) NOT NULL,
	`premioEntero` DECIMAL(10,2) NOT NULL,
	`recaudado` DECIMAL(8,2) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista rifaarteaga.consolidadoventas
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `consolidadoventas` (
	`numero` INT(10) NOT NULL,
	`turno` INT(10) NOT NULL,
	`fecha` DATETIME NOT NULL,
	`premio` DECIMAL(32,2) NULL,
	`recaudado` DECIMAL(30,2) NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista rifaarteaga.detalleshistorialbloqueos
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `detalleshistorialbloqueos` (
	`turno` VARCHAR(20) NOT NULL COLLATE 'utf8_spanish_ci',
	`nombre` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`idBloqueo` BIGINT(19) NOT NULL,
	`bloqueado_por` INT(10) NOT NULL,
	`turno_bloqueo` INT(10) NOT NULL,
	`bloqueo_accion` INT(10) NOT NULL,
	`bloqueo_date` DATETIME NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista rifaarteaga.detalleventas
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `detalleventas` (
	`idventa` BIGINT(19) NOT NULL,
	`fecha` DATETIME NOT NULL,
	`idvendedor` INT(10) NOT NULL,
	`codusuario` VARCHAR(30) NOT NULL COLLATE 'utf8_spanish_ci',
	`nombre` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`nombreTurno` VARCHAR(20) NOT NULL COLLATE 'utf8_spanish_ci',
	`turno` INT(10) NOT NULL,
	`numero` INT(10) NOT NULL,
	`premioDig` DECIMAL(6,2) NOT NULL,
	`premioEntero` DECIMAL(10,2) NOT NULL,
	`recaudado` DECIMAL(8,2) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista rifaarteaga.detalleventasborradas
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `detalleventasborradas` (
	`idvendedor` INT(10) NOT NULL,
	`turno` INT(10) NOT NULL,
	`numero` INT(10) NOT NULL,
	`premio` DECIMAL(6,2) NOT NULL,
	`fecha` DATETIME NOT NULL,
	`nombre_usuario` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`nombre_turno` VARCHAR(20) NOT NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;

-- Volcando estructura para tabla rifaarteaga.estadousers
CREATE TABLE IF NOT EXISTS `estadousers` (
  `idEstado` int NOT NULL AUTO_INCREMENT,
  `nombreEstado` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla rifaarteaga.estado_bloqueo
CREATE TABLE IF NOT EXISTS `estado_bloqueo` (
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla rifaarteaga.estado_turno
CREATE TABLE IF NOT EXISTS `estado_turno` (
  `id` int NOT NULL,
  KEY `id` (`id`),
  CONSTRAINT `estado_turno_ibfk_1` FOREIGN KEY (`id`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla rifaarteaga.historial_bloqueos
CREATE TABLE IF NOT EXISTS `historial_bloqueos` (
  `idBloqueo` bigint NOT NULL AUTO_INCREMENT,
  `bloqueado_por` int NOT NULL,
  `turno_bloqueo` int NOT NULL,
  `bloqueo_accion` int NOT NULL,
  `bloqueo_date` datetime NOT NULL,
  PRIMARY KEY (`idBloqueo`),
  KEY `FK_historial_bloqueos_turnos` (`turno_bloqueo`),
  KEY `FK_historial_bloqueos_usuarios` (`bloqueado_por`),
  CONSTRAINT `FK_historial_bloqueos_turnos` FOREIGN KEY (`turno_bloqueo`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_historial_bloqueos_usuarios` FOREIGN KEY (`bloqueado_por`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1230 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para vista rifaarteaga.listausuarios
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `listausuarios` (
	`id` INT(10) NOT NULL,
	`codusuario` VARCHAR(30) NOT NULL COLLATE 'utf8_spanish_ci',
	`imagen` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`nombre` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`usuario` VARCHAR(20) NOT NULL COLLATE 'utf8_spanish_ci',
	`cedula` VARCHAR(20) NOT NULL COLLATE 'utf8_spanish_ci',
	`telefono` VARCHAR(15) NOT NULL COLLATE 'utf8_spanish_ci',
	`correo` VARCHAR(80) NOT NULL COLLATE 'utf8_spanish_ci',
	`direccion` VARCHAR(300) NOT NULL COLLATE 'utf8_spanish_ci',
	`tipo` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish_ci',
	`tipo_id` INT(10) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para tabla rifaarteaga.maximos_numeros
CREATE TABLE IF NOT EXISTS `maximos_numeros` (
  `numero` int NOT NULL,
  `cantidadMaxima` bigint NOT NULL DEFAULT '0',
  `turno` int NOT NULL,
  `usuario` int NOT NULL,
  UNIQUE KEY `Index 3` (`numero`,`turno`,`usuario`) USING BTREE,
  KEY `FK_maximos_numeros_turnos` (`turno`) USING BTREE,
  KEY `FK_maximos_numeros_usuarios` (`usuario`) USING BTREE,
  CONSTRAINT `FK_maximos_numeros_turnos` FOREIGN KEY (`turno`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_maximos_numeros_usuarios` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla rifaarteaga.tipousers
CREATE TABLE IF NOT EXISTS `tipousers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla rifaarteaga.turnos
CREATE TABLE IF NOT EXISTS `turnos` (
  `idturno` int NOT NULL AUTO_INCREMENT,
  `turno` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `rango` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idturno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla rifaarteaga.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `codusuario` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cedula` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(300) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(300) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `token` varchar(300) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado` int NOT NULL,
  `id_tipo` int NOT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_tipo` (`id_tipo`),
  KEY `estado` (`estado`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_tipo`) REFERENCES `tipousers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`estado`) REFERENCES `estadousers` (`idEstado`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla rifaarteaga.ventas
CREATE TABLE IF NOT EXISTS `ventas` (
  `idventa` bigint NOT NULL AUTO_INCREMENT,
  `idvendedor` int NOT NULL,
  `fecha` datetime NOT NULL,
  `turno` int NOT NULL,
  `numero` int NOT NULL,
  `premio` decimal(6,2) NOT NULL,
  `idadmin` int DEFAULT NULL,
  PRIMARY KEY (`idventa`) USING BTREE,
  KEY `turno` (`turno`) USING BTREE,
  KEY `idvendedor` (`idvendedor`) USING BTREE,
  KEY `FK_ventas_usuarios` (`idadmin`) USING BTREE,
  CONSTRAINT `FK_ventas_usuarios` FOREIGN KEY (`idadmin`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ventas_ibfk_2` FOREIGN KEY (`idvendedor`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ventas_ibfk_3` FOREIGN KEY (`turno`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10352772 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla rifaarteaga.ventas_borradas
CREATE TABLE IF NOT EXISTS `ventas_borradas` (
  `idvendedor` int NOT NULL,
  `turno` int NOT NULL DEFAULT '0',
  `numero` int NOT NULL,
  `premio` decimal(6,2) NOT NULL,
  `fecha` datetime NOT NULL,
  KEY `FK_ventas_borradas_usuarios` (`idvendedor`),
  KEY `FK_ventas_borradas_turnos` (`turno`),
  CONSTRAINT `FK_ventas_borradas_turnos` FOREIGN KEY (`turno`) REFERENCES `turnos` (`idturno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ventas_borradas_usuarios` FOREIGN KEY (`idvendedor`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para disparador rifaarteaga.usuarios_before_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `usuarios_before_insert` BEFORE INSERT ON `usuarios` FOR EACH ROW BEGIN
	DECLARE baseDate DATE;
	DECLARE _lastInsert BIGINT;
	SET baseDate := NEW.creado;
	SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'usuarios' INTO _lastInsert;
	
	SET NEW.codusuario = CONCAT('C-', EXTRACT(YEAR FROM baseDate), '-', LPAD(EXTRACT(MONTH FROM baseDate), 2, '0'), '-', LPAD(_lastInsert, 5, '0'));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Volcando estructura para vista rifaarteaga.calcular
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `calcular`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `calcular` AS select `ventas`.`idvendedor` AS `idvendedor`,`ventas`.`idventa` AS `idventa`,`ventas`.`turno` AS `turno`,`ventas`.`fecha` AS `fecha`,`ventas`.`numero` AS `numero`,`ventas`.`premio` AS `premio`,(`ventas`.`premio` * 1000) AS `premioEntero`,(`ventas`.`premio` * 13) AS `recaudado` from `ventas`;

-- Volcando estructura para vista rifaarteaga.consolidadoventas
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `consolidadoventas`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `consolidadoventas` AS select `calcular`.`numero` AS `numero`,`calcular`.`turno` AS `turno`,`calcular`.`fecha` AS `fecha`,sum(`calcular`.`premioEntero`) AS `premio`,sum(`calcular`.`recaudado`) AS `recaudado` from `calcular` group by `calcular`.`numero`,`calcular`.`turno`;

-- Volcando estructura para vista rifaarteaga.detalleshistorialbloqueos
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `detalleshistorialbloqueos`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `detalleshistorialbloqueos` AS select `turnos`.`turno` AS `turno`,`usuarios`.`nombre` AS `nombre`,`hb`.`idBloqueo` AS `idBloqueo`,`hb`.`bloqueado_por` AS `bloqueado_por`,`hb`.`turno_bloqueo` AS `turno_bloqueo`,`hb`.`bloqueo_accion` AS `bloqueo_accion`,`hb`.`bloqueo_date` AS `bloqueo_date` from ((`historial_bloqueos` `hb` join `turnos` on((`hb`.`turno_bloqueo` = `turnos`.`idturno`))) join `usuarios` on((`hb`.`bloqueado_por` = `usuarios`.`id`)));

-- Volcando estructura para vista rifaarteaga.detalleventas
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `detalleventas`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `detalleventas` AS select `v`.`idventa` AS `idventa`,`v`.`fecha` AS `fecha`,`v`.`idvendedor` AS `idvendedor`,`usr`.`codusuario` AS `codusuario`,`usr`.`nombre` AS `nombre`,`turn`.`turno` AS `nombreTurno`,`v`.`turno` AS `turno`,`v`.`numero` AS `numero`,`v`.`premio` AS `premioDig`,`v`.`premioEntero` AS `premioEntero`,`v`.`recaudado` AS `recaudado` from ((`calcular` `v` join `usuarios` `usr` on((`usr`.`id` = `v`.`idvendedor`))) join `turnos` `turn` on((`v`.`turno` = `turn`.`idturno`)));

-- Volcando estructura para vista rifaarteaga.detalleventasborradas
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `detalleventasborradas`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `detalleventasborradas` AS select `vb`.`idvendedor` AS `idvendedor`,`vb`.`turno` AS `turno`,`vb`.`numero` AS `numero`,`vb`.`premio` AS `premio`,`vb`.`fecha` AS `fecha`,`usr`.`nombre` AS `nombre_usuario`,`trn`.`turno` AS `nombre_turno` from ((`ventas_borradas` `vb` join `usuarios` `usr` on((`usr`.`id` = `vb`.`idvendedor`))) join `turnos` `trn` on((`trn`.`idturno` = `vb`.`turno`)));

-- Volcando estructura para vista rifaarteaga.listausuarios
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `listausuarios`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `listausuarios` AS select `u`.`id` AS `id`,`u`.`codusuario` AS `codusuario`,`u`.`imagen` AS `imagen`,`u`.`nombre` AS `nombre`,`u`.`usuario` AS `usuario`,`u`.`cedula` AS `cedula`,`u`.`telefono` AS `telefono`,`u`.`correo` AS `correo`,`u`.`direccion` AS `direccion`,`t`.`tipo` AS `tipo`,`t`.`id` AS `tipo_id` from (`usuarios` `u` join `tipousers` `t` on((`u`.`id_tipo` = `t`.`id`)));

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
