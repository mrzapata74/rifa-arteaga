<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ventas en Línea 24</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
  <link rel="stylesheet" href="css/estilos.css?v=001">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
  <!-- ANIMATE CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />


</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="home.php"> <strong>Ventas en Línea 24</strong> </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <!--           <li class="nav-item active">
            <a class="nav-link" href="./home.php">Home <span class="sr-only">(current)</span></a>
          </li> -->
      <?php if ($_SESSION['rol'] == 1) { ?>
        <li class="nav-item active">
          <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            ADMINISTRAR
          </button>
          <div class="dropdown-menu" id="mnuNav">
            <a class="dropdown-item" href="panelctrl.php" id="itemMnuNav"><i class="fas fa-mail-bulk"></i> Interfaz de Ventas</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="establecerMaximos.php" id="itemMnuNav"><i class="fas fa-glasses"></i> Establecer Máximos</a>
            <a class="dropdown-item" href="establecerMaximosGenerales.php" id="itemMnuNav"><i class="fas fa-check-double"></i> Establecer Máximos Generales</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="importarVentas.php" id="itemMnuNav"><i class="fas fa-file-import"></i> Importar ventas</a>
          </div>

        </li>
        <li class="nav-item active">
          <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            REPORTES
          </button>
          <div class="dropdown-menu" id="mnuNav">
            <a class="dropdown-item" href="usuarios.php" id="itemMnuNav"><i class="fas fa-user-secret"></i> Lista de Usuarios</a>
            <a class="dropdown-item" href="detalleVentas.php" id="itemMnuNav"><i class="fas fa-paste"></i> Detalle de Ventas</a>
            <a class="dropdown-item" href="ventasTotalesVendedor.php" id="itemMnuNav"><i class="fas fa-print"></i> Consolidado por vendedor</a>
            <a class="dropdown-item" href="detalleVentasBorradas.php" id="itemMnuNav"><i class="fas fa-trash-alt"></i> Historial de borrado</a>
            <a class="dropdown-item" href="detallesBloqueos.php" id="itemMnuNav"><i class="fas fa-lock"></i> Historial de bloqueos</a>
            <a class="dropdown-item" href="consolidadoventas.php" id="itemMnuNav"><i class="fas fa-print"></i> Consolidado</a>
          </div>
        </li>
    </ul>

  <?php } ?>

  <div class="btn-group my-2 my-lg-0 float-right">
    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-user-tie" id="iconoUsuario"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
      <h6 class="dropdown-header" id="username"><i class="fa fa-user-circle" id="iconoUser"></i><?php echo utf8_encode($_SESSION['nombreUsuario']) ?></h6>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="logout.php"><i class="fas fa-sign-out-alt"></i> Cerrar Sesión</a>
    </div>
  </div>
  </div>
</nav>

<body>