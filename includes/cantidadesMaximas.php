<?php

require_once __DIR__ . '/../conexion.php';

function getListaTurnos($pdo)
{
    $comando = $pdo->prepare("SELECT idturno, turno FROM turnos");
    $comando->execute();

    $lista_turno = $comando->fetchAll(PDO::FETCH_ASSOC);
    return $lista_turno;
}

function getTurnoActivo($pdo)
{
    $comando = $pdo->prepare("SELECT id FROM estado_turno");
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    $turno_db = isset($resultado['id']) ? $resultado['id'] : 1;
    return $turno_db;
}

function getListaUsuarios($pdo)
{
    $comando = $pdo->prepare("SELECT id, nombre FROM usuarios");
    $comando->execute();

    $lista = $comando->fetchAll(PDO::FETCH_ASSOC);
    return $lista;
}

function estaBloqueado($pdo)
{
    $comando = $pdo->prepare("SELECT estado FROM estado_bloqueo");
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    $estado = isset($resultado['estado']) ? $resultado['estado'] : 0;

    return (int) $estado;
}

function listaCantidadesMaximas($pdo, int $start = 0, int $length = 0, int $usuario = -1, int $numero = -1)
{
    $str = ($length > 0 ? "LIMIT $start, $length" : "");
    $where = [];
    if ($usuario >= 1) {
        $where[] = "nm.usuario = $usuario";
    }
    if ($numero >= 0) {
        $where[] = "numero = $numero";
    }
    if (count($where) > 0) {
        $where = "WHERE " . join(" AND ", $where);
    } else {
        $where = "";
    }

    $comando = $pdo->prepare("SELECT nm.numero, nm.cantidadMaxima, nm.usuario AS usuarioId, usr.nombre, nm.turno as turnoId, trn.turno FROM maximos_numeros AS nm INNER JOIN usuarios AS usr ON nm.usuario = usr.id INNER JOIN turnos AS trn ON trn.idturno = nm.turno $where $str");
    $comando->execute();
    $resultado = $comando->fetchAll(PDO::FETCH_ASSOC);
    return $resultado;
}

function listaCantidadesMaximasGenerales($pdo)
{
    $comando = $pdo->prepare("SELECT numero, cantidadMaxima FROM maximos_generales");
    $comando->execute();
    $resultado = $comando->fetchAll(PDO::FETCH_ASSOC);
    return $resultado;
}

function getCountCantidadesMaximas($pdo): int
{
    $comando = $pdo->prepare("SELECT COUNT(*) AS total FROM maximos_numeros");
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    return $resultado['total'];
}

function nuevoCantidadMaxima($pdo, int $numero, int $cantidad, int $turno, int $usuario)
{
    $comando = $pdo->prepare("INSERT INTO maximos_numeros (numero, cantidadMaxima, turno, usuario) VALUES(:numero, :maxima, :turno, :usuario)");
    $comando->bindParam(':numero', $numero);
    $comando->bindParam(':maxima', $cantidad);
    $comando->bindParam(':turno', $turno);
    $comando->bindParam(':usuario', $usuario);
    return (!$comando->execute() ? false : true);
}

function nuevoCantidadMaximaGeneral($pdo, int $numero, int $cantidad)
{
    $comando = $pdo->prepare("INSERT INTO maximos_generales (numero, cantidadMaxima) VALUES(:numero, :maxima)");
    $comando->bindParam(':numero', $numero);
    $comando->bindParam(':maxima', $cantidad);
    $comando->execute();

    return $comando;
}

function bulkCantidadMaxima($pdo, int $cantidad, int $min = 0, int $max = 99, int $usuario_id = -1)
{
    $usuarios = $usuario_id === -1 ? getListaUsuarios($pdo) : false;
    $turno = getTurnoActivo($pdo);

    $pdo->beginTransaction();

    $sql = 'INSERT INTO maximos_numeros (numero, cantidadMaxima, turno, usuario) VALUES (?, ?, ?, ?)';
    $stmt = $pdo->prepare($sql);

    for ($i = $min; $i <= $max; $i++) {
        $numero = $i;
        if ($usuarios) {
            foreach ($usuarios as $usuario) {
                $userId = $usuario['id'];
                $stmt->execute([$numero, $cantidad, $turno, $userId]);
            }
        } else {
            $comand = $stmt->execute([$numero, $cantidad, $turno, $usuario_id]);
            if (!$comand) return false;
        }
    }
    return $pdo->commit();
}

function updateTurnos($pdo, int $turno)
{
    $sql = "UPDATE maximos_numeros SET turno = :turno";
    $comando = $pdo->prepare($sql);
    $comando->bindParam(':turno', $turno);
    $comando->execute();
}

function updateMontos($pdo, int $numero, int $cantidad)
{
    $sql = "UPDATE maximos_numeros SET cantidadMaxima = :cantidad WHERE numero = :numero";
    $comando = $pdo->prepare($sql);
    $comando->bindParam(':numero', $numero);
    $comando->bindParam(':cantidad', $cantidad);
    $comando->execute();
}

function borrarCantidadMaxima($pdo, int $numero, int $turno, int $usuario)
{
    $comando = $pdo->prepare("DELETE FROM maximos_numeros WHERE numero = :numero AND turno = :turno AND usuario = :usuario");
    $comando->bindParam(':numero', $numero);
    $comando->bindParam(':turno', $turno);
    $comando->bindParam(':usuario', $usuario);
    $comando->execute();
}

function borrarCantidadMaximaGeneral($pdo, int $numero)
{
    $comando = $pdo->prepare("DELETE FROM maximos_generales WHERE numero = :numero");
    $comando->bindParam(':numero', $numero);
    $comando->execute();
}

function limpiarCantidadMaxima($pdo)
{
    $comando = $pdo->prepare("DELETE FROM maximos_numeros");
    $comando->execute();
}

function limpiarCantidadMaximaVendedor($pdo, int $usuario_id)
{
    $comando = $pdo->prepare("DELETE FROM maximos_numeros WHERE usuario = :usuario");
    $comando->bindParam(':usuario', $usuario_id);
    $comando->execute();
}

function limpiarCantidadMaximaGenerales($pdo)
{
    $comando = $pdo->prepare("DELETE FROM maximos_generales");
    $comando->execute();
}

function cantidadActualNumero($pdo, int $numero, int $turno, int $usuario)
{
    $fecha_hoy = date('Y-m-d', time());

    $comando = $pdo->prepare("SELECT SUM(premioEntero) as total FROM calcular WHERE numero = :numero AND turno = :turno AND fecha BETWEEN CONCAT(:fechahoy, ' 00:00:00') AND CONCAT(:fechahoy, ' 23:59:59') AND idvendedor = :usuario");
    $comando->bindParam(':numero', $numero);
    $comando->bindParam(':turno', $turno);
    $comando->bindParam(':usuario', $usuario);
    $comando->bindParam(':fechahoy', $fecha_hoy);
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    return $resultado;
}

function cantidadActualNumeroGeneral($pdo, int $numero)
{
    $comando_turno = $pdo->prepare("SELECT id FROM estado_turno");
    $comando_turno->execute();
    $turno_activo_res = $comando_turno->fetch(PDO::FETCH_ASSOC);
    $turno_activo = isset($turno_activo_res['id']) ? $turno_activo_res['id'] : 1;

    $fecha_hoy = date('Y-m-d', time());

    $comando = $pdo->prepare("SELECT SUM(premioEntero) as total FROM calcular WHERE numero = :numero AND turno = :turno AND fecha BETWEEN CONCAT(:fechahoy, ' 00:00:00') AND CONCAT(:fechahoy, ' 23:59:59')");
    $comando->bindParam(':numero', $numero);
    $comando->bindParam(':turno', $turno_activo);
    $comando->bindParam(':fechahoy', $fecha_hoy);
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    return $resultado;
}

function cantidadMaxima($pdo, int $numero, int $turno, int $usuario)
{
    $comando = $pdo->prepare("SELECT cantidadMaxima FROM maximos_numeros WHERE numero = :numero AND turno = :turno AND usuario = :usuario");
    $comando->bindParam(':numero', $numero);
    $comando->bindParam(':turno', $turno);
    $comando->bindParam(':usuario', $usuario);
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    return $resultado;
}

function cantidadMaximaGeneral($pdo, int $numero)
{
    $comando = $pdo->prepare("SELECT cantidadMaxima FROM maximos_generales WHERE numero = :numero");
    $comando->bindParam(':numero', $numero);
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    return $resultado;
}

/**
 * Regresa verdadero si el valor actual del numero es menor o igual al permitido
 */
function puedeEntrar($pdo, int $numero, float $input, int $turno, int $usuario, bool $ignorar_bloqueo = false)
{

    $bloqueo = ($ignorar_bloqueo ? false : estaBloqueado($pdo));
    if ($bloqueo === 1) {
        return false;
    }

    // Cantidad Maxima general
    $puede_general = false;
    $cantidadMaximaGeneral = cantidadMaximaGeneral($pdo, $numero);
    $cantidadActualGeneral = cantidadActualNumeroGeneral($pdo, $numero);

    if ($cantidadMaximaGeneral && $cantidadActualGeneral) {
        $maxima = $cantidadMaximaGeneral['cantidadMaxima'];
        $actual = $cantidadActualGeneral['total'];

        $sum = ($actual + ($input * 1000));
        $puede_general = ($maxima >= $sum);
    } else {
        $puede_general = true;
    }

    if ($puede_general === true) {
        $cantidadMaxima = cantidadMaxima($pdo, $numero, $turno, $usuario);
        $cantidadActual = cantidadActualNumero($pdo, $numero, $turno, $usuario);

        if ($cantidadMaxima && $cantidadActual) {
            $maxima = $cantidadMaxima['cantidadMaxima'];
            $actual = $cantidadActual['total'];

            $sum = ($actual + ($input * 1000));
            return ($maxima >= $sum);
        }
        return true;
    }
    return false;
}
