<?php
session_start();

if (!isset($_SESSION['user_id']))
    header('Location: /index.php');

include('conexion.php');

$fecha_hoy = date('Y-m-d', time());

$start_date = isset($_POST['start_date']) && strlen($_POST['start_date']) > 0 ? $_POST['start_date'] : $fecha_hoy;
$end_date = isset($_POST['end_date']) && strlen($_POST['end_date']) > 0 ? $_POST['end_date'] : $fecha_hoy;

$comando = $pdo->prepare("SELECT * FROM detalleshistorialbloqueos WHERE DATE(bloqueo_date) BETWEEN :startDate AND :endDate");
$comando->bindParam(':startDate', $start_date);
$comando->bindParam(':endDate', $end_date);
$comando->execute();

$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);

$json = [
    'data' => $resultado
];

echo json_encode($json, JSON_PRETTY_PRINT);
