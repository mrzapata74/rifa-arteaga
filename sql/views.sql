-- --------------------------------------------------------
-- Host:                         10.10.1.25
-- Server version:               8.0.26 - MySQL Community Server - GPL
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6337
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for view rifa_junio_sql.calcular
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `calcular` (
	`idvendedor` INT(10) NOT NULL,
	`idventa` BIGINT(19) NOT NULL,
	`turno` INT(10) NOT NULL,
	`fecha` DATETIME NOT NULL,
	`numero` INT(10) NOT NULL,
	`premio` DECIMAL(6,2) NOT NULL,
	`premioEntero` DECIMAL(10,2) NOT NULL,
	`recaudado` DECIMAL(12,4) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view rifa_junio_sql.consolidadoventas
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `consolidadoventas` (
	`numero` INT(10) NOT NULL,
	`turno` INT(10) NOT NULL,
	`fecha` DATETIME NOT NULL,
	`premio` DECIMAL(32,2) NULL,
	`recaudado` DECIMAL(34,4) NULL
) ENGINE=MyISAM;

-- Dumping structure for view rifa_junio_sql.detalleshistorialbloqueos
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `detalleshistorialbloqueos` (
	`turno` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`nombre` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`idBloqueo` BIGINT(19) NOT NULL,
	`bloqueado_por` INT(10) NOT NULL,
	`turno_bloqueo` INT(10) NOT NULL,
	`bloqueo_accion` INT(10) NOT NULL,
	`bloqueo_date` DATETIME NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view rifa_junio_sql.detalleventas
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `detalleventas` (
	`idventa` BIGINT(19) NOT NULL,
	`fecha` DATETIME NOT NULL,
	`idvendedor` INT(10) NOT NULL,
	`codusuario` VARCHAR(30) NULL COLLATE 'utf8_general_ci',
	`nombre` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`nombreTurno` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`turno` INT(10) NOT NULL,
	`numero` INT(10) NOT NULL,
	`premioDig` DECIMAL(6,2) NOT NULL,
	`premioEntero` DECIMAL(10,2) NOT NULL,
	`recaudado` DECIMAL(12,4) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view rifa_junio_sql.detalleventasborradas
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `detalleventasborradas` (
	`idvendedor` INT(10) NOT NULL,
	`turno` INT(10) NOT NULL,
	`numero` INT(10) NOT NULL,
	`premio` DECIMAL(6,2) NOT NULL,
	`fecha` DATETIME NOT NULL,
	`nombre_usuario` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`nombre_turno` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view rifa_junio_sql.listausuarios
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `listausuarios` (
	`id` INT(10) NOT NULL,
	`codusuario` VARCHAR(30) NULL COLLATE 'utf8_general_ci',
	`imagen` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci',
	`nombre` VARCHAR(100) NOT NULL COLLATE 'utf8_spanish_ci',
	`usuario` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`cedula` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`telefono` VARCHAR(15) NOT NULL COLLATE 'utf8_general_ci',
	`correo` VARCHAR(80) NOT NULL COLLATE 'utf8_general_ci',
	`direccion` VARCHAR(300) NOT NULL COLLATE 'utf8_spanish_ci',
	`tipo` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`tipo_id` INT(10) NOT NULL,
	`precio_venta` DECIMAL(6,2) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view rifa_junio_sql.calcular
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `calcular`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `calcular` AS select `ventas`.`idvendedor` AS `idvendedor`,`ventas`.`idventa` AS `idventa`,`ventas`.`turno` AS `turno`,`ventas`.`fecha` AS `fecha`,`ventas`.`numero` AS `numero`,`ventas`.`premio` AS `premio`,(`ventas`.`premio` * 1000) AS `premioEntero`,(`ventas`.`premio` * `ventas`.`precio_venta`) AS `recaudado` from `ventas`;

-- Dumping structure for view rifa_junio_sql.consolidadoventas
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `consolidadoventas`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `consolidadoventas` AS select `calcular`.`numero` AS `numero`,`calcular`.`turno` AS `turno`,`calcular`.`fecha` AS `fecha`,sum(`calcular`.`premioEntero`) AS `premio`,sum(`calcular`.`recaudado`) AS `recaudado` from `calcular` group by `calcular`.`numero`,`calcular`.`turno`;

-- Dumping structure for view rifa_junio_sql.detalleshistorialbloqueos
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `detalleshistorialbloqueos`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `detalleshistorialbloqueos` AS select `turnos`.`turno` AS `turno`,`usuarios`.`nombre` AS `nombre`,`hb`.`idBloqueo` AS `idBloqueo`,`hb`.`bloqueado_por` AS `bloqueado_por`,`hb`.`turno_bloqueo` AS `turno_bloqueo`,`hb`.`bloqueo_accion` AS `bloqueo_accion`,`hb`.`bloqueo_date` AS `bloqueo_date` from ((`historial_bloqueos` `hb` join `turnos` on((`hb`.`turno_bloqueo` = `turnos`.`idturno`))) join `usuarios` on((`hb`.`bloqueado_por` = `usuarios`.`id`)));

-- Dumping structure for view rifa_junio_sql.detalleventas
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `detalleventas`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `detalleventas` AS select `v`.`idventa` AS `idventa`,`v`.`fecha` AS `fecha`,`v`.`idvendedor` AS `idvendedor`,`usr`.`codusuario` AS `codusuario`,`usr`.`nombre` AS `nombre`,`turn`.`turno` AS `nombreTurno`,`v`.`turno` AS `turno`,`v`.`numero` AS `numero`,`v`.`premio` AS `premioDig`,`v`.`premioEntero` AS `premioEntero`,`v`.`recaudado` AS `recaudado` from ((`calcular` `v` join `usuarios` `usr` on((`usr`.`id` = `v`.`idvendedor`))) join `turnos` `turn` on((`v`.`turno` = `turn`.`idturno`)));

-- Dumping structure for view rifa_junio_sql.detalleventasborradas
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `detalleventasborradas`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `detalleventasborradas` AS select `vb`.`idvendedor` AS `idvendedor`,`vb`.`turno` AS `turno`,`vb`.`numero` AS `numero`,`vb`.`premio` AS `premio`,`vb`.`fecha` AS `fecha`,`usr`.`nombre` AS `nombre_usuario`,`trn`.`turno` AS `nombre_turno` from ((`ventas_borradas` `vb` join `usuarios` `usr` on((`usr`.`id` = `vb`.`idvendedor`))) join `turnos` `trn` on((`trn`.`idturno` = `vb`.`turno`)));

-- Dumping structure for view rifa_junio_sql.listausuarios
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `listausuarios`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `listausuarios` AS select `u`.`id` AS `id`,`u`.`codusuario` AS `codusuario`,`u`.`imagen` AS `imagen`,`u`.`nombre` AS `nombre`,`u`.`usuario` AS `usuario`,`u`.`cedula` AS `cedula`,`u`.`telefono` AS `telefono`,`u`.`correo` AS `correo`,`u`.`direccion` AS `direccion`,`t`.`tipo` AS `tipo`,`t`.`id` AS `tipo_id`,`u`.`precio_venta` AS `precio_venta` from (`usuarios` `u` join `tipousers` `t` on((`u`.`id_tipo` = `t`.`id`)));

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
