-- --------------------------------------------------------
-- Host:                         10.10.1.25
-- Server version:               8.0.26 - MySQL Community Server - GPL
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6337
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table rifa_junior.estadousers: ~3 rows (approximately)
/*!40000 ALTER TABLE `estadousers` DISABLE KEYS */;
INSERT INTO `estadousers` (`idEstado`, `nombreEstado`) VALUES
	(1, 'Activo'),
	(2, 'Suspendido'),
	(3, 'Baja');
/*!40000 ALTER TABLE `estadousers` ENABLE KEYS */;

-- Dumping data for table rifa_junior.estado_bloqueo: ~1 rows (approximately)
/*!40000 ALTER TABLE `estado_bloqueo` DISABLE KEYS */;
INSERT INTO `estado_bloqueo` (`estado`) VALUES
	(0);
/*!40000 ALTER TABLE `estado_bloqueo` ENABLE KEYS */;

-- Dumping data for table rifa_junior.estado_turno: ~1 rows (approximately)
/*!40000 ALTER TABLE `estado_turno` DISABLE KEYS */;
INSERT INTO `estado_turno` (`id`) VALUES
	(1);
/*!40000 ALTER TABLE `estado_turno` ENABLE KEYS */;

-- Dumping data for table rifa_junior.tipousers: ~2 rows (approximately)
/*!40000 ALTER TABLE `tipousers` DISABLE KEYS */;
INSERT INTO `tipousers` (`id`, `tipo`) VALUES
	(1, 'Administrador'),
	(2, 'Vendedor');
/*!40000 ALTER TABLE `tipousers` ENABLE KEYS */;

-- Dumping data for table rifa_junior.turnos: ~4 rows (approximately)
/*!40000 ALTER TABLE `turnos` DISABLE KEYS */;
INSERT INTO `turnos` (`idturno`, `turno`, `rango`) VALUES
	(1, 'Matutino', '11:00 am'),
	(2, 'Sorteo Extra', '6:00 pm'),
	(3, 'Tarde', '3:00 pm'),
	(4, 'Noche', '9:00 pm');
/*!40000 ALTER TABLE `turnos` ENABLE KEYS */;

-- Dumping data for table rifa_junior.usuarios: ~1 rows (approximately)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `imagen`, `codusuario`, `nombre`, `cedula`, `telefono`, `direccion`, `usuario`, `password`, `correo`, `token`, `estado`, `id_tipo`, `creado`) VALUES
	(1, '1597017435.png', 'C-2021-07-00001', 'admin', '', '', '', 'admin', '$2y$10$l5Y1i9UNNFgKRlK3Qj9OQu8iLrC6NBChrOIE7iHoRK3HNhn.hpBxy', '', 'dfb82db8569df4c62e5a63f48d224a63', 1, 1, '2021-08-02 15:09:57');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
