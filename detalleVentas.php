<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1))
    header('Location: /index.php');

include('conexion.php');
require('includes/header.php');

// SQL Query
$comando = $pdo->prepare("SELECT id, nombre FROM listausuarios");
$comando->execute();

// array data 
$user_list = $comando->fetchAll(PDO::FETCH_ASSOC);

// SQL Query
$comando = $pdo->prepare("SELECT idturno, turno FROM turnos");
$comando->execute();

// array data 
$lista_turno = $comando->fetchAll(PDO::FETCH_ASSOC);

?>

<div class="container" id="wraphome">
    <h4 id="tituloDetalleVentas">Detalle de Ventas por Vendedor</h4>
    <form class="form-inline">
        <div class="form-group mb-2" id="selUsers">
            <label for="user_select">
                <i class="fas fa-user"></i> Usuario
            </label>
            <select class="form-control form-control-sm" id="user_select">
                <option value="-1" selected>Todos</option>
                <?php
                foreach ($user_list as $user) {
                    $user_id = $user['id'];
                    $user_name = $user['nombre'];
                    echo "<option value=\"$user_id\">$user_name</option>\r\n";
                }
                ?>
            </select>
            <label for="turno_select">
                <i class="fas fa-tasks"></i> Turno
            </label>
            <select class="form-control form-control-sm" id="turno_select">
                <option value="-1" selected>Todos</option>
                <?php
                foreach ($lista_turno as $turno) {
                    $turno_id = $turno['idturno'];
                    $turno_name = $turno['turno'];
                    echo "<option value=\"$turno_id\">$turno_name</option>\r\n";
                }
                ?>
            </select>
            <label for="numero_select">
                <i class="fas fa-list-ol"></i> Numero
            </label>
            <select class="form-control form-control-sm" id="numero_select">
                <option value="-1" selected>Todos</option>
                <?php
                foreach (range(0, 99) as $numero) {
                    echo "<option value=\"$numero\">$numero</option>\r\n";
                }
                ?>
            </select>
        </div>
    </form>
    <input type="text" class="contanier form-control text-center" name="rangepicker" id="calendario" />
    <button type="button" class="btn btn-danger" id="delete_range">Borrar rango</button>
    <br>
    <table id="rptDeVtas" class="display responsive nowrap table table-striped">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th>Fecha</th>
                <th>Codigo</th>
                <th>Vendedor</th>
                <th>Turno</th>
                <th>Número</th>
                <th id="titlealigcenter">Mono</th>
                <th id="titlealigcenter">Recaudado</th>
            </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        var dateInputFormat = 'YYYY-MM-DD HH:mm:ss';

        var numberFormat = $.fn.dataTable.render.number(',', '.', 2, 'C$').display;
        var dateRender = function(url, type, full) {
            var formated = moment(url, dateInputFormat);
            return (formated.isValid() ? formated.format('dddd DD MMMM YYYY | hh:mm:ssa') : url);
        };

        window.deleteVenta = function(venta_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: '¿Estás seguro de borrar este registro?',
                text: "¡No podrás revertir esta acción!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, Borrarlo!',
                cancelButtonText: 'No, Cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    // Aciones si 
                    let date_input_format = 'YYYY-MM-DD';
                    $.post('obtVtasTotales.php', {
                        delete_id: venta_id,
                    }, function(res) {
                        console.log('bye bye peaple');
                    });
                    swalWithBootstrapButtons.fire(
                        '¡Eliminado!',
                        'El registro han sido eliminado.',
                        'success'
                    ).then((result) => {
                        table.ajax.reload();
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelado',
                        'Tu registro está seguro :)',
                        'error'
                    )
                }
            })
        }

        // Date picker
        var date_range = $('input[name="rangepicker"]').daterangepicker({
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Rango Personalizado",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
        }, function(start, end, label) {
            table.ajax.reload();
            console.log(start, end, label);
        });

        $("#user_select").change(function() {
            table.ajax.reload();
        });
        $("#turno_select").change(function() {
            table.ajax.reload();
        });
        $("#numero_select").change(function() {
            table.ajax.reload();
        });


        $('#delete_range').click(function() {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: '¿Estás seguro de borrar esta información?',
                text: "¡No podrás revertir esta acción!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, Borrarla!',
                cancelButtonText: 'No, Cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    // Aciones si 
                    let date_input_format = 'YYYY-MM-DD';
                    $.post('obtVtasTotales.php', {
                        delete: true,
                        start_date: date_range.data('daterangepicker').startDate.format(date_input_format),
                        end_date: date_range.data('daterangepicker').endDate.format(date_input_format),
                        user_id: $('#user_select').val(),
                        turno_id: $('#turno_select').val()
                    }, function(res) {
                        console.log('bye bye peaple');
                    });
                    swalWithBootstrapButtons.fire(
                        '¡Eliminados!',
                        'Tus registros han sido eliminados.',
                        'success'
                    ).then((result) => {
                        table.ajax.reload();
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelado',
                        'Tus datos están seguros :)',
                        'error'
                    )
                }
            })
        });

        var table = $('#rptDeVtas').DataTable({
            fixedHeader: true,
            searching: false,
            language: {
                "lengthMenu": "Ver _MENU_ Archivos por Página",
                "zeroRecords": "Lo sentimos, no tenemos resultados",
                "info": "Mostrando Página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin Registros para Mostrar",
                "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
                processing: "Cargando, esto podria tomar unos segundos....",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                },
                "search": "Buscar",
                buttons: {
                    colvisRestore: 'Restaurar',
                }
            },
            processing: true,
            serverSide: true,
            ordering: false,
            dom: 'Blfrtip',
            buttons: [{
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel'
                },
                {
                    extend: 'pdf',
                    text: 'PDF'
                },
                {
                    extend: 'print',
                    text: 'Imprimir',
                    messageTop: '<h3 class="text-center">DETALLE DE VENTAS POR VENDEDOR Y FECHA</h3>',
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '12pt');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                },
                {
                    extend: 'colvis',
                    text: 'Ver/Ocultar',
                    postfixButtons: ['colvisRestore']
                }
            ],
            responsive: {
                details: {
                    type: 'column'
                }
            },
            bDestroy: true,
            iDisplayLength: 10,
            order: [
                [5, "asc"]
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Todos"]
            ],
            ajax: {
                url: 'obtVtasTotales.php',
                type: "POST",
                dataType: "json",
                error: function(e) {
                    console.log(e.responseText);
                },
                data: function(d) {
                    let date_input_format = 'YYYY-MM-DD';
                    var start_date = date_range.data('daterangepicker').startDate.format(date_input_format);
                    var end_date = date_range.data('daterangepicker').endDate.format(date_input_format);
                    var user_id = $('#user_select').val();
                    var turno_id = $('#turno_select').val();

                    let filtro_numero = $('#numero_select').val();

                    console.log(start_date, end_date, user_id, turno_id);

                    d.start_date = start_date;
                    d.end_date = end_date;
                    d.user_id = user_id;
                    d.turno_id = turno_id;
                    d.filtro_numero = filtro_numero;
                }
            },
            columns: [
                // 0 | Responsive
                {
                    className: 'control noVis notexport',
                    data: null,
                    orderable: false,
                    defaultContent: '',
                    width: "5%"
                },
                {
                    className: 'notexport',
                    data: null,
                    orderable: false,
                    searchable: false,
                    width: "5%",
                    render: function(url, type, full) {
                        let venta_id = full.idventa;
                        let html = `<button class="btn btn-danger btn-sm" onclick="deleteVenta(${venta_id});"><i class="fas fa-trash"></i></button>`;
                        return html;
                    }
                },
                // Fecha
                {
                    name: 'fecha',
                    data: 'fecha',
                    render: dateRender,
                    width: "5%"
                },
                // ID Vendedor
                {
                    name: 'codusuario',
                    data: 'codusuario',
                    className: 'table-center',
                    visible: false
                },
                // Nombre vendedor
                {
                    name: 'nombre',
                    data: 'nombre',
                },
                // Turno
                {
                    name: 'turno',
                    data: 'nombreTurno',
                    className: 'aligtxtc'
                },
                // Numero
                {
                    name: 'numero',
                    data: 'numero',
                    className: 'aligtxtc'
                },
                // Premio
                {
                    name: 'premio',
                    data: 'premioEntero',
                    render: numberFormat,
                    className: 'aligtxtd'
                },
                // Recaudado
                {
                    name: 'recaudado',
                    data: 'recaudado',
                    render: numberFormat,
                    className: 'aligtotDet'
                }
            ],
            footerCallback: function(row, data, start, end, display) {
                let colname = 'recaudado:name';
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(colname)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal = api
                    .column(colname, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(colname).footer()).html(
                    numberFormat(pageTotal) + ' (' + numberFormat(total) + ')'
                );
            }
        });
    });
</script>


<?php
require('includes/footer.php');
?>