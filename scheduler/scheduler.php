<?php

date_default_timezone_set('America/Managua'); // Zona horaria del PHP

require_once __DIR__ . '/../vendor/autoload.php';

use GO\Scheduler;

// Create a new scheduler
$scheduler = new Scheduler();

// ... configure the scheduled jobs (see below) ...

// 11:00 am Matutino
$scheduler->php(__DIR__ . '/bloquearEntradas.php')->daily(11)->output('rifaarteaga_schuduler.log');

// 3:00 pm Tarde
$scheduler->php(__DIR__ . '/bloquearEntradas.php')->daily(15)->output('rifaarteaga_schuduler.log');

// 9:00 pm Noche
$scheduler->php(__DIR__ . '/bloquearEntradas.php')->daily(21)->output('rifaarteaga_schuduler.log');

// 6:00 pm Martes Sorteo Extra
$scheduler->php(__DIR__ . '/bloquearEntradas.php')->tuesday(18)->output('rifaarteaga_schuduler.log');

// 6:00 pm Sábados Sorteo Extra
$scheduler->php(__DIR__ . '/bloquearEntradas.php')->saturday(18)->output('rifaarteaga_schuduler.log');

// Let the scheduler execute jobs which are due.
$scheduler->run();
