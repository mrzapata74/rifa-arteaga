<?php
session_start();

if (!isset($_SESSION['user_id'])) {
    die();
}

$user_id = $_SESSION['user_id']; // User ID 

include('conexion.php');
require_once __DIR__ . '/includes/cantidadesMaximas.php';
require_once __DIR__ . '/includes/detallesVentas.php';

$json = [];
if ($user_id) {
    $id = $user_id;
    $fecha = FECHA_HOY;

    $numero = $_POST['numero'];
    $premio = $_POST['premio'];

    if (!$numero || !$premio) {
        die();
    }

    $comando = $pdo->prepare("SELECT id FROM estado_turno");
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    $turno_db = isset($resultado['id']) ? $resultado['id'] : 1;
    $puedeIr = puedeEntrar($pdo, $numero, (float) $premio, $turno_db, $user_id);
    $turno = $turno_db;

    $comando = $pdo->prepare("SELECT precio_venta FROM usuarios WHERE id = :user_id");
    $comando->bindParam(':user_id', $user_id);
    $comando->execute();
    $resultado = $comando->fetch(PDO::FETCH_ASSOC);
    $precio_venta = isset($resultado['precio_venta']) ? $resultado['precio_venta'] : 13.00;

    if ($puedeIr) {
        $comando = $pdo->prepare("INSERT INTO ventas (idvendedor, fecha, turno, numero, premio, precio_venta) VALUES(:idvendedor, :fecha, :turno, :numero, :premio, :precio_venta)");
        $comando->bindParam(':idvendedor', $id);
        $comando->bindParam(':fecha', $fecha);
        $comando->bindParam(':turno', $turno);
        $comando->bindParam(':numero', $numero);
        $comando->bindParam(':premio', $premio);
        $comando->bindParam(':precio_venta', $precio_venta);

        if (!$comando->execute()) {
            $json = [
                'data' => false,
                'error' => $comando->errorInfo(),
            ];
        } else {
            // Obtiene el ID de la venta
            $last_id = $pdo->lastInsertId();

            $ventas_totales = getVentasPorTurnoVendedor($pdo, $turno, $id);
            $venta_actual = cantidadActualNumero($pdo, $numero, $turno_db, $user_id);
            $venta_maxima = cantidadMaxima($pdo, $numero, $turno_db, $user_id);

            $json = [
                'data' => true,
                'venta_id' => $last_id,
                'turno' => $turno,
                'fecha' => $fecha,
                'vendedor_id' => $id,
                'precio_venta' => $precio_venta,
                'recaudado' => isset($ventas_totales[0]['recaudado']) ? $ventas_totales[0]['recaudado'] : 0,
                'venta_actual' => $venta_actual,
                'venta_maxima' => $venta_maxima,
                'post' => $_POST
            ];
        }
    } else {
        $cantidadMaximaGeneral = cantidadMaximaGeneral($pdo, $numero);
        $cantidadActualGeneral = cantidadActualNumeroGeneral($pdo, $numero);

        $json = [
            'error' => "El numero $numero ya tiene el máximo de ventas, o se bloquearon las entradas.",
            'maximos' => [
                'usuario' => [
                    'maximo' => cantidadMaxima($pdo, $numero, $turno_db, $user_id),
                    'actual' => cantidadActualNumero($pdo, $numero, $turno_db, $user_id)
                ],
                'general' => [
                    'maxima' => $cantidadMaximaGeneral,
                    'actual' => $cantidadActualGeneral
                ]
            ],
            'turno_id' => $turno_db,
            'usuario_id' => $user_id,
            'numero' => $numero,
            'premio' => (((float) $premio) * 1000),
            'fecha' => $fecha,
            'bloqueo' => estaBloqueado($pdo),
        ];
    }
}

echo json_encode($json);
