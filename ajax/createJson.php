<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1)) {
    header('Location: /index.php');
    return;
}

require_once __DIR__ . '/../conexion.php';

$time = time();
$fecha_hoy = date('Y-m-d', $time);

$start_date = isset($_POST['start_date']) && strlen($_POST['start_date']) > 0 ? $_POST['start_date'] : $fecha_hoy;
$end_date = isset($_POST['end_date']) && strlen($_POST['end_date']) > 0 ? $_POST['end_date'] : $fecha_hoy;

$cantidad_corte = isset($_POST['corte']) && strlen($_POST['corte']) > 0 && intval($_POST['corte']) ? $_POST['corte'] : 0;
$precio_venta = isset($_POST['precio_venta']) && strlen($_POST['precio_venta']) > 0 && floatval($_POST['precio_venta']) ? $_POST['precio_venta'] : 13.00;
$turno_id =  isset($_POST['turno_id']) && strlen($_POST['turno_id']) > 0 && intval($_POST['turno_id']) ? $_POST['turno_id'] : 0;

if ($turno_id > 0) {
    // SQL Query
    $comando = $pdo->prepare("SELECT numero, turno, DATE(fecha) AS fecha, SUM(premioEntero) AS premio, SUM(recaudado) AS recaudado FROM calcular WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND turno = :id_turno GROUP BY numero, turno");
    $comando->bindParam(':startDate', $start_date);
    $comando->bindParam(':endDate', $end_date);
    $comando->bindParam(':id_turno', $turno_id);
    $comando->execute();
} else {
    // SQL Query
    $comando = $pdo->prepare("SELECT numero, turno, DATE(fecha) AS fecha, SUM(premioEntero) AS premio, SUM(recaudado) AS recaudado FROM calcular WHERE DATE(fecha) BETWEEN :startDate AND :endDate GROUP BY numero, turno");
    $comando->bindParam(':startDate', $start_date);
    $comando->bindParam(':endDate', $end_date);
    $comando->execute();
}

// array data
$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);

$buffer_data = [];
// Prepare buffer data
for ($i = 0; $i < 100; $i++) {
    $buffer_data[] = [
        "numero" => $i,
        "premio" => 0
    ];
}

foreach ($resultado as $venta) {
    $venta_numero = $venta['numero'];
    $venta_monto = ($venta['premio'] - $cantidad_corte) / 1000;

    if (isset($buffer_data[$venta_numero])) {
        $buffer_data[$venta_numero] = [
            'numero' => $venta_numero,
            'premio' => $venta_monto
        ];
    }
}

$json = json_encode([
    'meta' => [
        'fecha' => $fecha_hoy,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'corte' => $cantidad_corte,
        'precio_venta' => $precio_venta,
        'turno' => $turno_id
    ],
    'data' => $buffer_data
]);

$file_name = "ventas_$fecha_hoy-$turno_id-$time.json";

$file = fopen(__DIR__ . "/../json/$file_name", 'w');
$cwrite = fwrite($file, $json);
fclose($file);

echo json_encode([
    "file" => $file_name,
    "loader" => "$fecha_hoy-$turno_id-$time",
    "save" => $cwrite,
    "link" => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/json/$file_name"
], JSON_PRETTY_PRINT);
