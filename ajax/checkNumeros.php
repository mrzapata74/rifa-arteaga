<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1)) {
    header('Location: /index.php');
    return;
}

header('Access-Control-Allow-Origin: *');

require_once __DIR__ . '/../conexion.php';
require_once __DIR__ . '/../includes/cantidadesMaximas.php';

$fecha = FECHA_HOY;
$json_print = [];

$turno_id =  isset($_POST['turno_id']) && strlen($_POST['turno_id']) > 0 && intval($_POST['turno_id']) ? $_POST['turno_id'] : 0;
$user_id =  isset($_POST['user_id']) && strlen($_POST['user_id']) > 0 && intval($_POST['user_id']) ? $_POST['user_id'] : 0;

$link = isset($_POST['link']) ? $_POST['link'] : false;

if ($link) {
    $json = @file_get_contents($link);
    if (!$json) {
        $json_print['error'] = true;
        return;
    }

    $json = json_decode($json);
    $ventas = $json->data;

    foreach ($ventas as $venta) {
        $numero = $venta->numero;
        $premio = $venta->premio;
        $json_print[$numero] = puedeEntrar($pdo, $numero, (float) $premio, $turno_id, $user_id, true);
    }
}

echo json_encode($json_print, JSON_PRETTY_PRINT);
