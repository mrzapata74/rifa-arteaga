<?php

include_once __DIR__ . '/../conexion.php';
include_once __DIR__ . '/../includes/cantidadesMaximas.php';

$action = filter_input(INPUT_GET, 'p');

$json = [];

switch ($action) {
    case 'lista':
        $lista = listaCantidadesMaximasGenerales($pdo);
        $structure = [
            'data' => $lista
        ];
        $json = $structure;
        break;

    case 'borrar':
        $numero = filter_input(INPUT_GET, 'numero', FILTER_VALIDATE_INT);
        borrarCantidadMaximaGeneral($pdo, $numero);
        $json = [
            'status' => 200
        ];
        break;

    case 'nuke':
        limpiarCantidadMaximaGenerales($pdo);
        break;

    case 'nuevo':
        $numero = filter_input(INPUT_POST, 'numero', FILTER_VALIDATE_INT);
        $maximo = filter_input(INPUT_POST, 'maximo', FILTER_VALIDATE_INT);
        if ($numero >= 0 && $maximo >= 0) {
            nuevoCantidadMaximaGeneral($pdo, $numero, $maximo);
            $json = [
                'status' => 200
            ];
        } else {
            $json = [
                'error' => 1
            ];
        }
        break;
    default:
        $json = [
            'error' => 404
        ];
        break;
}

echo json_encode($json);
