<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1)) {
    header('Location: /index.php');
    return;
}

require_once __DIR__ . '/../conexion.php';

header('Access-Control-Allow-Origin: *');

$fecha = FECHA_HOY;
$json_print = [];

$turno_id =  isset($_POST['turno_id']) && strlen($_POST['turno_id']) > 0 && intval($_POST['turno_id']) ? $_POST['turno_id'] : 0;
$user_id =  isset($_POST['user_id']) && strlen($_POST['user_id']) > 0 && intval($_POST['user_id']) ? $_POST['user_id'] : 0;

$link = isset($_POST['link']) ? $_POST['link'] : false;

if ($link) {
    $json = @file_get_contents($link);
    if (!$json) {
        $json_print['error'] = true;
        return;
    }

    $json = json_decode($json);
    $ventas = $json->data;
    $precio_venta = $json->meta->precio_venta;
    $pdo->beginTransaction();

    $sql = 'INSERT INTO ventas (idvendedor, fecha, turno, numero, premio, precio_venta) VALUES(?, ?, ?, ?, ?, ?)';
    $stmt = $pdo->prepare($sql);

    foreach ($ventas as $venta) {
        $numero = $venta->numero;
        $premio = $venta->premio;
        if ($premio > 0) {
            $stmt->execute([$user_id, $fecha, $turno_id, $numero, $premio, $precio_venta]);
        }
    }
    if ($pdo->commit()) {
        $json_print['insert'] = true;
    }
}

echo json_encode($json_print);
