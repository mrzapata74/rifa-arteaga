<?php

session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1)) {
    header('Location: /index.php');
    return;
}

require_once __DIR__ . '/../conexion.php';

$fecha_hoy = date('Y-m-d', time());
$start_date = isset($_POST['start_date']) && strlen($_POST['start_date']) > 0 ? $_POST['start_date'] : $fecha_hoy;
$end_date = isset($_POST['end_date']) && strlen($_POST['end_date']) > 0 ? $_POST['end_date'] : $fecha_hoy;

$user_id = isset($_POST['user_id']) && intval($_POST['user_id']) > 0 ? $_POST['user_id'] : -1;
$turno_id = isset($_POST['turno_id']) && intval($_POST['turno_id']) > 0 ? $_POST['turno_id'] : 1;
$numero = isset($_POST['numero']) && intval($_POST['numero']) >= 0 ? $_POST['numero'] : -1;

$numero_ganador = [];

if ($user_id > 0) { // Usuario especifico
    if ($numero >= 0) { // Numero ganador
        $comando = $pdo->prepare("SELECT vt.idvendedor, usr.nombre,  SUM(vt.premio) AS premio, vt.precio_venta FROM ventas AS vt INNER JOIN usuarios AS usr ON usr.id = vt.idvendedor WHERE DATE(vt.fecha) BETWEEN :startDate AND :endDate AND vt.turno = :turno_id AND vt.idvendedor = :user_id AND vt.numero = :numero GROUP BY idvendedor, precio_venta");

        $comando->bindParam(':user_id', $user_id);
        $comando->bindParam(':numero', $numero);

        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->bindParam(':turno_id', $turno_id);

        $comando->execute();
        $resultado = $comando->fetchAll(PDO::FETCH_ASSOC);
        $numero_ganador = $resultado;
    }
    // Todos los números
    $comando = $pdo->prepare("SELECT vt.idvendedor, usr.nombre,  SUM(vt.premio) AS premio, vt.precio_venta FROM ventas AS vt INNER JOIN usuarios AS usr ON usr.id = vt.idvendedor WHERE DATE(vt.fecha) BETWEEN :startDate AND :endDate AND vt.turno = :turno_id AND vt.idvendedor = :user_id GROUP BY idvendedor, precio_venta");

    $comando->bindParam(':user_id', $user_id);
} else { // Todos los usuarios
    if ($numero >= 0) { // Numero ganador
        $comando = $pdo->prepare("SELECT vt.idvendedor, usr.nombre,  SUM(vt.premio) AS premio, vt.precio_venta FROM ventas AS vt INNER JOIN usuarios AS usr ON usr.id = vt.idvendedor WHERE DATE(vt.fecha) BETWEEN :startDate AND :endDate AND vt.turno = :turno_id AND vt.numero = :numero GROUP BY idvendedor, precio_venta");

        $comando->bindParam(':numero', $numero);

        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->bindParam(':turno_id', $turno_id);

        $comando->execute();
        $resultado = $comando->fetchAll(PDO::FETCH_ASSOC);
        $numero_ganador = $resultado;
    }

    // Todos los números
    $comando = $pdo->prepare("SELECT vt.idvendedor, usr.nombre,  SUM(vt.premio) AS premio, vt.precio_venta FROM ventas AS vt INNER JOIN usuarios AS usr ON usr.id = vt.idvendedor WHERE DATE(vt.fecha) BETWEEN :startDate AND :endDate AND vt.turno = :turno_id GROUP BY idvendedor, precio_venta");
}

$comando->bindParam(':startDate', $start_date);
$comando->bindParam(':endDate', $end_date);
$comando->bindParam(':turno_id', $turno_id);

$comando->execute();

$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);

$data = [];

foreach ($resultado as $row) {
    $premio = $row['premio'];
    $idvendedor = $row['idvendedor'];
    $precio_venta = $row['precio_venta'];

    $numero_ganador_data = array_filter($numero_ganador, function ($arr) use ($idvendedor, $precio_venta) {
        return ($arr['idvendedor'] === $idvendedor && $arr['precio_venta'] === $precio_venta);
    });
    $premio_ganador = isset($numero_ganador_data[array_key_first($numero_ganador_data)]['premio']) ? $numero_ganador_data[array_key_first($numero_ganador_data)]['premio'] : 0;

    $data[] = [
        "id_vendedor" => $idvendedor,
        "nombre_vendedor" => $row['nombre'],
        "premio_entero" => $premio,
        "premio" => ($premio * 1000),
        "recaudado" => ($premio * $precio_venta),
        "premio_ganador" => ($premio_ganador * 1000),
        "recaudado_ganador" => ($premio_ganador * $precio_venta),
        "precio_venta" => $precio_venta
    ];
}

$json = [
    'data' => $data,
    'data_raw' => $numero_ganador,
    'user_id' => $user_id,
    'turno_id' => $turno_id,
    'numero' => $numero
];

echo json_encode($json, JSON_PRETTY_PRINT);
