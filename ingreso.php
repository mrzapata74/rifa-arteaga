<?php 
session_start();

include('conexion.php');

$json = [];
if(isset($_SESSION['user_id'])) {
   $json['login'] = true;
   $json['mensaje'] = "Login existente";
}else{
    if(!empty($_POST['usuario']) && !empty($_POST['password'])){
        $comando = $pdo->prepare("SELECT id, usuario, password, nombre, id_tipo FROM usuarios WHERE usuario = :usuario");
        $comando->bindParam(':usuario', $_POST['usuario']);
        $comando->execute();
        $resultado = $comando->fetch(PDO::FETCH_ASSOC);

        if($resultado && count($resultado) > 0 && password_verify($_POST['password'], $resultado['password'])){
            $_SESSION['user_id'] = $resultado['id'];
            $_SESSION['nombreUsuario'] = $resultado['nombre'];
            $_SESSION['rol'] = $resultado['id_tipo'];
            $json['login'] = true;
            $json['resultado'] = $resultado;
        } else {
            $json['login'] = false;
            $json['mensaje'] = 'Datos Incorrectos';
        }
    } else {
        $json['login'] = false;
        $json['mensaje'] = "no post data";
    }
}


echo json_encode($json, JSON_PRETTY_PRINT);

