<?php
include('conexion.php');

$json = [
    "error" => "no data"
];

if (isset($_POST['nombre'])) {
    $nombre = $_POST['nombre'];
    $cedula = $_POST['cedula'];
    $celular = $_POST['telefono'];
    $direccion = $_POST['direccion'];
    $correo = $_POST['correo'];
    $usuario = $_POST['usuario'];
    $tipoUsuario = $_POST['tipo_id'];
    $user_id = $_POST['id'];
    $precio_venta = floatval($_POST['precio_venta']);

    $comando = $pdo->prepare("UPDATE usuarios SET nombre=:nombre, cedula=:cedula, telefono=:telefono, direccion=:direccion, usuario=:usuario, correo=:correo, id_tipo=:id_tipo, precio_venta=:precio_venta WHERE id = :user_id");
    $comando->bindParam(':nombre', $nombre);
    $comando->bindParam(':cedula', $cedula);
    $comando->bindParam(':telefono', $celular);
    $comando->bindParam(':direccion', $direccion);
    $comando->bindParam(':usuario', $usuario);
    $comando->bindParam(':correo', $correo);
    $comando->bindParam(':id_tipo', $tipoUsuario);
    $comando->bindParam(':user_id', $user_id);
    $comando->bindParam(':precio_venta', $precio_venta);
    $comando->execute();

    if (!$comando) {
        $json = [
            "error" =>  "Error en la inserción de los datos."
        ];
    }
    $json = [
        "message" => "Datos insertados satisfactoriamente"
    ];
}

echo json_encode($json);
