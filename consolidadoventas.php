<?php
session_start();
if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1))
    header('Location: /index.php');
include('conexion.php');
require('includes/header.php');

// SQL Query
$comando = $pdo->prepare("SELECT idturno, turno FROM turnos");
$comando->execute();

// array data 
$lista_turno = $comando->fetchAll(PDO::FETCH_ASSOC);

// SQL Query Usuarios
$vendedores = $pdo->prepare("SELECT id, nombre FROM usuarios");
$vendedores->execute();

//Array de usuarios
$lista_vendedores = $vendedores->fetchAll(PDO::FETCH_ASSOC);

?>
<br>
<input type="text" class="contanier form-control text-center" name="rangepicker" id="calendario" />
<small class="text-center text-muted" id="messDate">Selecciona un rango de fechas para mostrar en el reporte</small>

<form class="form-inline">
    <div class="form-group mb-2" id="contTurno">
        <label for="turno_select">
            <i class="fas fa-user"></i> Turno
        </label>
        <select class="form-control form-control-sm" id="turno_select">
            <option value="-1" selected>Todos</option>
            <?php
            foreach ($lista_turno as $turno) {
                $turno_id = $turno['idturno'];
                $turno_name = $turno['turno'];
                echo "<option value=\"$turno_id\">$turno_name</option>\r\n";
            }
            ?>
        </select>


        <label for="vendedor_select">
            <i class="fas fa-user"></i> Vendedor
        </label>
        <select class="form-control form-control-sm" id="vendedor_select">
            <option value="-1" selected>Todos</option>
            <?php
            foreach ($lista_vendedores as $vendedor) {
                $vendedor_id = $vendedor['id'];
                $vendedor_name = $vendedor['nombre'];
                echo "<option value=\"$vendedor_id\">$vendedor_name</option>\r\n";
            }
            ?>
        </select>
    </div>
</form>


<div class="row" style="margin: 10px; padding: 10px;">
    <div class="col-sm-12 col-md-4">
        <label for="colum_count" id="labelcons">Filas</label>
        <div class="col" id="inputFila"><input type="number" class="form-control text-center" id="colum_count" value="10"></div>
    </div>
    <div class="col-sm-12 col-md-4">
        <label for="cantidad_corte" id="labelcons">Corte C$</label>
        <div class="col" id="inputCorte"><input type="number" class="form-control text-center corteNum" id="cantidad_corte" value="0"></div>
    </div>
    <div class="col-sm-12 col-md-4">
        <label for="precio_venta" id="labelcons">Precio de venta</label>
        <div class="col"><input type="number" class="form-control text-center" id="precio_venta" value="13"></div>
    </div>
</div>

<div id="contn" class="row">
    <div class="col-6">
        <button type="button" class="btn btn-info" id="generateJsonLink"><i class="fas fa-save"></i> Generar link de reporte</button>
    </div>
</div>

<div class="container" id="wrappConsol">
    <div class="row" id="filappal">
    </div>
</div>
<br /><br /><br /><br />

<script type="text/javascript">
    var date_range = null;
    var base_url = window.location.pathname.split('/');
    base_url.pop();
    base_url = base_url.join('/');

    function textToClipboard(text) {
        var dummy = document.createElement("textarea");
        document.body.appendChild(dummy);
        dummy.value = text;
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
    }

    // Dom daterange
    $(document).ready(function() {
        $('#generateJsonLink').click(function() {
            Swal.fire({
                title: 'Generar reporte para compartir',
                showCancelButton: true,
                confirmButtonText: 'Generar',
                cancelButtonText: 'Cancelar',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.post(`${base_url}/ajax/createJson.php`, {
                            corte: parseInt($('#cantidad_corte').val()),
                            precio_venta: parseFloat($('#precio_venta').val()),
                            turno_id: parseInt($('#turno_select').val()),
                            start_date: date_range.data('daterangepicker').startDate.format('YYYY/MM/DD'),
                            end_date: date_range.data('daterangepicker').endDate.format('YYYY/MM/DD')
                        })
                        .done(function(data) {
                            return data;
                        })
                        .fail(function() {
                            Swal.showValidationMessage(`Request failed`);
                        });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    let json = JSON.parse(result.value);
                    let copyLink = `${window.location.origin}${base_url}/json/load.php?f=${json.loader}`;
                    Swal.fire({
                        title: `Ventas guardadas`,
                        html: `Link copiado al portapapeles <a target="_blank" href="${copyLink}">aquí.</a>`
                    });
                    textToClipboard(copyLink);
                }
            })
        });

        function loop() {
            setInterval(function() {
                cargaVentasTotales();
            }, 1200)
        }

        function cargaVentasTotales() {
            $.post('totalventas.php', {
                colums: parseInt($('#colum_count').val()),
                corte: parseInt($('#cantidad_corte').val()),
                precio_venta: parseFloat($('#precio_venta').val()),
                turno_id: parseInt($('#turno_select').val()),
                vendedor_id: parseInt($('#vendedor_select').val()),
                start_date: date_range.data('daterangepicker').startDate.format('YYYY/MM/DD'),
                end_date: date_range.data('daterangepicker').endDate.format('YYYY/MM/DD')
            }, function(res) {
                $('#filappal').html(res);
            });
        }

        // Date picker
        date_range = $('input[name="rangepicker"]').daterangepicker({
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Rango Personalizado",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
        }, function(start, end, label) {

        });

        // Ajax loop start
        loop();



    });


    /*     
    var cleaveNumber = new Cleave('.corteNum', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });
     */
</script>

<?php
require('includes/footer.php');
?>