<?php
session_start();
if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1))
    header('Location: /index.php');
include('conexion.php');
require('includes/header.php');

// SQL Query
$comando = $pdo->prepare("SELECT id, tipo FROM tipousers");
$comando->execute();
// array data
$user_types = $comando->fetchAll(PDO::FETCH_ASSOC);

?>
<div class="container" id="wraphome">
    <h4 id="tituloDetalleVentas">Lista de Usuarios</h4>
    <div class="box-header with-border">
        <h3 class="box-title">
            <div class="btn-group" role="group">
                <button id="btnAddUser" class="btn-form-toggle btn btn-success"><i class="fa fa-plus-circle"></i> Agregar usuario</button>
            </div>
        </h3>
    </div>
    <div id="tableSpace">
        <table id="rptDeUsers" class="display responsive nowrap table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Acciones</th>
                    <th>Código</th>
                    <th>Foto</th>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Precio venta</th>
                    <th>Cédula</th>
                    <th>Teléfono</th>
                    <th>Correo</th>
                    <th>Dirección</th>
                    <th>Rol</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>




    <br>
    <div id="formAddvendedor">

        <form id="formUser" style="display: none;">

            <div class="col imagen" id="crop-area">
                <div class="row d-flex justify-content-center">
                    <div id="upload-demo"></div>
                    <button type="button" class="btn btn-rotate" data-deg="-90">
                        <i class="fas fa-chevron-left"></i>
                    </button>
                    <button type="button" class="btn btn-rotate" data-deg="90">
                        <i class="fas fa-chevron-right"></i>
                    </button>
                </div>
            </div>



            <div class="row entradas">
                <div class="form-group">
                    <label class="col-form-label btn btn-sm btn-info" for="image" id="lblbtn">
                        <i class="fas fa-upload"></i> Subir foto</label>
                    <div>
                        <input id="image" type="file" name="image" class="custom-file-input">
                    </div>
                </div>
                <input type="hidden" name="id" />
                <div class="form-group">
                    <label for="nombre" class="col-form-label">Tipo</label>
                    <div>
                        <select class="form-control" id="tipo_id" name="tipo_id">
                            <?php
                            foreach ($user_types as $user) {
                                $type_id = $user['id'];
                                $type_name = $user['tipo'];
                                echo "<option value=\"$type_id\">$type_name</option>\r\n";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" id="grupoInputsUser">
                    <label for="nombre" class="col-form-label">Nombre</label>
                    <div>
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                </div>
                <div class="form-group" id="grupoInputsUser">
                    <label for="usuario" class="col-form-label">Usuario</label>
                    <div>
                        <input type="text" class="form-control" id="usuario" name="usuario">
                    </div>
                </div>
                <div class="form-group unagrilla" id="grupoInputsUser">
                    <label for="cedula" class="col-form-label">Precio de venta</label>
                    <div>
                        <input type="number" class="form-control" id="precio_venta" name="precio_venta" value="13">
                    </div>
                </div>
                <div class="form-group" id="grupoInputsUser">
                    <label for="cedula" class="col-form-label">Cedula</label>
                    <div>
                        <input type="text" class="form-control" id="cedula" name="cedula">
                    </div>
                </div>
                <div class="form-group" id="grupoInputsUser">
                    <label for="telefono" class="col-form-label">Teléfono</label>
                    <div>
                        <input type="text" class="form-control" id="telefono" name="telefono">
                    </div>
                </div>
                <div class="form-group" id="grupoInputsUser">
                    <label for="correo" class="col-form-label">Correo</label>
                    <div>
                        <input type="text" class="form-control" id="correo" name="correo">
                    </div>
                </div>
                <div class="form-group" id="grupoInputsUser">
                    <label for="direccion" class="col-form-label">Dirección</label>
                    <div>
                        <input type="text" class="form-control" id="direccion" name="direccion">
                    </div>
                </div>
                <div class="form-group unagrilla" id="grupoInputsUser">
                    <button id="btnSaveUser" type="button" class="btn btn-primary mb-2"><i class="fas fa-save"></i> Guardar</button>
                    <button type="button" class="btn-form-toggle btn btn-primary mb-2"><i class="fas fa-arrow-circle-left"></i> Cancelar</button>
                </div>



            </div>

        </form>


    </div>


</div>

<script type="text/javascript">
    var toggleForm = function() {
        var form = $('#formUser');
        var table = $('#tableSpace');
        table.toggle();
        form.toggle();
        // $('#upload-demo').croppie('bind');
    };
    var table;

    // Croppie
    var croppie = $('#upload-demo').croppie({
        url: 'img/users/default.png',
        enableExif: true,
        enableOrientation: true,
        viewport: {
            width: 250,
            height: 250,
            type: 'circle'
        },
        boundary: {
            width: 300,
            height: 300
        }
    });
    $(function() {
        // btn form
        $('.btn-form-toggle').click(function() {
            toggleForm();
        });

        $('#btnAddUser').click(function() {
            $('#formUser').trigger("reset");
            $('#formUser').find('input[type=hidden]').each(function() {
                this.value = '';
            });
            $('#upload-demo').croppie('bind', {
                url: 'img/users/default.png'
            });
        });

        $('.btn-rotate').on('click', function(ev) {
            var deg = parseInt($(this).data('deg'));
            croppie.croppie('rotate', deg);
        });

        // File
        $('#image').on('change', function() {
            $("#upload-demo").croppie('bind');
            var reader = new FileReader();
            reader.onload = function(e) {
                croppie.croppie('bind', {
                    url: e.target.result
                }).then(function() {
                    console.log('jQuery bind complete');
                });
            };
            reader.readAsDataURL(this.files[0]);
        });

        // Datatable
        table = $('#rptDeUsers').DataTable({
            fixedHeader: false,
            language: {
                "lengthMenu": "Ver _MENU_ Archivos por Página",
                "zeroRecords": "Lo sentimos, no tenemos resultados",
                "info": "Mostrando Página _PAGE_ de _PAGES_ de un Total de _MAX_ Registros",
                "infoEmpty": "Sin Registros para Mostrar",
                "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                },
                "search": "Buscar",
                buttons: {
                    colvisRestore: 'Restaurar',
                }
            },
            aProcessing: true,
            aServerSide: true,
            dom: 'Blfrtip',
            buttons: [{
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel'
                },
                {
                    extend: 'pdf',
                    text: 'PDF'
                },
                {
                    extend: 'print',
                    text: 'Imprimir',
                    messageTop: '<h3 class="text-center">REPORTE DE USUARIOS</h3>',
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '11pt')
                            .prepend(
                                '<img src="https://www.mzdevocotal.com/rifaarteaga/img/logoArInforme.png" style="position:absolute; top:0; left:0; opacity:0.8; width:400px; height:250px;" />'
                            );

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }

                },
                {
                    extend: 'colvis',
                    text: 'Ver/Ocultar',
                    postfixButtons: ['colvisRestore']
                }

            ],
            responsive: {
                details: {
                    type: 'column'
                }
            },
            bDestroy: true,
            iDisplayLength: 10,
            order: [
                [1, "asc"]
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Todos"]
            ],
            ajax: {
                url: 'queryusuarios.php',
                type: "GET",
                dataType: "json",
                error: function(e) {
                    console.log(e.responseText);
                }
            },
            columns: [
                // 0 | Responsive
                {
                    className: 'control noVis notexport',
                    data: null,
                    orderable: false,
                    defaultContent: '',
                    width: "5%",
                },
                // 1 | Acciones
                {
                    className: 'notexport',
                    data: null,
                    orderable: false,
                    searchable: false,
                    width: "5%",
                    render: function(url, type, full) {
                        let user_id = full.id;

                        let buffer = [];
                        $.each(full, function(index, value) {
                            buffer.push(`data-${index}="${value}"`);
                        });

                        let html = `<button class="btn btn-danger btn-sm btn-remove-user" data-user_id="${user_id}"><i class="fas fa-trash"></i></button>`;
                        html += `<button class="btn btn-warning btn-sm btn-password-user" data-user_id="${user_id}"><i class="fas fa-key"></i></button>`;
                        html += `<button class="btn btn-info btn-sm btn-update-user" ${buffer.join(" ")}><i class="fas fa-edit"></i></button>`;
                        return html;
                    }
                },
                // 2 | Cod user
                {
                    data: 'codusuario'
                },
                // 3 | imagen
                {
                    data: 'imagen',
                    className: 'notexport',
                    orderable: false,
                    render: function(url, type, full) {
                        var imagen = full.imagen;
                        return `<img style="width:80px;" src="img/users/${imagen}"/>`;
                    }
                },
                // 4 | Nombre
                {
                    data: 'nombre'

                },
                // 5 | usuario
                {
                    data: 'usuario'
                },
                // precio de venta
                {
                    data: 'precio_venta'
                },
                // 6 | cedula
                {
                    data: 'cedula',
                    visible: false
                },
                // 7 | telefono
                {
                    data: 'telefono'
                },
                // 8 | Correo
                {
                    data: 'correo',
                    visible: false
                },
                // 9 | dirección
                {
                    data: 'direccion',
                    visible: false
                },
                // 10 | tipo
                {
                    data: 'tipo'
                }
            ]
        });

        table.on('draw', function() {
            // btn update user
            $('.btn-update-user').unbind('click');
            $('.btn-update-user').click(function() {
                var button = $(this);
                $.each(button.data(), function(index, value) {
                    $(`#formUser [name="${index}"]`).val(value);
                    if (index === 'imagen') {
                        var img = value;
                        $('#upload-demo').croppie('bind', {
                            url: `img/users/${img}`
                        }).then(function() {
                            $('#upload-demo').croppie('setZoom', 0);
                        });
                    }
                });
                toggleForm();
            });
            // btn remove user
            $('.btn-remove-user').unbind('click');
            $('.btn-remove-user').click(function() {
                var button = $(this);
                var user_Id = button.data('user_id');
                if (confirm('¿Estas seguro que queires borrar el usuario?')) {
                    console.log(`Borrando usuario ${user_Id}...`);
                    $.post('deleteuser.php', {
                        user_id: user_Id
                    }, function(res) {
                        console.log(res);
                        table.ajax.reload();
                    });
                }
            });
            // btn password user
            $('.btn-password-user').unbind('click');
            $('.btn-password-user').click(function() {
                var button = $(this);
                var user_Id = button.data('user_id');

                let new_password = prompt("¿Cual es la nueva contraseña?");
                if (new_password.length >= 3) {
                    $.post('updatePasswordUser.php', {
                        user_id: user_Id,
                        password: new_password
                    }, function(res) {
                        console.log(res);
                        alert('Contraseña actualizada');
                        // table.ajax.reload();
                    });
                } else {
                    alert("La contraseña es muy corta");
                }
            });
        });
    });

    // btn form save
    $('#btnSaveUser').click(function() {
        var form = $('#formUser');
        var formData = $(form).serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});
        console.log(formData);
        var data = formData;

        if (formData.id) {
            console.log('Actualizando usuario');
            $.post('updateUserData.php', data, function(res) {
                console.log(res);
                toggleForm();
                table.ajax.reload();
            });
        } else {
            data.callback = function(res) {
                console.log('Usuario guardado');
                console.log(res);
                table.ajax.reload();
            };
            croppie.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function(img) {
                console.log('Guardando usuario...');
                toggleForm();
                data.image64 = img;
                console.log(data);
                $.post('newuser.php', data, data.callback);
            });
        }
    });
</script>


<?php
require('includes/footer.php');
?>