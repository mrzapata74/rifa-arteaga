<?php
session_start();

if (!isset($_SESSION['user_id'])) {
    die();
}

$user_id = $_SESSION['user_id']; // User ID 

// Conexión de la base de datos
/**
 * @var $pdo es el objeto de la base de datos.
 */
include('conexion.php');

/**
 * Esta función tiene que obtener los datos de turno esta activo y si esta bloqueada la entrada de datos.
 * 
 */

// Variable GET para las distintas acciones del método
$action = $_GET['accion'];

// Varible con datos que se usaran dentro del script en formato JSON para fácil implementación de JS.
$json = [];

switch ($action) {
    case 'home': // Estados de la pagina de inicio
        $tiempo = date('h:i A');
        $comando = $pdo->prepare("SELECT id FROM estado_turno");
        $comando->execute();
        $resultado = $comando->fetch(PDO::FETCH_ASSOC);

        $turno_db = isset($resultado['id']) ? $resultado['id'] : 1;

        $turno_info = $pdo->prepare("SELECT idturno, turno, rango FROM turnos WHERE idturno = :turnoId");
        $turno_info->bindParam(':turnoId', $turno_db);
        $turno_info->execute();
        $turno_info_res = $turno_info->fetchAll(PDO::FETCH_ASSOC);

        $comando = $pdo->prepare("SELECT estado FROM estado_bloqueo");
        $comando->execute();
        $resultado = $comando->fetch(PDO::FETCH_ASSOC);
        $estado = isset($resultado['estado']) ? $resultado['estado'] : 0;

        $json['bloqueo'] = $estado;
        $json['turno_activo'] = $turno_info_res;
        $json['hora'] = $tiempo;
        $json['login'] = isset($_SESSION['user_id']);
        break;
    case 'turno_activo': // Regresar el turno en objeto JSON del turno que tenga el estado 1
        $comando = $pdo->prepare("SELECT id FROM estado_turno");
        $comando->execute();
        $resultado = $comando->fetch(PDO::FETCH_ASSOC);

        $turno_db = isset($resultado['id']) ? $resultado['id'] : 1;

        $turno_info = $pdo->prepare("SELECT idturno, turno, rango FROM turnos WHERE idturno = :turnoId");
        $turno_info->bindParam(':turnoId', $turno_db);
        $turno_info->execute();
        $turno_info_res = $turno_info->fetchAll(PDO::FETCH_ASSOC);

        $json['turno_activo'] = $turno_info_res;
        break;
    case 'establecer_turno': // setear el turno activo
        $idTurno = $_GET['turno_id'] ?: 0; // ID del turno se saca de la url (post body)

        // Borrar todos los estados.
        $delete = $pdo->prepare("DELETE FROM estado_turno");
        $delete->execute();

        $comando = $pdo->prepare("INSERT INTO estado_turno (id) VALUES(:idTurno)");
        $comando->bindParam(':idTurno', $idTurno);
        $comando->execute();

        $json['status'] = 200;
        $json['id_data'] = $idTurno;
        break;

    case 'bloqueo_activo': // Consulta a la base de datos el estado de bloquedo de los campos.
        $comando = $pdo->prepare("SELECT estado FROM estado_bloqueo");
        $comando->execute();
        $resultado = $comando->fetch(PDO::FETCH_ASSOC);

        $estado = isset($resultado['estado']) ? $resultado['estado'] : 0;

        $json['bloqueo'] = $estado;
        break;
    case 'establecer_bloqueo_bloquear':
        $fecha = date(DATE_ATOM);
        $comando = $pdo->prepare("UPDATE estado_bloqueo SET estado = 1");
        $comando->execute();

        // Turno
        $comando = $pdo->prepare("SELECT id FROM estado_turno");
        $comando->execute();
        $resultado = $comando->fetch(PDO::FETCH_ASSOC);
        $turno_db = isset($resultado['id']) ? $resultado['id'] : 1;

        $comando2 = $pdo->prepare("INSERT INTO historial_bloqueos (bloqueado_por, turno_bloqueo, bloqueo_accion, bloqueo_date) VALUES(:iduser, :turno, 1, :fecha)");
        $comando2->bindParam(':iduser', $user_id);
        $comando2->bindParam(':turno', $turno_db);
        $comando2->bindParam(':fecha', $fecha);
        $comando2->execute();

        $json['status'] = 200;
        $json['bloquedo'] = 'Bloqueado';
        break;
    case 'establecer_bloqueo_desbloquear':
        $fecha = date(DATE_ATOM);

        $comando = $pdo->prepare("UPDATE estado_bloqueo SET estado = 0");
        $comando->execute();

        // Turno
        $comando = $pdo->prepare("SELECT id FROM estado_turno");
        $comando->execute();
        $resultado = $comando->fetch(PDO::FETCH_ASSOC);
        $turno_db = isset($resultado['id']) ? $resultado['id'] : 1;

        $comando2 = $pdo->prepare("INSERT INTO historial_bloqueos (bloqueado_por, turno_bloqueo, bloqueo_accion, bloqueo_date) VALUES(:iduser, :turno, 0, :fecha)");
        $comando2->bindParam(':iduser', $user_id);
        $comando2->bindParam(':turno', $turno_db);
        $comando2->bindParam(':fecha', $fecha);
        $comando2->execute();

        $json['status'] = 200;
        $json['bloquedo'] = 'Desbloqueado';
        break;
    default:
        $json['error'] = 404;
        break;
}

echo json_encode($json);
