<?php
session_start();
include('conexion.php');

if (!isset($_SESSION['user_id'])) {
    return;
}

$user_id = $_SESSION['user_id'] ?: 0;
$turno = isset($_POST['turno']) ? $_POST['turno'] : 1;

$fecha_hoy = date('Y-m-d', time());
$start_date = isset($_POST['start_date']) && strlen($_POST['start_date']) > 0 ? $_POST['start_date'] : $fecha_hoy;
$end_date = isset($_POST['end_date']) && strlen($_POST['end_date']) > 0 ? $_POST['end_date'] : $fecha_hoy;

$start = isset($_POST['start']) ? intval($_POST['start']) : 0;
$length = isset($_POST['length']) ? intval($_POST['length']) : 0;
$draw = isset($_POST['draw']) ? intval($_POST['draw']) : 0;

$numero = isset($_POST['numero']) ? intval($_POST['numero']) : -1;
$numero_sql = ($numero !== -1 ? " and numero = $numero" : '');

$comando = $pdo->prepare("SELECT COUNT(idventa) AS total FROM ventas WHERE idvendedor = :vendedor AND turno = :turno AND DATE(fecha) BETWEEN :startDate AND :endDate $numero_sql");
$comando->bindParam(':vendedor', $user_id);
$comando->bindParam(':turno', $turno);
$comando->bindParam(':startDate', $start_date);
$comando->bindParam(':endDate', $end_date);
$comando->execute();
$totalVentas = $comando->fetch(PDO::FETCH_ASSOC);
$totalVentas = ($totalVentas ? $totalVentas['total'] : 0);

$comando = $pdo->prepare("SELECT idventa, fecha, turno, numero, premio FROM ventas WHERE turno = :turno AND idvendedor = :vendedor AND DATE(fecha) BETWEEN :startDate AND :endDate $numero_sql ORDER BY fecha DESC LIMIT $start, $length");
$comando->bindParam(':vendedor', $user_id);
$comando->bindParam(':turno', $turno);
$comando->bindParam(':startDate', $start_date);
$comando->bindParam(':endDate', $end_date);

$comando->execute();
$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);

echo json_encode([
    'data' => $resultado,
    "draw" => $draw,
    "recordsTotal" => $totalVentas,
    "recordsFiltered" => $totalVentas,
    'other' => [
        'start' => $start,
        'length' => $length
    ]
]);
