<?php
session_start();
include('conexion.php');

$fecha_hoy = date('Y-m-d', time());
$start_date = isset($_POST['start_date']) && strlen($_POST['start_date']) > 0 ? $_POST['start_date'] : $fecha_hoy;
$end_date = isset($_POST['end_date']) && strlen($_POST['end_date']) > 0 ? $_POST['end_date'] : $fecha_hoy;
$user_id = isset($_POST['user_id']) && intval($_POST['user_id']) > 0 ? $_POST['user_id'] : -1;
$turno_id = isset($_POST['turno_id']) && intval($_POST['turno_id']) > 0 ? $_POST['turno_id'] : -1;

$delete = isset($_POST['delete']) ? $_POST['delete'] : false;

if ($delete) {
    if ($user_id > 0) {
        if ($turno_id > 0) {
            $comando = $pdo->prepare("DELETE FROM ventas_borradas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :user_id AND turno = :turno_id");
            $comando->bindParam(':startDate', $start_date);
            $comando->bindParam(':endDate', $end_date);
            $comando->bindParam(':user_id', $user_id);
            $comando->bindParam(':turno_id', $turno_id);
        } else {
            $comando = $pdo->prepare("DELETE FROM ventas_borradas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :user_id");
            $comando->bindParam(':startDate', $start_date);
            $comando->bindParam(':endDate', $end_date);
            $comando->bindParam(':user_id', $user_id);
        }
    } else {
        if ($turno_id > 0) {
            $comando = $pdo->prepare("DELETE FROM ventas_borradas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND turno = :turno_id");
            $comando->bindParam(':startDate', $start_date);
            $comando->bindParam(':endDate', $end_date);
            $comando->bindParam(':turno_id', $turno_id);
        } else {
            $comando = $pdo->prepare("DELETE FROM ventas_borradas WHERE DATE(fecha) BETWEEN :startDate AND :endDate");
            $comando->bindParam(':startDate', $start_date);
            $comando->bindParam(':endDate', $end_date);
        }
    }

    $comando->execute();
    return;
}

if ($user_id > 0) {
    if ($turno_id > 0) {
        $comando = $pdo->prepare("SELECT * FROM detalleventasborradas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :user_id AND turno = :turno_id");
        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->bindParam(':user_id', $user_id);
        $comando->bindParam(':turno_id', $turno_id);
        $comando->execute();
    } else {
        $comando = $pdo->prepare("SELECT * FROM detalleventasborradas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :user_id");
        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->bindParam(':user_id', $user_id);
        $comando->execute();
    }
} else {
    if ($turno_id > 0) {
        $comando = $pdo->prepare("SELECT * FROM detalleventasborradas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND turno = :turno_id");
        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->bindParam(':turno_id', $turno_id);
        $comando->execute();
    } else {
        $comando = $pdo->prepare("SELECT * FROM detalleventasborradas WHERE DATE(fecha) BETWEEN :startDate AND :endDate");
        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->execute();
    }
}

$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);

$json = [
    'data' => $resultado
];

echo json_encode($json);
