<?php
session_start();
include('conexion.php');
require_once __DIR__ . '/includes/cantidadesMaximas.php';
require_once __DIR__ . '/includes/detallesVentas.php';

$fecha_hoy = date('Y-m-d', time());
$start_date = isset($_POST['start_date']) && strlen($_POST['start_date']) > 0 ? $_POST['start_date'] : $fecha_hoy;
$end_date = isset($_POST['end_date']) && strlen($_POST['end_date']) > 0 ? $_POST['end_date'] : $fecha_hoy;
$user_id = isset($_POST['user_id']) && intval($_POST['user_id']) > 0 ? $_POST['user_id'] : -1;
$turno_id = isset($_POST['turno_id']) && intval($_POST['turno_id']) > 0 ? $_POST['turno_id'] : -1;

$filtro_numero = isset($_POST['filtro_numero']) && strlen($_POST['filtro_numero']) > 0 ? $_POST['filtro_numero'] : -1;

$delete = isset($_POST['delete']) ? $_POST['delete'] : false;
$delete_id = isset($_POST['delete_id']) ? $_POST['delete_id'] : 0;

$start = isset($_POST['start']) && intval($_POST['start']) ? $_POST['start'] : 0;
$length = isset($_POST['length']) && intval($_POST['length']) ? $_POST['length'] : 25;
$draw = isset($_POST['draw']) ? intval($_POST['draw']) : 0;

$LIMIT = ($length > -1 ? "LIMIT $start, $length" : "");
$FILTRO = ($filtro_numero > -1 ? "AND numero = :numero" : "");

$nombre_user = utf8_encode($_SESSION['nombreUsuario']);

if ($delete) {
    $admin = isset($_SESSION['user_id']) && (isset($_SESSION['rol']) && $_SESSION['rol'] == 1);
    if (!$admin) {
        die('adm');
    }
    if ($user_id > 0) {
        if ($turno_id > 0) {
            $comando = $pdo->prepare("DELETE FROM ventas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :user_id AND turno = :turno_id");
            $comando->bindParam(':startDate', $start_date);
            $comando->bindParam(':endDate', $end_date);
            $comando->bindParam(':user_id', $user_id);
            $comando->bindParam(':turno_id', $turno_id);
        } else {
            $comando = $pdo->prepare("DELETE FROM ventas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :user_id");
            $comando->bindParam(':startDate', $start_date);
            $comando->bindParam(':endDate', $end_date);
            $comando->bindParam(':user_id', $user_id);
        }
    } else {
        if ($turno_id > 0) {
            $comando = $pdo->prepare("DELETE FROM ventas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND turno = :turno_id");
            $comando->bindParam(':startDate', $start_date);
            $comando->bindParam(':endDate', $end_date);
            $comando->bindParam(':turno_id', $turno_id);
        } else {
            $comando = $pdo->prepare("DELETE FROM ventas WHERE DATE(fecha) BETWEEN :startDate AND :endDate");
            $comando->bindParam(':startDate', $start_date);
            $comando->bindParam(':endDate', $end_date);
        }
    }

    $comando->execute();

    $fecha = FECHA_HOY;
    $hdel = $pdo->prepare("INSERT INTO ventas_borradas_rangos (id_borrador, id_vendedor, fecha_borrado, fecha_inicio, fecha_fin) VALUES(:id_borrador, :id_vendedor, :fecha_borrado, :fecha_inicio, :fecha_fin)");
    $hdel->bindParam(':id_borrador', $_SESSION['user_id']);
    $hdel->bindParam(':id_vendedor', $user_id);
    $hdel->bindParam(':fecha_borrado', $fecha);
    $hdel->bindParam(':fecha_inicio', $start_date);
    $hdel->bindParam(':fecha_fin', $end_date);
    if (!$hdel->execute()) {
        echo 'no se pudo insertar el registro de borrado de rango';
    }

    return;
}

if ($delete_id) {
    $bloqueo = estaBloqueado($pdo);
    $admin = isset($_SESSION['user_id']) && (isset($_SESSION['rol']) && $_SESSION['rol'] == 1);
    if ($admin || $bloqueo !== 1) {
        $ventacmd = $pdo->prepare("SELECT * FROM ventas WHERE idventa = :id_venta");
        $ventacmd->bindParam(':id_venta', $delete_id);
        $ventacmd->execute();
        $venta = $ventacmd->fetch(PDO::FETCH_ASSOC);

        $turno_actual = getTurnoActivo($pdo);

        if ($venta['turno'] !== $turno_actual && !$admin) {
            echo 'no se pudo borrar la venta idem';
            return;
        }

        $comando = $pdo->prepare("DELETE FROM ventas WHERE idventa = :id_venta");
        $comando->bindParam(':id_venta', $delete_id);

        if ($comando->execute()) {
            $fecha = FECHA_HOY;
            $hdel = $pdo->prepare("INSERT INTO ventas_borradas (idvendedor, borrado_por, turno, numero, premio, fecha) VALUES(:idvendedor, :borrado_por, :turno, :numero, :premio, :fecha)");
            $hdel->bindParam(':idvendedor', $venta['idvendedor']);
            $hdel->bindParam(':borrado_por', $nombre_user);
            $hdel->bindParam(':turno', $venta['turno']);
            $hdel->bindParam(':numero', $venta['numero']);
            $hdel->bindParam(':premio', $venta['premio']);
            $hdel->bindParam(':fecha', $fecha);
            if (!$hdel->execute()) {
                echo 'no se pudo insertar el registro de borrado';
            }
        } else {
            echo 'no se pudo borrar la venta';
        }
    } else {
        echo 'entradas bloqueadas';
    }

    $recaudado_total = getVentasPorTurnoVendedor($pdo, $turno_actual, $_SESSION['user_id']);
    $recaudado_total_dia = isset($recaudado_total[0]['recaudado']) ? $recaudado_total[0]['recaudado'] : 0;

    echo json_encode([
        'delete' => true,
        'recaudado' => $recaudado_total_dia
    ]);


    return;
}


if ($user_id > 0) {
    if ($turno_id > 0) {
        $comando = $pdo->prepare("SELECT * FROM detalleventas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :user_id AND turno = :turno_id $FILTRO $LIMIT");
        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->bindParam(':user_id', $user_id);
        $comando->bindParam(':turno_id', $turno_id);

        if ($filtro_numero > -1) {
            $comando->bindParam(':numero', $filtro_numero);
        }

        $comando->execute();
    } else {
        $comando = $pdo->prepare("SELECT * FROM detalleventas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :user_id $FILTRO $LIMIT");
        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->bindParam(':user_id', $user_id);

        if ($filtro_numero > -1) {
            $comando->bindParam(':numero', $filtro_numero);
        }

        $comando->execute();
    }
} else {
    if ($turno_id > 0) {
        $comando = $pdo->prepare("SELECT * FROM detalleventas WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND turno = :turno_id $FILTRO $LIMIT");
        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);
        $comando->bindParam(':turno_id', $turno_id);

        if ($filtro_numero > -1) {
            $comando->bindParam(':numero', $filtro_numero);
        }

        $comando->execute();
    } else {
        $comando = $pdo->prepare("SELECT * FROM detalleventas WHERE DATE(fecha) BETWEEN :startDate AND :endDate $FILTRO $LIMIT");
        $comando->bindParam(':startDate', $start_date);
        $comando->bindParam(':endDate', $end_date);

        if ($filtro_numero > -1) {
            $comando->bindParam(':numero', $filtro_numero);
        }

        $comando->execute();
    }
}

$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);
$count = count($resultado);

$comandoTotal = $pdo->prepare("SELECT COUNT(1) AS totalVentas FROM ventas");
$comandoTotal->execute();
$recordsTotal = $comandoTotal->fetch(PDO::FETCH_ASSOC);

$json = [
    'data' => $resultado,
    "draw" => $draw,
    "count" => $count,
    "post" => filter_input_array(INPUT_POST),
    "recordsTotal" => intval($recordsTotal['totalVentas']),
    "recordsFiltered" => intval($recordsTotal['totalVentas'])
];

echo json_encode($json);
