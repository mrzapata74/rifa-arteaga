<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1)) {
    header('Location: /index.php');
    return;
}

include_once __DIR__ . '/conexion.php';
include_once __DIR__ . '/includes/cantidadesMaximas.php';

$listaTurnos = getListaTurnos($pdo);
$listaUsuarios = getListaUsuarios($pdo);
$turnoActivoId = getTurnoActivo($pdo);

require('includes/header.php');

?>
<div class="container">
    <br><br><br><br>
    <h4 id="tituloDetalleVentas">Importar ventas al sistema</h4>
    <form class="form-inline">
        <div class="form-group mb-2" id="selUsers">
            <label for="user_select">
                <i class="fas fa-user"></i> Usuario
            </label>
            <select class="form-control form-control-sm" id="user_select">
                <?php
                foreach ($listaUsuarios as $usuario) {
                    $id = $usuario['id'];
                    $nombre = $usuario['nombre'];
                    echo sprintf('<option value="%s">%s</option>\r\n', $id, $nombre);
                }
                ?>
            </select>
            <label for="select_turno">
                <i class="fas fa-user"></i> Turno
            </label>
            <select id="select_turno" class="form-control form-control-sm">
                <?php
                foreach ($listaTurnos as $turno) {
                    $id = $turno['idturno'];
                    $turno = $turno['turno'];
                    echo sprintf('<option %s value="%s">%s</option>\r\n', ($turnoActivoId === $id ? "selected" : ""), $id, $turno);
                }
                ?>
            </select>
            <label>
                <i class="fas fa-user"></i> Link
            </label>
            <input type="text" name="copy_link" id="copy_link">
            <button type="button" class="btn-sm btn-primary" id="pasteJsonLink"><i class="fas fa-paste"></i> Pegar</button>
            <button type="button" class="btn-sm btn-info" id="loadJsonLink"><i class="fas fa-save"></i> Cargar</button>
            <button type="button" class="btn-sm btn-warning" id="checkMaximos"><i class="fas fa-check"></i> Comprobar máximos</button>
        </div>
    </form>

    <div class="container" id="wrappConsol" style="background-color: #007bff;">
        <div class="row" id="filappal"></div>
    </div>
</div>
<script type="text/javascript">
    Array.range = function(n) {
        // Array.range(5) --> [0,1,2,3,4]
        return Array.apply(null, Array(n)).map((x, i) => i)
    };

    Object.defineProperty(Array.prototype, 'chunk', {
        value: function(n) {
            return Array.range(Math.ceil(this.length / n)).map((x, i) => this.slice(i * n, i * n + n));
        }
    });
    var grand_total = 0;

    function importJsonFile() {
        let link = $('#copy_link').val();

        let turno_name = $('#select_turno option:selected').text(),
            user_name = $('#user_select option:selected').text();

        let turno_id = $('#select_turno').val(),
            user_id = $('#user_select').val();

        if (link) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: '¿Estás seguro de cargar estas ventas?',
                html: `Las ventas se cargaran en el turno: <b>${turno_name}</b> para el usuario: <b>${user_name}</b> con un total de <b>C$${grand_total}</b>`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Importar',
                cancelButtonText: 'Cancelar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    // Aciones si
                    $.post('ajax/loadJson.php', {
                        turno_id: turno_id,
                        user_id: user_id,
                        link: link
                    }, function(res) {
                        console.log(res);
                        swalWithBootstrapButtons.fire(
                            '¡Cargado correctamente!',
                            'Las ventas fueron importadas al sistema.',
                            'success'
                        );
                    });
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    swalWithBootstrapButtons.fire(
                        'Cancelado',
                        'Se cancelo el importado de las ventas',
                        'info'
                    )
                }
            });
        }
    };

    $(function() {
        var numberFormat = $.fn.dataTable.render.number(',', '.', 0, '').display;
        $('#pasteJsonLink').click(function() {
            navigator.clipboard.readText()
                .then(text => {
                    document.getElementById("copy_link").value = text;
                })
                .catch(err => {
                    document.getElementById("copy_link").value = 'No se pudo copiar el portapapeles: ' + err;
                });
        });

        $("#checkMaximos").click(function() {
            let link = $('#copy_link').val();

            let turno_id = $('#select_turno').val(),
                user_id = $('#user_select').val();

            $.post('ajax/checkNumeros.php', {
                turno_id: turno_id,
                user_id: user_id,
                link: link
            }, function(res) {
                json = JSON.parse(res);
                let invalid = [];
                json.forEach(function(valid, index) {
                    if (!valid) {
                        invalid.push(`<b>${index}</b>`);
                    }
                });
                let hasInvalid = (invalid.length > 0)
                Swal.fire({
                    title: (!hasInvalid ? 'No se encontraron números que sobrepasen los limites' : `Se encontraron ${invalid.length} números inválidos en el reporte`),
                    html: (!hasInvalid ? `` : 'Los números: ' + invalid.join(',')),
                    icon: (!hasInvalid ? `success` : `error`)
                });
            });
        });

        $('#loadJsonLink').click(function() {
            let link = $('#copy_link').val();
            $('#filappal').html('');
            grand_total = 0;
            console.log(link);
            $.getJSON(link, function(data) {
                console.log(data);
                let ventas = data.data;
                let precio_venta = data.meta.precio_venta;
                let html = [];
                ventas.chunk(10).forEach(chunk => {
                    html.push('<div class="card-columns" id="datoColumna">');
                    html.push('<div class="row" id="filaDatos">');
                    html.push('<div class="col-3"><strong>N°</strong></div>');
                    html.push('<div class="col-9" id="tpremio"><strong>Premio</strong></div>');
                    html.push('</div>');
                    chunk.forEach(venta => {
                        let venta_numero = venta.numero;
                        let venta_dinero = venta.premio * 1000;
                        grand_total += (venta_dinero > 0 ? venta_dinero : 0);
                        let venta_monto = numberFormat(venta_dinero > 0 ? (venta_dinero) : "");

                        html.push('<div class = "card p-5" id = "numCard">');
                        html.push('<div class = "card-body" id = "realData">');
                        html.push('<div class = "row" id = "filaDatos" >');
                        html.push(`<div class='col-3' id='numventa'>${venta_numero}</div>`);
                        html.push(`<div class='col-9' id='montovta'>${venta_monto}</div>`);
                        html.push('</div></div></div>');
                    });
                    html.push('</div>');
                });
                html.push(`<button type="button" class="btn btn-warning" onclick="importJsonFile();"><i class="fas fa-save"></i> Importar</button>`);
                html.push('<br>');
                html.push(`<h5 class="card" style="background-color: red; left: 70%;">El total es: C$${numberFormat(grand_total * (precio_venta / 1000))}</h5>`);
                $('#filappal').html(html.join('\n'));
                $('#filappal').append(`<h5 style="background-color: red;">Precio de venta: ${precio_venta}</h5>`);
            });
        });
    });
</script>
<?php
require('includes/footer.php');
?>