<?php
session_start();
include('conexion.php');

$fecha_hoy = date('Y-m-d', time());
$start_date = isset($_POST['start_date']) && strlen($_POST['start_date']) > 0 ? $_POST['start_date'] : $fecha_hoy;
$end_date = isset($_POST['end_date']) && strlen($_POST['end_date']) > 0 ? $_POST['end_date'] : $fecha_hoy;

$cantidad_corte = isset($_POST['corte']) && strlen($_POST['corte']) > 0 && intval($_POST['corte']) ? $_POST['corte'] : 0;
$turno_id =  isset($_POST['turno_id']) && strlen($_POST['turno_id']) > 0 && intval($_POST['turno_id']) ? $_POST['turno_id'] : 0;
$vendedor_id =  isset($_POST['vendedor_id']) && strlen($_POST['vendedor_id']) > 0 && intval($_POST['vendedor_id']) ? $_POST['vendedor_id'] : 0;

// html chunk size via POST variable or default to 10
$chunk_size = isset($_POST['colums']) && strlen($_POST['colums']) > 0 && intval($_POST['colums']) ? $_POST['colums'] : 10;

$precio_venta = isset($_POST['precio_venta']) && strlen($_POST['precio_venta']) > 0 && floatval($_POST['precio_venta']) ? $_POST['precio_venta'] : 13.00;

if ($turno_id > 0) { // Existe un turno para filtrar
    // Existe un usuario para filtrar, y existe un turno para filtrar
    if ($vendedor_id > 0) {
        $comando = $pdo->prepare("SELECT numero, turno, DATE(fecha) AS fecha, SUM(premioEntero) AS premio, SUM(recaudado) AS recaudado FROM calcular WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND turno = :id_turno AND idvendedor = :idvendedor GROUP BY numero, turno");
        // Como existe un usuario, se filtra por el id del usuario, el id del turno y la fecha de inicio y fin se pasan despues de la consulta
        $comando->bindParam(':idvendedor', $vendedor_id);
    } else { // No existe un usuario para filtrar, pero existe un turno
        $comando = $pdo->prepare("SELECT numero, turno, DATE(fecha) AS fecha, SUM(premioEntero) AS premio, SUM(recaudado) AS recaudado FROM calcular WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND turno = :id_turno GROUP BY numero, turno");
    }

    // Parametros de la consulta

    $comando->bindParam(':startDate', $start_date);
    $comando->bindParam(':endDate', $end_date);
    $comando->bindParam(':id_turno', $turno_id);
    // Ejecutar la consulta
    $comando->execute();
} else { // No existe un turno para filtrar, se mostraran todos los turnos
    // Existe un usuario para filtrar, y se muestran todos los turnos
    if ($vendedor_id > 0) {
        $comando = $pdo->prepare("SELECT numero, turno, DATE(fecha) AS fecha, SUM(premioEntero) AS premio, SUM(recaudado) AS recaudado FROM calcular WHERE DATE(fecha) BETWEEN :startDate AND :endDate AND idvendedor = :idvendedor GROUP BY numero, turno");
        // Como existe un usuario, se filtra por el id del usuario, la fecha de inicio y fin se pasan despues de la consulta
        $comando->bindParam(':idvendedor', $vendedor_id);
    } else {
        // No existe un usuario para filtrar, se muestran todos los turnos
        $comando = $pdo->prepare("SELECT numero, turno, DATE(fecha) AS fecha, SUM(premioEntero) AS premio, SUM(recaudado) AS recaudado FROM calcular WHERE DATE(fecha) BETWEEN :startDate AND :endDate GROUP BY numero, turno");
    }

    // Parametros de la consulta

    $comando->bindParam(':startDate', $start_date);
    $comando->bindParam(':endDate', $end_date);
    // Ejecutar la consulta
    $comando->execute();
}

// array data
$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);

// html buffer variable
$html = [];

// suma total
$grand_total = 0;

// Recaudado suma
$recaudado_total = 0;

$buffer_data = [];
// Prepare buffer data
for ($i = 0; $i < 100; $i++) {
    $buffer_data[] = [
        "numero" => $i,
        "premio" => 0
    ];
}

foreach ($resultado as $venta) {
    $venta_numero = $venta['numero'];
    $venta_monto = ($venta['premio'] - $cantidad_corte);
    $recaudado = $venta['recaudado'];

    if (isset($buffer_data[$venta_numero])) {
        $buffer_data[$venta_numero] = [
            'numero' => $venta_numero,
            'premio' => $venta_monto
        ];
        $grand_total += ($venta_monto > 0 ? $venta_monto : 0);
        $recaudado_total += $recaudado;
    }
}

// Prepare html format
foreach (array_chunk($buffer_data, $chunk_size) as $chunk) { // array data every $chunk_size val
    $html[] = '<div class="card-columns" id="datoColumna">'; // div column
    $html[] = '
    <div class="row" id="filaDatos">
        <div class="col-3"><strong>N°</strong></div>
        <div class="col-9" id="tpremio"><strong>Monto</strong></div>
    </div>';

    // loop array chunk
    foreach ($chunk as $venta) {
        // venta array data
        $venta_numero = $venta['numero'];
        $venta_dinero = $venta['premio'];
        $venta_monto = ($venta_dinero > 0 ? numberFormat($venta_dinero) : "");

        // card of venta data
        $html[] = '
        <div class="card p-5" id="numCard">
            <div class="card-body" id="realData">
                <div class="row" id="filaDatos">';
        $html[] = "<div class='col-3' id='numventa'>$venta_numero</div>";
        $html[] = "<div class='col-9' id='montovta'>$venta_monto</div>";
        $html[] = '</div></div></div>';
    }

    $html[] = '</div>'; // end div column
}

$grand_total_abs = ($grand_total * ($precio_venta / 1000));
$grand_total_format = numberFormat($grand_total_abs > 0 ? $grand_total_abs : 0);

$html[] = "<h5 class='card'>El total es: C$$grand_total_format</h5>";
$html[] = "<h5>El recaudado total es: C$$recaudado_total</h5>";

echo join(PHP_EOL, $html);
