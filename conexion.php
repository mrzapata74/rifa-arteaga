<?php


// Las conexiones sin contraseña son una mala idea.

$pdo = new PDO('mysql:host=localhost;dbname=loto', 'root', '');

date_default_timezone_set('America/Managua'); // Zona horaria del PHP
define("FECHA_HOY", date(DATE_ATOM)); // Se define una constante con el formato correcto de la fecha actual

// Util function
function numberFormat($number)
{
    return number_format($number, 0, '.', ',');
};
