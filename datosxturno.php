<?php
session_start();
include('conexion.php');
require_once __DIR__ . '/includes/cantidadesMaximas.php';

$turno = $_POST['datoTurno'];
$fecha = $_POST['datoFecha'];

$comando = $pdo->prepare("SELECT numero, turno, fecha, sum(premioDig), sum(premioEntero) as premioEntero, sum(recaudado) FROM detalleventas WHERE idvendedor = :vendedorId and turno = :turno and cast(fecha as date) = :fecha GROUP BY numero, turno");
$comando->bindParam(':vendedorId', $_SESSION['user_id']);
$comando->bindParam(':turno', $turno);
$comando->bindParam(':fecha', $fecha);
$comando->execute();
$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);

$user_id = $_SESSION['user_id'];

$merged_data = [];

foreach ($resultado as $index => $value) {
    $numero = $value['numero'];
    $turno = $value['turno'];
    $maximo = cantidadMaxima($pdo, $numero, $turno, $user_id);
    $arr = array_merge($value, ['maximo' => $maximo ? $maximo['cantidadMaxima'] : 0]);
    $merged_data[] = $arr;
}

$datos = json_encode($merged_data);
echo $datos;
