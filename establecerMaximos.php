<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1))
    header('Location: /index.php');

include_once __DIR__ . '/conexion.php';
include_once __DIR__ . '/includes/cantidadesMaximas.php';


require('includes/header.php');

$listaTurnos = getListaTurnos($pdo);
$listaUsuarios = getListaUsuarios($pdo);
$turnoActivoId = getTurnoActivo($pdo);

?>

<div class="container">
    <br>
    <br>
    <br>
    <h4 id="tituloDetalleVentas">Establecer Límites Máximos</h4>
    <form class="form-inline">
        <div class="form-group mb-2" id="selUsers">
            <label for="user_select">
                <i class="fas fa-user"></i> Usuario
            </label>
            <select class="form-control form-control-sm" id="user_select">
                <option value="-1">TODOS</option>
                <?php
                foreach ($listaUsuarios as $usuario) {
                    $id = $usuario['id'];
                    $nombre = $usuario['nombre'];
                    echo sprintf('<option value="%s">%s</option>\r\n', $id, $nombre);
                }
                ?>
            </select>
            <label for="numero_select">
                <i class="fas fa-list-ol"></i> Numero
            </label>
            <select class="form-control form-control-sm" id="numero_select">
                <option value="-1" selected>TODOS</option>
                <?php
                for ($i = 0; $i <= 99; $i++) {
                    echo "<option value=\"$i\">$i</option>\r\n";
                }
                ?>
            </select>
        </div>
    </form>
    <button type="button" class="btn btn-info" id="nuevoMaxmino"><i class="fas fa-plus-circle"></i> Agregar nuevo</button>
    <button type="button" class="btn btn-warning" id="nuevoAll"><i class="fas fa-folder-plus"></i> Agregar todo</button>
    <button type="button" class="btn btn-warning" id="updateTurnos"><i class="fas fa-edit"></i> Actualizar turnos</button>
    <button type="button" class="btn btn-warning" id="updateMontos"><i class="fas fa-list-ol"></i> Actualizar números</button>
    <button type="button" class="btn btn-danger" id="deleteVendedor"><i class="fas fa-trash"></i> Limpiar Vendedor</button>
    <button type="button" class="btn btn-danger" id="deleteAll"><i class="fas fa-trash"></i> Borra Todo</button>
    <table id="tablaMax" class="display responsive nowrap table table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Borrar</th>
                <th>Número</th>
                <th>Máximo</th>
                <th>Usuario</th>
                <th>Turno</th>
            </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>


<script type="text/javascript">
    var table;

    $('#deleteAll').click(function() {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: '¿Estás seguro de borrar TODOS los registros de los números máximos?',
            text: "No podrás revertir esta acción, si quieres borrar todo escribe la palabra 'BORRAR' en el campo de texto.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, borrarlo.',
            cancelButtonText: 'Mantener registros.',
            input: 'text',
            inputValidator: (value) => {
                let confirmar = `BORRAR`;
                console.log(value, confirmar);
                if (!value) {
                    return `Por favor escribe ${confirmar} para confirmar el borrado.`;
                }
                if (confirmar !== value.trim()) {
                    return `La palabra tiene que ser exactamente ${confirmar}. para proceder con el borrado`;
                }
            }
        }).then((result) => {
            if (result.value) {
                // Acciones si 
                $.get('ajaxCantidadesMaximas.php', {
                    p: 'nuke'
                }, function(res) {
                    console.log(res);
                    table.ajax.reload();
                });
                swalWithBootstrapButtons.fire(
                    'Eliminados',
                    'Registros eliminados.',
                    'success'
                ).then((result) => {
                    table.ajax.reload();
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelado',
                    'Operación cancelada',
                    'info'
                )
            }
        });
    });

    $('#deleteVendedor').click(function() {

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        });

        let user_select = $(`#user_select`).val() || -1;

        console.log(`[+] Iniciando borrado del usuario ${user_select}`);
        // Si el usuario es -1, es decir, todos los usuarios seleccionados, no se puede borrar nada
        if (user_select == -1) {
            swalWithBootstrapButtons.fire(
                'Selecciona un usuario',
                'Si quieres borrar todos los registros usa el botón de Borrar todo',
                'info'
            );
            return;
        }

        // Borrado de todos los registros del usuario
        swalWithBootstrapButtons.fire({
            title: '¿Estás seguro de borrar TODOS los registros de este usuario?',
            text: "No podrás revertir esta acción, si quieres borrar sus registros escribe CONFIRMAR",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Borrar Registros',
            cancelButtonText: 'Mantener registros.',
            input: 'text',
            inputValidator: (value) => {
                let confirmar = `CONFIRMAR`;
                console.log(value, confirmar);
                if (!value) {
                    return `Por favor escribe ${confirmar} para confirmar el borrado.`;
                }
                if (confirmar !== value.trim()) {
                    return `La palabra tiene que ser exactamente ${confirmar}. para proceder con el borrado`;
                }
            }
        }).then((result) => {
            if (result.value) {
                // Acciones si 
                $.get('ajaxCantidadesMaximas.php', {
                    p: 'nuke_vendedor',
                    user_select: user_select
                }, function(res) {
                    console.log(res);
                    table.ajax.reload();
                });
                swalWithBootstrapButtons.fire(
                    'Eliminados',
                    'Registros eliminados.',
                    'success'
                ).then((result) => {
                    table.ajax.reload();
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelado',
                    'Operación cancelada',
                    'info'
                )
            }
        });
    });

    $('#updateTurnos').click(function() {
        (async () => {
            const {
                value: formValues
            } = await Swal.fire({
                title: 'Actualizar turno para todos los números y usuarios',
                html: `<select id="swal-input1" class="swal2-select" style="display: flex;">
                    <option value="" disabled="">Selecciona un turno</option>
                    <optgroup label="Turno">
                    <?php
                    foreach ($listaTurnos as $turno) {
                        $id = $turno['idturno'];
                        $turno = $turno['turno'];
                        echo sprintf('<option %s value="%s">%s</option>\r\n', ($turnoActivoId === $id ? "selected" : ""), $id, $turno);
                    }
                    ?>
                    </optgroup>
                    </select>`,
                focusConfirm: false,
                preConfirm: () => {
                    return {
                        turno: document.getElementById('swal-input1').value
                    }
                }
            })

            if (formValues) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'info',
                    title: 'Agregando valores...'
                });
                $.post('ajaxCantidadesMaximas.php?p=updateTurnos', {
                    turno: parseInt(formValues.turno)
                }, function(res) {
                    let json = JSON.parse(res);
                    console.log(json);
                    if (!json.error) {
                        Swal.fire('Turnos actualizados');
                        table.ajax.reload();
                    } else {
                        Swal.fire('Error al añadir el valor.');
                    }
                });
            }
        })();
    });

    $('#updateMontos').click(function() {
        (async () => {
            const {
                value: formValues
            } = await Swal.fire({
                title: 'Actualizar cantidad máxima para todos los turnos y usuarios',
                html: '<input id="swal-input1" class="swal2-input" placeholder="Numero">' +
                    '<input id="swal-input2" class="swal2-input" placeholder="Monto máximo">',
                focusConfirm: false,
                preConfirm: () => {
                    return {
                        numero: document.getElementById('swal-input1').value,
                        monto: document.getElementById('swal-input2').value
                    }
                }
            })

            if (formValues) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'info',
                    title: 'Agregando valores...'
                });
                $.post('ajaxCantidadesMaximas.php?p=updateMontos', {
                    numero: parseInt(formValues.numero),
                    monto: parseInt(formValues.monto)
                }, function(res) {
                    let json = JSON.parse(res);
                    console.log(json);
                    if (!json.error) {
                        Swal.fire('Monto actualizado');
                        table.ajax.reload();
                    } else {
                        Swal.fire('Error al añadir el valor.');
                    }
                });
            }
        })();
    });

    $('#nuevoAll').click(function() {
        (async () => {
            const {
                value: formValues
            } = await Swal.fire({
                title: 'Ingresa un valor máximo',
                html: '<input id="swal-input1" class="swal2-input" placeholder="Monto máximo">' +
                    '<input id="swal-input2" class="swal2-input" placeholder="Numero mínimo: 0 ">' +
                    '<input id="swal-input3" class="swal2-input" placeholder="Numero máximo: 99">' +
                    `<select id="swal-input4" class="swal2-select" style="display: flex;">
                    <option value="" disabled="">Selecciona un usuario</option>
                    <optgroup label="Agregar máximo a">
                    <option value="-1">TODOS</option>
                    <?php
                    foreach ($listaUsuarios as $usuario) {
                        $id = $usuario['id'];
                        $nombre = $usuario['nombre'];
                        echo sprintf('<option value="%s">%s</option>\r\n', $id, $nombre);
                    }
                    ?>
                    </optgroup>
                    </select>`,
                focusConfirm: false,
                preConfirm: () => {
                    return {
                        max: document.getElementById('swal-input1').value,
                        nmin: document.getElementById('swal-input2').value,
                        nmax: document.getElementById('swal-input3').value,
                        user: document.getElementById('swal-input4').value
                    }
                }
            })

            if (formValues) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                Toast.fire({
                    icon: 'info',
                    title: 'Agregando valores...'
                });
                $.post('ajaxCantidadesMaximas.php?p=bulk_nuevo', {
                    maximo: parseInt(formValues.max),
                    nmin: parseInt(formValues.nmin),
                    nmax: parseInt(formValues.nmax),
                    user: parseInt(formValues.user)
                }, function(res) {
                    let json = JSON.parse(res);
                    console.log(json);
                    if (!json.error) {
                        Swal.fire('Datos añadidos.');
                        table.ajax.reload();
                    } else {
                        Swal.fire('Error al añadir el valor.');
                    }
                });
            }
        })();
    });

    $('#nuevoMaxmino').click(function() {
        (async () => {
            const {
                value: formValues
            } = await Swal.fire({
                title: 'Ingresa un valor máximo',
                html: '<input id="swal-input1" class="swal2-input" placeholder="Numero (seperados por coma: 1,3,5,6 ...)">' +
                    '<input id="swal-input2" class="swal2-input" placeholder="Cantidad máxima">' +
                    `<select id="swal-input3" class="swal2-select" style="display: flex;">
                    <option value="" disabled="">Selecciona un turno</option>
                    <optgroup label="Turno">
                    <?php
                    foreach ($listaTurnos as $turno) {
                        $id = $turno['idturno'];
                        $turno = $turno['turno'];
                        echo sprintf('<option %s value="%s">%s</option>\r\n', ($turnoActivoId === $id ? "selected" : ""), $id, $turno);
                    }
                    ?>
                    </optgroup>
                    </select>` +
                    `<select id="swal-input4" class="swal2-select" style="display: flex;">
                    <option value="" disabled="">Selecciona un usuario</option>
                    <optgroup label="Usuario">
                    <?php
                    foreach ($listaUsuarios as $usuario) {
                        $id = $usuario['id'];
                        $nombre = $usuario['nombre'];
                        echo sprintf('<option value="%s">%s</option>\r\n', $id, $nombre);
                    }
                    ?>
                    </optgroup>
                    </select>`,
                focusConfirm: false,
                preConfirm: () => {
                    return {
                        numero: document.getElementById('swal-input1').value,
                        max: document.getElementById('swal-input2').value,
                        turno: document.getElementById('swal-input3').value,
                        usuario: document.getElementById('swal-input4').value
                    }
                }
            })

            if (formValues) {
                let numeros = formValues.numero.split(',');

                for (let number of uniq = [...new Set(numeros)]) {
                    let numero = parseInt(number);
                    console.log(`[+] Agregando ${numero} a las cantidades maximas...`);
                    $.post('ajaxCantidadesMaximas.php?p=nuevo', {
                        numero: numero,
                        maximo: parseInt(formValues.max),
                        turno: parseInt(formValues.turno),
                        usuario: parseInt(formValues.usuario),
                    }, function(res) {
                        let json = JSON.parse(res);
                        console.log(json);
                        if (!json.error) {
                            Swal.fire(`Agregando ${numero} a las cantidades maximas`);
                            table.ajax.reload();
                        } else {
                            Swal.fire(`Error al añadir el numero ${numero}.`);
                        }
                    });
                }

            }
        })();
    });

    function borrarNumero(numero, turno, usuario) {
        console.log(numero, turno, usuario);

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: '¿Estás seguro de borrar esta información?',
            text: "¡No podrás revertir esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, Borrarla!',
            cancelButtonText: 'No, Cancelar!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                // Aciones si 
                $.get('ajaxCantidadesMaximas.php', {
                    p: 'borrar',
                    numero: parseInt(numero),
                    turno: parseInt(turno),
                    usuario: parseInt(usuario)
                }, function(res) {
                    console.log(res);
                    table.ajax.reload();
                });
                swalWithBootstrapButtons.fire(
                    '¡Eliminados!',
                    'Tus registros han sido eliminados.',
                    'success'
                ).then((result) => {
                    table.ajax.reload();
                });
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelado',
                    'Tus datos están seguros :)',
                    'error'
                )
            }
        });
    }

    $(function() {
        var numberFormat = $.fn.dataTable.render.number(',', '.', 2, 'C$').display;


        $("#user_select").change(function() {
            table.ajax.reload();
        });
        $("#numero_select").change(function() {
            table.ajax.reload();
        });

        table = $('#tablaMax').DataTable({
            fixedHeader: true,
            language: {
                "lengthMenu": "Ver _MENU_ Archivos por Página",
                "zeroRecords": "Lo sentimos, no tenemos resultados",
                "info": "Mostrando Página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin Registros para Mostrar",
                "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                },
                "search": "Buscar",
                processing: "Cargando, esto podría tomar unos segundos....",
                buttons: {
                    colvisRestore: 'Restaurar',
                }
            },
            serverSide: true,
            processing: true,
            ordering: false,
            filter: false,
            dom: 'Blfrtip',
            buttons: [{
                extend: 'colvis',
                text: 'Ver/Ocultar',
                postfixButtons: ['colvisRestore']
            }],
            responsive: {
                details: {
                    type: 'column'
                }
            },
            bDestroy: true,
            iDisplayLength: 10,
            order: [
                [1, "asc"]
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Todos"]
            ],
            ajax: {
                url: 'ajaxCantidadesMaximas.php?p=lista',
                type: "GET",
                dataType: "json",
                error: function(e) {
                    console.log(e.responseText);
                },
                data: function(d) {
                    var user = $('#user_select').val();
                    var numero = $('#numero_select').val();

                    d.user = user;
                    d.numero = numero;
                }
            },
            columns: [
                // 0 | Responsive
                {
                    className: 'control noVis notexport',
                    data: null,
                    orderable: false,
                    defaultContent: '',
                    width: "5%",
                },
                // Acciones
                {
                    className: 'notexport',
                    data: null,
                    orderable: false,
                    searchable: false,
                    width: "5%",
                    render: function(url, type, full) {
                        let json = table.ajax.json();
                        let numero = full.numero;
                        let usuarioId = full.usuarioId;
                        let turnoId = full.turnoId;

                        let html = `<button class="btn btn-danger btn-sm" onclick="borrarNumero(${numero}, ${turnoId}, ${usuarioId});"><i class="fas fa-trash"></i></button>`;
                        return html;
                    }
                },
                // Numero
                {
                    name: 'numero',
                    data: 'numero',
                    className: 'table-center',
                    width: "5%",
                },
                // Cantidad Maxima
                {
                    name: 'cantidadMaxima',
                    data: 'cantidadMaxima',
                    render: numberFormat
                },
                // Usuario
                {
                    name: 'usuario',
                    data: 'nombre'
                },
                // Turno
                {
                    name: 'turno',
                    data: 'turno'
                }
            ]
        });
    });
</script>


<?php
require('includes/footer.php');
?>