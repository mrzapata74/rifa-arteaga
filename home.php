<?php
session_start();
include('conexion.php');
require('includes/header.php');

require_once __DIR__ . '/includes/cantidadesMaximas.php';
require_once __DIR__ . '/includes/detallesVentas.php';

if (!isset($_SESSION['user_id'])) {
  header('Location: /index.php');
  return;
}

// SQL Query
$comando = $pdo->prepare("SELECT idturno, turno FROM turnos");
$comando->execute();

// array data 
$lista_turno = $comando->fetchAll(PDO::FETCH_ASSOC);


$turno_actual = getTurnoActivo($pdo);
$recaudado_total = getVentasPorTurnoVendedor($pdo, $turno_actual, $_SESSION['user_id']);
$recaudado_total_dia = isset($recaudado_total[0]['recaudado']) ? $recaudado_total[0]['recaudado'] : 0;

?>


<div class="container" id="wraphome">
  <div class="row" id="contImagen">
    <img src="img/?v=3" alt="">
  </div>

  <form id="frmdatos">
    <hr>
    <div class="align-items-center" id="camposNum">

      <div class="row" id="contHoraTurno">

        <div id="campoTurno">
          <label for="hora" id="labelbold">Turno</label>
          <div class="input-group">
            <div class="input-group-prepend" id="grupoTurno" disabled=true>
              <div class="input-group-text" id="icongpo"><i class="fas fa-sun"></i></i></div>
              <div id="marcoturno">
                Cargando...
              </div>
            </div>
          </div>
        </div>


        <div id="campohora">
          <label for="hora" id="labelbold">Hora</label>
          <div class="input-group">
            <div class="input-group-prepend" id="grupoHora" disabled=true>
              <div class="input-group-text" id="iconoReloj"><i class="far fa-clock"></i></div>
              <div id="marco">
                ...
              </div>

            </div>
          </div>
        </div>
      </div>



      <div id="historialVentas">
        <h6>5 Últimos Ingresos</h6>
        <h6>Total Recaudado: C$<span id="totalRecaudado"><?php echo number_format($recaudado_total_dia, 2); ?></span></h6>
        <table class="table table-striped" id="tblIngreso">
          <thead class="thead-dark">
            <tr>
              <th></th>
              <th>Número</th>
              <th>Monto</th>
            </tr>
          </thead>
          <tbody id="dataIngreso">
          </tbody>
        </table>
        <hr />
        <h6>Máximo del último ingreso</h6>
        <table class="table table-striped" id="ultimo_ingreso_maximo">
          <tbody class="thead-dark" style="font-weight: bold;">
            <tr>
              <td>Número</td>
              <td>...</td>
            </tr>
            <tr>
              <td>Venta Total</td>
              <td>...</td>
            </tr>
            <tr>
              <td>Máximo</td>
              <td style="color: red;">...</td>
            </tr>
            <tr>
              <td>Por vender</td>
              <td>...</td>
            </tr>
          </tbody>
        </table>
      </div>
      <hr>
      <div class="row" id="digiArea">
        <input type="hidden" id="iduser" value="<?php echo $_SESSION['user_id'] ?>">
        <div id="campoNumero">
          <label for="numero" id="labelnumero">Número</label>
          <div class="input-group" id="contgrupnumero">
            <div class="input-group-prepend">
              <div class="input-group-text">#</div>
            </div>
            <input type="number" maxlength="2" min="00" max="99" required class="form-control" id="numero">
          </div>
        </div>
        <div id="campoMoney">
          <label for="permio" id="labelpremio">Monto</label>
          <div class="input-group" id="contgrupnumero">
            <div class="input-group-prepend">
              <div class="input-group-text">C$</div>
            </div>
            <input type="number" oninput="check_zero(this);" name="premio" step="0.01" min="0" required class="form-control" id="premio">
          </div>
        </div>
      </div>
    </div>
    <div class="row" id="contbtnEnviar">

      <button type="submit" id="btnEnviar" class="btn btn-primary btn-lg">Enviar</button>

    </div>
  </form>

  <br />
  <div class="row" id="filabtn">
    <div class="container" id="contbtnventas">
      <!-- Button trigger modal -->
      <div class="btn-group" role="group">
        <button type="button" id="verventas" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
          <i class="fas fa-tags"></i> Ver Ventas
        </button>
        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target=".bd-example-modal-lg">
          <i class="fas fa-user-tag"></i> Ver Mis Ventas
        </button><br>
      </div>
      <button type="button" id="btnDelVta" class="btn btn-danger" data-toggle="modal" data-target=".del-example-modal-lg">
        <i class="fas fa-trash"></i></i> Números Borrados
      </button>
      <div>
        <div class="alert alert-danger" role="alert" id="msgCheckVtas">
          <i class="fas fa-search-dollar" style="font-size:1.2em"></i> <span style="color:red;"><strong>¡OJO Revisa tu detalle de ventas antes de que cierre el turno!</strong></span>
        </div>
        <p>
          © 2024 Copyright:
          <a href="https://mzdevocotal.com/" rel="noreferrer" target="_blank">mzdevocotal.com</a>
        </p>
      </div>
      <br />
    </div>

    <!-- CONSECUTIVO DE INGRESOS DE VENTAS -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="modalmisvtas">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Mis ventas</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group mb-2">
              <label for="turno_select">
                <i class="fas fa-user"></i> Turno
              </label>
              <select class="form-control form-control-sm" id="turno_select">
                <?php
                foreach ($lista_turno as $turno) {
                  $turno_id = $turno['idturno'];
                  $turno_name = $turno['turno'];
                  echo "<option value=\"$turno_id\">$turno_name</option>\r\n";
                }
                ?>
              </select>
              <label for="turno_select">
                <i class="fas fa-calendar"></i> Fecha
              </label>
              <input type="text" class="contanier form-control text-center" name="rangepicker" id="rangeMisvts" />
              <label for="numero_select">
                <i class="fas fa-list-ol"></i> Número
              </label>
              <select class="form-control form-control-sm" id="numero_select">
                <option value="-1" selected>TODOS</option>
                <?php
                for ($i = 0; $i <= 99; $i++) {
                  echo "<option value=\"$i\">$i</option>\r\n";
                }
                ?>
              </select>
            </div>
            <table id="dtMisVentas" class="display responsive nowrap table table-striped">
              <thead>
                <tr>
                  <th></th>
                  <th>Hora</th>
                  <th>#</th>
                  <th>Monto</th>
                </tr>
              </thead>
              <tbody></tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <!--  NUMEROS BORRADOS -->
    <div class="modal fade del-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" id="modalmisvtasborradas">
        <div class="modal-content" id="marcoVtasBorradas">
          <div class="modal-header" style="background:white;">

            <h5 class="modal-title"><i class="fas fa-trash" style="color:red;"></i> <strong>Números Borrados</strong> </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group mb-2">
              <label for="turno_select">
                <i class="fas fa-user"></i> Turno
              </label>
              <select class="form-control form-control-sm" id="turno_select">
                <?php
                foreach ($lista_turno as $turno) {
                  $turno_id = $turno['idturno'];
                  $turno_name = $turno['turno'];
                  echo "<option value=\"$turno_id\">$turno_name</option>\r\n";
                }
                ?>
              </select>
              <input type="text" class="contanier form-control text-center" name="rangepicker" />
            </div>
            <table id="dtMisVentasBorradas" class="display responsive nowrap table table-striped">
              <thead>
                <tr>
                  <th>Hora</th>
                  <th>Número</th>
                  <th>Monto</th>
                </tr>
              </thead>
              <tbody></tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class=" modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" id="cabeceraModal">
            <h5 class="modal-title tituloVentas" id="exampleModalLabel">Ventas</h5>
            <input type="date" id="fechaRep" class="form-control" value="<?php echo date("Y-m-d"); ?>">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <br>


          </div>
          <div class="row" id="resumen">
            <div class="col-6" id="vtaTurno">Turno: <br>C$</div>
            <div class="col-6" id="vtaDia">Venta del Día: <br>C$</div>
          </div>
          <div class="modal-body" id="wrapbtn">
            <div class="row" id="btngroupTurnos">
              <a href="#" class="btn btn-success btn-md" name="1" id="matutino">Matutino</a>
              <a href="#" class="btn btn-primary btn-md" name="3" id="tarde">Tarde</a>
              <a href="#" class="btn btn-secondary btn-md" name="2" id="mdia">Turno Extra</a>
              <a href="#" class="btn btn-info btn-md" name="4" id="noche">Noche</a>
            </div>

            <table class="table table-bordered" id="tablavtas">
              <thead class="thead-dark">
                <tr>
                  <th scope="col" class="aligtxtc">N°</th>
                  <th scope="col" class="aligtxtc">Monto C$</th>
                </tr>
              </thead>
              <tbody id="bodyventas"></tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>

      <a href="#exampleModal" class="float go-up" style="left:3%;">
        <div><i class="fas fa-angle-double-up my-float"></i></div>
      </a>

    </div>



  </div>


  <script type="text/javascript">
    var dtMisVentas;
    var dtMisVentasBorradas;
    var turno_data = {};
    var data_range = {};
    var data_rangedel = {};

    $(function() {
      var data_range_baseconfig = {
        ranges: {
          'Hoy': [moment(), moment()],
          'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
          'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
          'Este Mes': [moment().startOf('month'), moment().endOf('month')],
          'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        locale: {
          "format": "DD/MM/YYYY",
          "separator": " - ",
          "applyLabel": "Aplicar",
          "cancelLabel": "Cancelar",
          "fromLabel": "From",
          "toLabel": "To",
          "customRangeLabel": "Rango Personalizado",
          "daysOfWeek": [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
          ],
          "monthNames": [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
          ],
          "firstDay": 1
        },
      };

      // Date picker
      date_range = $('.bd-example-modal-lg input[name="rangepicker"]').daterangepicker(data_range_baseconfig,
        function(start, end, label) {
          dtMisVentas.ajax.reload();
        });

      data_rangedel = $('.del-example-modal-lg input[name="rangepicker"]').daterangepicker(data_range_baseconfig,
        function(start, end, label) {
          dtMisVentasBorradas.ajax.reload();
        });

      // Mis ventas, filtro numero
      $('.bd-example-modal-lg #numero_select').change(function() {
        dtMisVentas.ajax.reload();
      });
    });

    window.deleteVenta = function(venta_id) {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      swalWithBootstrapButtons.fire({
        title: '¿Estás seguro de borrar este registro?',
        text: "¡No podrás revertir esta acción!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, Borrarlo!',
        cancelButtonText: 'No, Cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          // Acciones si 
          let date_input_format = 'YYYY-MM-DD';
          $.post('obtVtasTotales.php', {
            delete_id: venta_id,
          }, function(res) {
            let json = JSON.parse(res);
            console.log(json);
            // css tabla historial
            let entradas = $('div#historialVentas table tbody tr');
            $.each(entradas, function() {
              let entry = $(this);
              let ventaid = entry.data('ventaid');
              if (venta_id === ventaid) {
                entry.css('color', 'white');
                entry.css('background-color', 'red');

                // Borrar boton de borrado para la venta
                let tr_venta = $(entry).find('.btn-borrar-venta');
                tr_venta.html('');
              }
            });
            // Recaudado
            let recaudado = json.recaudado;
            $(`#totalRecaudado`).html(formatMoney(recaudado));
          });
          swalWithBootstrapButtons.fire(
            '¡Eliminado!',
            'El registro han sido eliminado.',
            'success'
          ).then((result) => {
            try {
              if (dtMisVentas) {
                dtMisVentas.ajax.reload();
              }
            } catch (error) {
              console.log(error);
            }
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Cancelado', 'Tu registro está seguro :)', 'error');
        }
      })
    }

    $(".bd-example-modal-lg #turno_select").change(function() {
      dtMisVentas.ajax.reload();
    });

    $(".del-example-modal-lg #turno_select").change(function() {
      dtMisVentasBorradas.ajax.reload();
    });


    $('.del-example-modal-lg').on('shown.bs.modal', function(e) {
      dtMisVentasBorradas = $('#dtMisVentasBorradas').DataTable({
        fixedHeader: false,
        searching: false,
        language: {
          "lengthMenu": "Ver _MENU_ Archivos por Página",
          "zeroRecords": "Lo sentimos, no tenemos resultados",
          "info": "Mostrando Página _PAGE_ de _PAGES_",
          "infoEmpty": "Sin Registros para Mostrar",
          "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
          processing: "Cargando, esto podría tomar unos segundos....",
          "paginate": {
            "previous": "<",
            "next": ">"
          },
          "search": "Buscar",
          buttons: {
            colvisRestore: 'Restaurar',
          }
        },
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        bDestroy: true,
        paging: true,
        ordering: false,
        info: false,
        ajax: {
          url: 'ventasBorradasVendedor.php',
          type: "POST",
          dataType: "json",
          error: function(e) {
            console.log(e.responseText);
          },
          data: function(d) {
            let turno = parseInt($('.del-example-modal-lg #turno_select').val());
            let date_input_format = 'YYYY-MM-DD';
            let start_date = data_rangedel.data('daterangepicker').startDate.format(date_input_format);
            let end_date = data_rangedel.data('daterangepicker').endDate.format(date_input_format);

            d.turno = turno;
            d.start_date = start_date;
            d.end_date = end_date;
          }
        },
        columns: [
          // 0 | Fecha
          {
            name: 'fecha',
            data: 'fecha',
            render: function(url, type, full) {
              var formated = moment(url, 'YYYY-MM-DD HH:mm:ss');
              return (formated.isValid() ? formated.format('hh:mm:ssa') : url);
            },
            width: "5%"
          },
          // 1 | Numero
          {
            name: 'numero',
            data: 'numero',
            width: "10px"
          },
          // 2 | Premio
          {
            name: 'premio',
            data: 'premio',
            width: "10px"
          }
        ]
      });
    });
    $('.del-example-modal-lg').on('hidden.bs.modal', function(e) {
      dtMisVentasBorradas.destroy();
    });

    $('.bd-example-modal-lg').on('shown.bs.modal', function(e) {
      dtMisVentas = $('#dtMisVentas').DataTable({
        fixedHeader: false,
        searching: false,
        language: {
          "lengthMenu": "Ver _MENU_ Archivos por Página",
          "zeroRecords": "Lo sentimos, no tenemos resultados",
          "info": "Mostrando Página _PAGE_ de _PAGES_",
          "infoEmpty": "Sin Registros para Mostrar",
          "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
          processing: "Cargando, esto podria tomar unos segundos....",
          "paginate": {
            "previous": "<",
            "next": ">"
          },
          "search": "Buscar",
          buttons: {
            colvisRestore: 'Restaurar',
          }
        },
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        bDestroy: true,
        paging: true,
        ordering: false,
        info: false,
        ajax: {
          url: 'ventasVendedor.php',
          type: "POST",
          dataType: "json",
          error: function(e) {
            console.log(e.responseText);
          },
          data: function(d) {
            let turno = parseInt($('#turno_select').val());
            let date_input_format = 'YYYY-MM-DD';
            let start_date = date_range.data('daterangepicker').startDate.format(date_input_format);
            let end_date = date_range.data('daterangepicker').endDate.format(date_input_format);
            let numero = parseInt($('.bd-example-modal-lg #numero_select').val());

            d.turno = turno;
            d.numero = numero;
            d.start_date = start_date;
            d.end_date = end_date;
          }
        },
        columnDefs: [{
          targets: [0],
          width: "5px"
        }],
        columns: [
          // 0 | Acciones
          {
            className: 'notexport',
            data: null,
            orderable: false,
            searchable: false,
            render: function(url, type, full) {
              let venta_id = full.idventa;
              let turno = full.turno;
              let fechaVenta = moment(full.fecha, 'YYYY/MM/DD');
              let fechaHoy = moment();

              let html = `<button class="btn btn-danger btn-sm" onclick="deleteVenta(${venta_id});"><i class="fas fa-trash"></i></button>`;
              return (turno_data.idturno == turno && fechaHoy.isSame(fechaVenta, 'day') && estado_de_bloqueo === 0 ? html : "");
            }
          },
          // 1 | Hora
          {
            name: 'fecha',
            data: 'fecha',
            render: function(url, type, full) {
              var formated = moment(url, 'YYYY-MM-DD HH:mm:ss');
              return (formated.isValid() ? formated.format('hh:mm:ssa') : url);
            },
            width: "5%"
          },
          // 1 | Numero
          {
            name: 'numero',
            data: 'numero',
            width: "10px"
          },
          // 2 | Premio
          {
            name: 'premio',
            data: 'premio',
            width: "10px"
          }
        ]
      });
    });

    $('.bd-example-modal-lg').on('hidden.bs.modal', function(e) {
      dtMisVentas.destroy();
    });
  </script>

  <script>
    var estado_de_bloqueo = 0;

    function check_zero(input) {
      if (input.value == 0) {
        input.setCustomValidity('El numero tiene que ser mayor que 0');
      } else {
        // input is fine -- reset the error message
        input.setCustomValidity('');
      }
    }

    function tiempo() {
      $.ajax({
        url: "hora.php",
        type: "GET",
        success: function(response) {
          document.getElementById("marco").innerHTML = response;
        }
      })
    }

    function loop() {
      $.ajax({
        url: "estados.php",
        type: "GET",
        data: {
          accion: 'home'
        },
        success: function(response) {
          let json = JSON.parse(response);
          let hora = json.hora;
          let turno = json.turno_activo[0];
          let login = json.login;

          turno_data = turno;
          $('#marcoturno').html(turno.turno);
          $("#marco").html(hora);

          if (!login) {
            location.href = '/index.php';
          }

          let bloqueo = parseInt(json.bloqueo);
          // Las entradas se bloquean
          if (bloqueo === 1 && estado_de_bloqueo === 0) {
            estado_de_bloqueo = 1;
            $("#numero").attr('disabled', 'disabled');
            $("#premio").attr('disabled', 'disabled');
            $("#btnEnviar").attr('disabled', 'disabled');
            Swal.fire(
              'Se bloquearon las entradas',
              'Recuerda estar pendiente del restablecimiento',
              'success'
            );

            // Remover botones de borrado de la tabla de ventas
            let table = $('div#historialVentas table tbody');
            $.each(table, function() {
              let entry = $(this);
              let tr_venta = $(entry).find('.btn-borrar-venta');
              tr_venta.html('');
            });
          }
          // Las entradas se desbloquean 
          else if (bloqueo === 0 && estado_de_bloqueo === 1) {
            estado_de_bloqueo = 0;
            $("#numero").removeAttr('disabled');
            $("#premio").removeAttr('disabled');
            $("#btnEnviar").removeAttr('disabled');
          }
        }
      });

    }
    setInterval(function() {
      loop();
    }, 8500);
    loop();

    /* ANIMACIÓN DE BOTON FLOTANTE */
    $(window).scroll(function() {
      if ($(this).scrollTop() > 100) {
        $('.go-up').fadeIn();

      } else {

        $('.go-up').fadeOut();
      }
    });
    $('.go-up').click(function() {
      $('#exampleModal').animate({
        scrollTop: 0
      }, 600);
      return false;
    });



    function turno_activo() {
      $.ajax({
        url: "estados.php",
        type: "GET",
        data: {
          accion: 'turno_activo'
        },
        success: function(response) {
          let json = JSON.parse(response);
          let turno = json.turno_activo[0];
          // console.log(turno);
          turno_data = turno;
          $('#marcoturno').html(turno.turno);
        }
      })
    }

    function estado_bloqueo() {
      $.ajax({
        url: "estados.php",
        type: "GET",
        data: {
          accion: 'bloqueo_activo'
        },
        success: function(response) {
          let json = JSON.parse(response);
          let bloqueo = parseInt(json.bloqueo);
          if (bloqueo === 1 && estado_de_bloqueo === 0) {
            estado_de_bloqueo = 1;
            $("#numero").attr('disabled', 'disabled');
            $("#premio").attr('disabled', 'disabled');
            $("#btnEnviar").attr('disabled', 'disabled');
            Swal.fire(
              'Se bloquearon las entradas',
              'Recuerda estar pendiente del restablecimiento',
              'success'
            )

          } else if (bloqueo === 0 && estado_de_bloqueo === 1) {
            estado_de_bloqueo = 0;
            $("#numero").removeAttr('disabled');
            $("#premio").removeAttr('disabled');
            $("#btnEnviar").removeAttr('disabled');
          }
        }
      });
    }
    document.getElementById("icongpo").innerHTML = `<i class="fas fa-sun" style="color:coral; font-size:1.5em;"></i>`;
  </script>

  <?php
  require('includes/footer.php');
  ?>