



// @see https://stackoverflow.com/questions/149055/how-to-format-numbers-as-currency-string
function formatMoney(number, decPlaces, decSep, thouSep) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSep = typeof decSep === "undefined" ? "." : decSep;
    thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    var sign = number < 0 ? "-" : "";
    var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var j = (j = i.length) > 3 ? j % 3 : 0;

    return sign +
        (j ? i.substr(0, j) + thouSep : "") +
        i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
        (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

// @ see https://stackoverflow.com/questions/2998784/how-to-output-numbers-with-leading-zeros-in-javascript
Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
}

var date_ranger_var = {
    dom: null,
};

$(function () {
    moment.locale('es');

    date_ranger_var.dom = $('input[name="dates"]').daterangepicker({
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
            'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        locale: {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Rango Personalizado",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
    }, function (start, end, label) {
        date_ranger_var.start_date = start;
        date_ranger_var.end_date = end;
        // console.log(start, end, label);
    });

    /* AGREGAR USUARIO */
    $('#frmregistro').submit(function (e) {
        e.preventDefault();
        const postData = {
            nombre: $('#nombre').val(),
            cedula: $('#cedula').val(),
            celular: $('#celular').val(),
            direccion: $('#direccion').val(),
            correo: $('#correo').val(),
            usuario: $('#usuario').val(),
            password: $('#password').val(),
            tipoUsuario: $('#tipoUsuario').val()
        }
        console.log(postData);
        $.post('newuser.php', postData, function (response) {
            $('#frmregistro').trigger('reset');
            console.log(response)
        })
    });


    /* VALIDAR INGRESO */
    $('#frmlogin').submit(function (e) {
        e.preventDefault();
        const dataLogin = {
            usuario: $('#usuario').val(),
            password: $('#password').val()
        }
        console.log("LOS DATOS", dataLogin);
        $.post('ingreso.php', dataLogin, function (response) {
            let dato = JSON.parse(response);
            console.log("El dato devuelto es: ", dato);

            if (dato.login) {
                // el usuario ingreso correcto
                /* alert('Datos correctos'); */
                location.href = './home.php';
            } else {
                let avisar = "";
                avisar = `<div class="alert alert-danger" role="alert"><i class="fas fa-robot"></i> ${dato.mensaje}</div>`
                $('#aviso').html(avisar);

            }
            $('#frmlogin').trigger('reset');

        })
    })

    /* ENVIAR DATOS AL SERVIDOR */
    $('#frmdatos').submit(function (e) {
        e.preventDefault();

        let valorTurno = "";
        let fecha = 0
        if ($('#marcoturno').text() === "Matutino") {
            valorTurno = 1;

        } else if ($('#marcoturno').text() === "Sorteo Extra") {
            valorTurno = 2;

        } else if ($('#marcoturno').text() === "Tarde") {
            valorTurno = 3;

        } else if ($('#marcoturno').text() === "Noche") {
            valorTurno = 4;
        }

        // console.log("EL TURNO ES", $('#marcoturno').text());
        // console.log("PRUEBA DE FECHA", $('#dateFecha').val());

        const dataSend = {
            id: $('#iduser').val(),
            turno: valorTurno,
            numero: $('#numero').val(),
            premio: $('#premio').val()
        }
        console.log(`[+] Datos enviados: `, dataSend);
        $('#frmdatos').trigger('reset');

        $.post('guardardatos.php', dataSend, function (response) {
            let datos = JSON.parse(response);
            console.log(`[+] Data response: `, datos);

            if (datos.data) {
                let table = $('div#historialVentas').find('table#tblIngreso tbody');
                let data = datos.post;
                let premio = data.premio;
                let numero = data.numero;
                let ventaid = datos.venta_id;
                let recaudado = datos.recaudado;
                let precio_venta = datos.precio_venta;
                let venta_actual = datos.venta_actual.total;
                let venta_maxima = (datos.venta_maxima ? datos.venta_maxima.cantidadMaxima : 0);

                let html = `<tr data-ventaid="${ventaid}">
                                <td class="btn-borrar-venta">
                                    <a href="#" class="btn btn-danger btn-sm" onclick="deleteVenta(${ventaid}); return false;"><i class="fas fa-trash"></i></a>
                                </td>
                                <td>${numero}</td>
                                <td>${premio}</td>
                            </tr>`;

                table.append(html);
                if (table.find('tr').length >= 6) {
                    table.find('tr:first').remove();
                }
                // Recaudado total
                $(`#totalRecaudado`).html(formatMoney(recaudado));

                // Total de máximos y venta actual

                // Numero
                $('#ultimo_ingreso_maximo tr:eq(0)').find('td:eq(1)').html(`${numero}`);

                // Venta total
                $('#ultimo_ingreso_maximo tr:eq(1)').find('td:eq(1)').html(`C$${formatMoney(venta_actual)}`);

                // Venta máxima
                $('#ultimo_ingreso_maximo tr:eq(2)').find('td:eq(1)').html(venta_maxima > 0 ? `C$${formatMoney(venta_maxima)}` : 'N/A');

                // Por vender
                $('#ultimo_ingreso_maximo tr:eq(3)').find('td:eq(1)').html(venta_maxima > 0 ? `C$${formatMoney(venta_maxima - venta_actual)}` : 'N/A');
            } else {
                let data = datos;
                let error_message = data.error || 'Imposible añadir el número, intentalo de nuevo.';
                Swal.fire({
                    text: error_message,
                    icon: 'error'
                });
                alert(JSON.stringify(datos));
            }

        }).fail(function (response) {
            Swal.fire({
                text: 'No se puede añadir el número, intentalo de nuevo.',
                icon: 'error'
            });
            console.log(response);
        });
        $('#numero').focus();
    });

    $("#verventas").click(e => {
        e.preventDefault();
        updateventa1();

    })

    function updateventa1() {
        prueba(1);
        ventaxturno(1);
        ventaxdia(1);
    }
    function updateventa2() {
        prueba(2);
        ventaxturno(2);
        ventaxdia(2);
    }
    function updateventa3() {
        prueba(3);
        ventaxturno(3);
        ventaxdia(3);

    }
    function updateventa4() {
        prueba(4);
        ventaxturno(4);
        ventaxdia(4);
    }

    /* DETECTANDO BOTONES PRESIONADOS */
    $("#matutino").click(e => {
        e.preventDefault();
        prueba(1);
        ventaxturno(1);
        ventaxdia(1)

    });

    $("#mdia").click(e => {
        e.preventDefault();
        prueba(2);
        ventaxturno(2);
        ventaxdia(2)
    });

    $("#tarde").click(e => {
        e.preventDefault();
        prueba(3);
        ventaxturno(3);
        ventaxdia(3)
    });

    $("#noche").click(e => {
        e.preventDefault();
        prueba(4);
        ventaxturno(4);
        ventaxdia(4);
    });

    /*    $("#fechaRep").change(function(){
           prueba(dato);
       }) */

    function prueba(dato) {
        const datoTurno = dato
        const datoFecha = $('#fechaRep').val();
        /*         console.log(dato, datoFecha); */

        $.post('datosxturno.php', { datoTurno, datoFecha }, function (response) {
            /*             console.log("A VER QUE PASA", response); */
            let datos = JSON.parse(response);


            let plantilla = '';
            var NumerosValidos = [];
            datos.forEach(item => {
                NumerosValidos[item.numero] = item;
            });

            for (let index = 0; index <= 99; index++) {
                if (NumerosValidos[index]) {
                    let numero = parseInt(NumerosValidos[index].numero);
                    let dinero = parseFloat(NumerosValidos[index].premioEntero);
                    let maximo = parseFloat(NumerosValidos[index].maximo);

                    console.log(numero, dinero);

                    plantilla += `<tr><td class="aligtxtc" style="background-color: aqua; font-weight:bold">${numero.pad(2)}</td><td class="aligtxtd" style="background-color: aqua; font-weight:bold">C$${formatMoney(dinero)}${maximo > 0 ? ' / <span style=\'color:red; font-weight:bold;\'>C$' + formatMoney(maximo) + '</span>' : ''}</td></tr>`
                } else {
                    plantilla += `<tr><td class="aligtxtc">${index.pad(2)}</td><td style="color: red; font-weight:bold;text-align:center">Sin Venta</td></tr>`
                }
            }
            $('#bodyventas').html(plantilla);
        })
    }



    /* VENTAS POR CADA TURNO, FECHA Y VENDEDOR */
    function ventaxturno(valor) {
        const datoTurno = valor
        const datoFecha = $('#fechaRep').val();
        /* console.log(dato, datoFecha); */

        $.post('ventaxturnoyvendedor.php', { datoTurno, datoFecha }, function (response) {
            /*             console.log("Ojalá", response); */
            let datos = JSON.parse(response);

            let recaudo = '';
            if (datos.length > 0) {
                datos.forEach(money => {
                    recaudo += `
                                <span class="aligtxtc">
                                    Sorteo : ${money.turno} <br>
                                    C$ ${formatMoney(money.recaudado)}
                                </span>
             
                                 `
                });
                $('#vtaTurno').html(recaudo);
            } else {
                /*     datos.forEach(money => { */
                recaudo += `
                                <span class="aligtxtc">
                                Turno : ${datoTurno} <br>
                                    C$ 0.00
                                </span>
             
                                 `
                /*              }); */
                $('#vtaTurno').html(recaudo);
            }


        })
    }

    /* VENTAS POR DIA */
    function ventaxdia(data) {
        const datoTurno = data
        const datoFecha = $('#fechaRep').val();

        $.post('ventasxdia.php', { datoTurno, datoFecha }, function (response) {
            console.log("Dia", response);
            let datos = JSON.parse(response);

            let recaudodia = '';
            if (datos.length > 0) {
                datos.forEach(money => {
                    recaudodia += `
                                <span>
                                    Venta del Día<br>
                                    C$ ${formatMoney(money.recaudadodia)}
                                </span>
             
                                 `
                });
                $('#vtaDia').html(recaudodia);
            } else {
                recaudodia += `
                <span>
                    Venta del Día<br>
                    C$ 0.00
                </span>

                 `

                $('#vtaDia').html(recaudodia);
            }


        })
    }


    function ventaGlobal() {
        // $('#rptDetVtas').DataTable();
        //  $('#rptDetVtas').DataTable().fnDestroy();
        $.ajax({
            url: "obtVtasTotales.php",
            type: "GET",
            success: function (response) {
                let vtaTotales = JSON.parse(response);
                let datoglobal = "";
                if (vtaTotales.length) {
                    vtaTotales.forEach(vtas => {
                        datoglobal += `<tr>
                                    <td id="dtoCenter">${vtas.fecha}</td>
                                    <td id="dtoCenter">${vtas.idvendedor}</td>
                                    <td>${vtas.nombre}</td>
                                    <td id="dtoCenter">${vtas.turno}</td>
                                    <td id="dtoCenter">${vtas.numero}</td>
                                    <td id="dpremio">${$.fn.dataTable.render.number(',', '.', 2, '').display(vtas.premioEntero)}</td>
                                    <td id="ddinero">${$.fn.dataTable.render.number(',', '.', 2, '').display(vtas.recaudado)}</td>
                                  </tr>`
                        $("#bodyVtas").html(datoglobal);
                    })
                    /*                     $('#rptDetVtas').DataTable({
                                            "language": {
                                                "lengthMenu": "Ver _MENU_ Archivos por Página",
                                                "zeroRecords": "Lo sentimos, no tenemos resultados",
                                                "info": "Mostrando Página _PAGE_ de _PAGES_",
                                                "infoEmpty": "Sin Registros para Mostrar",
                                                "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
                                                "paginate": {
                                                    "previous": "Anterior",
                                                    "next": "Siguiente"
                                                },
                                                "search": "Buscar",
                                            },
                                            orderCellsTop: true,
                                            fixedHeader: true,
                                                                    "columnDefs":[
                                                                        { 
                                                                            "targets": 1,
                                                                            "width": "65px"
                                                                        },
                                                                        { 
                                                                            "targets": 2,
                                                                            "width": "150px"
                                                                        },
                                                                        { 
                                                                            "targets": 4,
                                                                            "width": "50px"
                                                                        },
                                                                        { 
                                                                            "targets": 6,
                                                                            "width": "200px"
                                                                        },
                                            
                                                                    ],
                                            "footerCallback": function (row, data, start, end, display) {
                                                var api = this.api(), data;
                    
                                                // Remove the formatting to get integer data for summation
                                                var intVal = function (i) {
                                                    return typeof i === 'string' ?
                                                        i.replace(/[\$,]/g, '') * 1 :
                                                        typeof i === 'number' ?
                                                            i : 0;
                                                };
                    
                                                // Total over all pages
                                                total = api
                                                    .column(6)
                                                    .data()
                                                    .reduce(function (a, b) {
                                                        return intVal(a) + intVal(b);
                                                    }, 0);
                    
                                                // Total over this page
                                                pageTotal = api
                                                    .column(6, { page: 'current' })
                                                    .data()
                                                    .reduce(function (a, b) {
                                                        return intVal(a) + intVal(b);
                                                    }, 0);
                    
                                                // Update footer
                                                $(api.column(6).footer()).html(
                                                    'C$' + formatMoney(pageTotal) + ' ( C$' + formatMoney(total) + ')'
                                                );
                                            },
                                            "initComplete": function (settings, json) {
                                                this.api().columns([0, 2]).every(function () {
                                                    var column = this;
                                                    var select = $('<select><option value=""></option></select>')
                                                        .appendTo($(column.footer()).empty())
                                                        .on('change', function () {
                                                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                                                        });
                                                    column.data().unique().sort().each(function (d, j) {
                                                        select.append('<option value="' + d + '">' + d + '</option>');
                                                    });
                                                });
                                            }
                                        }); */

                    /* AGREGANDO FILTROS DE BÚSQUEDA A LA TABLA */
                    $('#rptDetVtas thead tr').clone(true).appendTo('#rptDetVtas thead');
                    $('#rptDetVtas thead tr:eq(1) th').each(function (i) {
                        var title = $(this).text();
                        $(this).html('<input type="text" style="width:100%" placeholder="Buscar... " />');

                        $('input', this).on('keyup change', function () {
                            if ($('#rptDetVtas').DataTable().column(i).search() !== this.value) {
                                $('#rptDetVtas').DataTable()
                                    .column(i)
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    });
                }
            }
        })
    }

    // ventaGlobal();


    /*     $(document).ready(function() {
            $('#rptDetVtas').DataTable({
                "language": {
                    "lengthMenu": "Ver _MENU_ Archivos por Página",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing page _PAGE_ of _PAGES_",
                    "infoEmpty": "Sin Registros para Mostrar",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                }
            });
        }); */



})/* AQUI TERMINA FUNCION AUTOEJECUTABLE */



function ver() {
    let hora = $("#marco").text();
    alert(hora);
}

/* CONSULTA BOTONES ADMINISTRADORES */


function bloqueo(txtturno) {
    const textturno = txtturno;
    console.log("EL TURNO VISTO", txtturno);

    $.post("bloqueo.php", { textturno }, function (response) {
        let datoTurno = JSON.parse(response);
        console.log("EL TURNO DEVUELTO ES ", datoTurno);

        let dTurno = '';
        if (datoTurno) {
            datoTurno.forEach(turno => {
                dTurno += `${turno.turno}`
            });
            $('#marcoturno').html(dTurno);
        } else {
            dTurno += `No Definido`
            $('#marcoturno').html(dTurno);
        }
    })


}


$("#borrarvtas").click(e => {
    e.preventDefault();
    $.post("borrar.php", {}, function (response) {
        let respuesta = JSON.parse(response);
        Swal.fire('Datos Eliminados Correctamente')


    })

})




