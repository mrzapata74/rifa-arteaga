<?php
session_start();

if (!isset($_SESSION['user_id']))
    header('Location: /index.php');

require('includes/header.php');

?>

<div class="container">
    <br>
    <br>
    <br>
    <h4 id="tituloDetalleVentas">Establecer Límites Máximos generales</h4>
    <button type="button" class="btn btn-info" id="nuevoMaxmino"><i class="fas fa-plus-circle"></i> Agregar nuevo</button>
    <button type="button" class="btn btn-danger" id="deleteAll"><i class="fas fa-trash"></i> Borra Todo</button>
    <table id="tablaMax" class="display responsive nowrap table table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Acciones</th>
                <th>Número</th>
                <th>Máximo</th>
            </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>


<script type="text/javascript">
    var table;

    // Botón borrar todo
    $('#deleteAll').click(function() {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: '¿Estás seguro de borrar esta información?',
            text: "¡No podrás revertir esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, Borrarla!',
            cancelButtonText: 'No, Cancelar!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                // Aciones si 
                $.get('ajax/ajaxCantidadesMaximasGenerales.php', {
                    p: 'nuke'
                }, function(res) {
                    console.log(res);
                    table.ajax.reload();
                });
                swalWithBootstrapButtons.fire(
                    '¡Eliminados!',
                    'Tus registros han sido eliminados.',
                    'success'
                ).then((result) => {
                    table.ajax.reload();
                });
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelado',
                    'Tus datos están seguros :)',
                    'error'
                )
            }
        });
    });

    // Botón nuevo
    $('#nuevoMaxmino').click(function() {
        (async () => {
            const {
                value: formValues
            } = await Swal.fire({
                title: 'Ingresa un valor maximo',
                html: '<input id="swal-input1" class="swal2-input" placeholder="Numero">' +
                    '<input id="swal-input2" class="swal2-input" placeholder="Cantidad maxima">',
                focusConfirm: false,
                preConfirm: () => {
                    return {
                        numero: document.getElementById('swal-input1').value,
                        max: document.getElementById('swal-input2').value
                    }
                }
            })

            if (formValues) {
                $.post('ajax/ajaxCantidadesMaximasGenerales.php?p=nuevo', {
                    numero: parseInt(formValues.numero),
                    maximo: parseInt(formValues.max)
                }, function(res) {
                    let json = JSON.parse(res);
                    console.log(json);
                    if (!json.error) {
                        Swal.fire('Datos añadido.');
                        table.ajax.reload();
                    } else {
                        Swal.fire('Error al añadir el valor.');
                    }
                });

            }
        })();
    });

    // Función de borrado de numero
    function borrarNumero(numero) {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: '¿Estás seguro de borrar esta información?',
            text: "¡No podrás revertir esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, Borrarla!',
            cancelButtonText: 'No, Cancelar!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                // Acciones si 
                $.get('ajax/ajaxCantidadesMaximasGenerales.php', {
                    p: 'borrar',
                    numero: parseInt(numero)
                }, function(res) {
                    console.log(res);
                    table.ajax.reload();
                });
                swalWithBootstrapButtons.fire(
                    '¡Eliminados!',
                    'Tus registros han sido eliminados.',
                    'success'
                ).then((result) => {
                    table.ajax.reload();
                });
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelado',
                    'Tus datos están seguros :)',
                    'error'
                )
            }
        });
    }

    $(function() {
        var numberFormat = $.fn.dataTable.render.number(',', '.', 2, 'C$').display;

        table = $('#tablaMax').DataTable({
            fixedHeader: true,
            language: {
                "lengthMenu": "Ver _MENU_ Archivos por Página",
                "zeroRecords": "Lo sentimos, no tenemos resultados",
                "info": "Mostrando Página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin Registros para Mostrar",
                "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                },
                "search": "Buscar",
                buttons: {
                    colvisRestore: 'Restaurar',
                }
            },
            aProcessing: true,
            aServerSide: true,
            dom: 'Blfrtip',
            buttons: [{
                extend: 'colvis',
                text: 'Ver/Ocultar',
                postfixButtons: ['colvisRestore']
            }],
            responsive: {
                details: {
                    type: 'column'
                }
            },
            bDestroy: true,
            iDisplayLength: 10,
            order: [
                [1, "asc"]
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Todos"]
            ],
            ajax: {
                url: 'ajax/ajaxCantidadesMaximasGenerales.php?p=lista',
                type: "GET",
                dataType: "json",
                error: function(e) {
                    console.log(e.responseText);
                }
            },
            columns: [
                // 0 | Responsive
                {
                    className: 'control noVis notexport',
                    data: null,
                    orderable: false,
                    defaultContent: '',
                    width: "5%",
                },
                // Acciones
                {
                    className: 'notexport',
                    data: null,
                    orderable: false,
                    searchable: false,
                    width: "5%",
                    render: function(url, type, full) {
                        let numero = full.numero;

                        let html = `<button class="btn btn-danger btn-sm" onclick="borrarNumero(${numero});"><i class="fas fa-trash"></i></button>`;
                        return html;
                    }
                },
                // Numero
                {
                    name: 'numero',
                    data: 'numero',
                    className: 'table-center',
                    width: "5%",
                },
                // Cantidad Maxima
                {
                    name: 'cantidadMaxima',
                    data: 'cantidadMaxima',
                    render: numberFormat
                }
            ]
        });
    });
</script>


<?php
require('includes/footer.php');
?>