<?php
session_start();
include('conexion.php');


// SQL Query
$comando = $pdo->prepare("SELECT id, codusuario, imagen, nombre, usuario, cedula, telefono, direccion, tipo, tipo_id, correo, precio_venta FROM listausuarios");
$comando->execute();

// array data
$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);

$json = [
    'data' => $resultado
];

echo json_encode($json);
