<?php
session_start();
include('conexion.php');

// Variable POST para referenciar el id del turno que se llama.
$idturno = $_POST['textturno'];

// Trae la infamacion del turno.
$comando = $pdo->prepare("SELECT idturno, turno, rango FROM turnos WHERE idturno = :turnoId");
$comando->bindParam(':turnoId', $idturno);
$comando->execute();
$resultado = $comando->fetchAll(PDO::FETCH_ASSOC);


$dataTurno = json_encode($resultado);
echo $dataTurno; 

?>