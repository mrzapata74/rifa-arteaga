<?php

include_once __DIR__ . '/conexion.php';
include_once __DIR__ . '/includes/cantidadesMaximas.php';

$action = filter_input(INPUT_GET, 'p');

$json = [];

$uri = filter_input(INPUT_SERVER, 'REQUEST_URI');
$url = parse_url($uri, PHP_URL_QUERY);
parse_str($url, $query);


switch ($action) {
    case 'lista':
        $start = isset($query['start']) ? intval($query['start']) : 0;
        $length = isset($query['length']) ? intval($query['length']) : 25;
        $draw = isset($query['draw']) ? intval($query['draw']) : 0;

        $user = isset($query['user']) ? intval(trim($query['user'])) : -1;
        $numero = isset($query['numero']) ? intval(trim($query['numero'])) : -1;

        $recordsTotal = getCountCantidadesMaximas($pdo);
        $lista = listaCantidadesMaximas($pdo, $start, $length, $user, $numero);
        $count = count($lista);
        $structure = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            // "recordsFiltered" => ($user >= 0 || $numero >= 0 ? $count : $recordsTotal),
            "recordsFiltered" => $recordsTotal,
            "count" => $count,
            'data' => $lista
        ];
        $json = $structure;
        break;

    case 'borrar':
        $numero = filter_input(INPUT_GET, 'numero', FILTER_VALIDATE_INT);
        $turno = filter_input(INPUT_GET, 'turno', FILTER_VALIDATE_INT);
        $usuario = filter_input(INPUT_GET, 'usuario', FILTER_VALIDATE_INT);
        borrarCantidadMaxima($pdo, $numero, $turno, $usuario);
        $json = [
            'status' => 200
        ];
        break;

    case 'nuke':
        limpiarCantidadMaxima($pdo);
        break;

    case 'nuke_vendedor':
        $usuario = filter_input(INPUT_GET, 'user_select', FILTER_VALIDATE_INT);
        limpiarCantidadMaximaVendedor($pdo, $usuario);
        break;

    case 'nuevo':
        $numero = filter_input(INPUT_POST, 'numero', FILTER_VALIDATE_INT);
        $maximo = filter_input(INPUT_POST, 'maximo', FILTER_VALIDATE_INT);
        $turno = filter_input(INPUT_POST, 'turno', FILTER_VALIDATE_INT);
        $usuario = filter_input(INPUT_POST, 'usuario', FILTER_VALIDATE_INT);
        if ($numero >= 0 && $maximo >= 0 && $turno >= 0 && $usuario >= 0) {
            if (nuevoCantidadMaxima($pdo, $numero, $maximo, $turno, $usuario)) {
                $json = [
                    'status' => 200
                ];
            } else {
                $json = [
                    'error' => 2
                ];
            }
        } else {
            $json = [
                'error' => 1
            ];
        }
        break;
    case 'bulk_nuevo':
        $maximo = filter_input(INPUT_POST, 'maximo', FILTER_VALIDATE_INT);
        $nmin = filter_input(INPUT_POST, 'nmin', FILTER_VALIDATE_INT) ?: 0;
        $nmax = filter_input(INPUT_POST, 'nmax', FILTER_VALIDATE_INT) ?: 99;
        $user = filter_input(INPUT_POST, 'user', FILTER_VALIDATE_INT) ?: -1;

        if ($maximo >= 1) {
            if (bulkCantidadMaxima($pdo, $maximo, $nmin, $nmax, $user)) {
                $json = [
                    'status' => 200
                ];
            } else {
                $json = [
                    'error' => 2
                ];
            }
        } else {
            $json = [
                'error' => 1
            ];
        }
        break;
    case 'updateTurnos':
        $turno = filter_input(INPUT_POST, 'turno', FILTER_VALIDATE_INT);
        if ($turno) {
            updateTurnos($pdo, $turno);
        }
        break;
    case 'updateMontos':
        $numero = filter_input(INPUT_POST, 'numero', FILTER_VALIDATE_INT);
        $monto = filter_input(INPUT_POST, 'monto', FILTER_VALIDATE_INT);
        if ($numero !== null && $monto >= 1) {
            updateMontos($pdo, $numero, $monto);
            $json = [
                $numero, $monto
            ];
        }
        break;
    default:
        $json = [
            'error' => 404
        ];
        break;
}

echo json_encode($json, JSON_PRETTY_PRINT);
