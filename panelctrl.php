<?php
session_start();
if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1))

  header('Location: /index.php');
include('conexion.php');
require('includes/header.php');

// SQL Query
$comando = $pdo->prepare("SELECT id, nombre FROM listausuarios");
$comando->execute();

// array data 
$user_list = $comando->fetchAll(PDO::FETCH_ASSOC);

?>

<h4 id="titleAdmon">Administrar</h4>
<!-- Accordion  -->
<div class="container" id="wrappanel">
  <div class="accordion" id="accordionExample">
    <div class="card">
      <div class="card-header" id="headingOne">
        <h2 class="mb-0">
          <button class="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <i class="fas fa-tools"></i> <strong>
              ESTABLECER TURNOS
            </strong>
          </button>
        </h2>
      </div>

      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body" id="cuerpoEsTurnos">
          <!-- DATOS PARA MOSTRAR -->
          <div class="card" id="cardTarjetas">
            <div class="card-header">
              <!--  <strong>ESTABLECER TURNOS</strong> -->
            </div>
            <div id="campoTurno">
              <label for="hora" id="labelbold">Turno</label>
              <div class="input-group">
                <div class="input-group-prepend" id="grupoTurno" disabled=true>
                  <div class="input-group-text" id="icongpo"><i class="fas fa-sun"></i></i></div>
                  <div id="marcoturno">
                    Cargando...
                  </div>
                </div>
              </div>
            </div>
            <div id="campohora">
              <label for="hora" id="labelbold">Hora</label>
              <div class="input-group">
                <div class="input-group-prepend" id="grupoHora" disabled=true>
                  <div class="input-group-text" id="iconoReloj"><i class="far fa-clock"></i></div>
                  <div id="marco">
                    ...
                  </div>

                </div>
              </div>
            </div>
            <div class="card-body" id="cardcuerpo">
              <h5 class="card-title"></h5>
              <!-- BOTONES PADA ADMINSTRAR TURNOS -->
              <div class="row" id="btngroupTurnos">
                <a href="#" class="btn btn-success btn-md" name="1" id="BtnmatutinoAd">Matutino</a>
                <a href="#" class="btn btn-primary btn-md" name="3" id="BtntardeAd">Tarde</a>
                <a href="#" class="btn btn-secondary btn-md" name="2" id="BtnmdiaAd">Turno Extra</a>
                <a href="#" class="btn btn-info btn-md" name="4" id="BtnnocheAD">Noche</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header" id="headingTwo">
        <h2 class="mb-0">
          <button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <i class="fab fa-expeditedssl"></i>
            <strong>
              BLOQUEAR ENTRADAS
            </strong>
          </button>
        </h2>
      </div>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
        <div class="card-body" id="cardcuerpo">


          <div class="card" id="cardTarjetas">
            <div class="card-header">
              <!--      <strong>BLOQUEAR ENTRADAS</strong> -->
            </div>
            <div class="card-body" id="cardcuerpobloq">
              <h5 class="card-title">Estado Actual </h5>
              <div class="row" id="contbloqueo">
                <span id="estadoVis"></span>
              </div>

              <div class="btn-group btn-group-toggle" data-toggle="buttons" id="grupBtn">
                <label class="btn btn-secondary active">
                  <input type="radio" name="options" id="bloquear"> BLOQUEADO
                </label>
                <label class="btn btn-secondary">
                  <input type="radio" name="options" id="desbloquear"> DESBLOQUEADO
                </label>
              </div>
            </div>
          </div>
          <hr>
          <div class="card">
            <div class="card-body">
              <h5 class="card-title"><i class="fab fa-expeditedssl"></i> Bloqueos automáticos activados</h5>
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Turno</th>
                    <th scope="col">Hora de bloqueo</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Matutino</th>
                    <td>11:00 am</td>
                  </tr>
                  <tr>
                    <th scope="row">Tarde</th>
                    <td>3:00 pm</td>
                  </tr>
                  <tr>
                    <th scope="row">Noche</th>
                    <td>9:00 pm</td>
                  </tr>
                  <tr>
                    <th scope="row">Turno Extra</th>
                    <td>6:00 pm (Martes y Sábado)</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>




        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header" id="headingThree">
        <h2 class="mb-0">
          <button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <i class="fas fa-backspace"></i>
            <strong>
              REVERTIR VENTA
            </strong>
          </button>
          </button>
        </h2>
      </div>
      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
        <div class="card-body">
          <form id="frmdatos">
            <div class="row">
              <div class="row" id="wrapseluser">
                <div class="form-group mb-2" id="selUsers">
                  <label for="iduser">
                    <i class="fas fa-user"></i> Usuario
                  </label>
                  <select class="form-control form-control-sm" id="iduser">
                    <?php
                    foreach ($user_list as $user) {
                      $user_id = $user['id'];
                      $user_name = $user['nombre'];
                      echo "<option value=\"$user_id\">$user_name</option>\r\n";
                    }
                    ?>
                  </select>
                </div>
              </div>

              <br>
              <br>
              <div class="row">
                <div id="campoNumero">
                  <label for="numero" id="labelnumero">Número</label>
                  <div class="input-group" id="contgrupnumero">
                    <div class="input-group-prepend">
                      <div class="input-group-text">#</div>
                    </div>
                    <input type="number" maxlength="2" min="00" max="99" required class="form-control" id="numero">
                  </div>
                </div>
                <div id="campoMoney">
                  <label for="permio" id="labelpremio">Monto</label>
                  <div class="input-group" id="contgrupnumero">
                    <div class="input-group-prepend">
                      <div class="input-group-text">C$</div>
                    </div>
                    <input type="number" name="premio" step="0.01" required class="form-control" id="premio">
                  </div>
                </div>
              </div>

              <div class="row" id="contbtnEnviar">
                <button type="submit" id="btnEnviar" class="btn btn-primary btn-lg">Enviar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- End Accordion -->











<!-- 
<div class="container" id="wrappanel">
  <div class="card" id="cardTarjetas">
    <div class="card-header">
      <strong>ESTABLECER TURNOS</strong>
    </div>
    <div id="campoTurno">
      <label for="hora" id="labelbold">Turno</label>
      <div class="input-group">
        <div class="input-group-prepend" id="grupoTurno" disabled=true>
          <div class="input-group-text" id="icongpo"><i class="fas fa-sun"></i></i></div>
          <div id="marcoturno">
            Cargando...
          </div>
        </div>
      </div>
    </div>
    <div id="campohora">
      <label for="hora" id="labelbold">Hora</label>
      <div class="input-group">
        <div class="input-group-prepend" id="grupoHora" disabled=true>
          <div class="input-group-text" id="iconoReloj"><i class="far fa-clock"></i></div>
          <div id="marco">
            ...
          </div>

        </div>
      </div>
    </div>
    <div class="card-body" id="cardcuerpo">
      <h5 class="card-title"></h5>
      BOTONES PADA ADMINSTRAR TURNOS
      <div class="row" id="btngroupTurnos">
        <a href="#" class="btn btn-success btn-md" name="1" id="BtnmatutinoAd">Matutino</a>
        <a href="#" class="btn btn-secondary btn-md" name="2" id="BtnmdiaAd">Medio Día</a>
        <a href="#" class="btn btn-primary btn-md" name="3" id="BtntardeAd">Tarde</a>
        <a href="#" class="btn btn-info btn-md" name="4" id="BtnnocheAD">Noche</a>
      </div>
    </div>
  </div>

  <div class="card" id="cardTarjetas">
    <div class="card-header">
      <strong>BLOQUEAR ENTRADAS</strong>
    </div>
    <div class="card-body" id="cardcuerpobloq">
      <h5 class="card-title">Estado Actual </h5>
      <span id="estadoVis"></span>

      <div class="btn-group btn-group-toggle" data-toggle="buttons">
        <label class="btn btn-secondary active">
          <input type="radio" name="options" id="bloquear"> BLOQUEADO
        </label>
        <label class="btn btn-secondary">
          <input type="radio" name="options" id="desbloquear"> DESBLOQUEADO
        </label>
      </div>
    </div>
  </div>


</div> -->


<script type="text/javascript">
  var estado_de_bloqueo = 0;

  $(document).ready(function() { // hasta que cargue el dom
    estado_bloqueo();
    let establecer_turno = function(id_turno) {
      $.ajax({
        url: "estados.php",
        type: "GET",
        data: {
          accion: 'establecer_turno',
          turno_id: id_turno
        },
        success: function(response) {
          let json = JSON.parse(response);

          Swal.fire({
            title: 'El turno fue cambiado exitosamente',
            showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            }
          })


          /*         alert(`El turno fue cambiado`); */
        }
      })
    };
    $("#BtnmatutinoAd").click(e => {
      e.preventDefault();
      establecer_turno(1);
    })
    $("#BtnmdiaAd").click(e => {
      e.preventDefault();
      establecer_turno(2);
    })
    $("#BtntardeAd").click(e => {
      e.preventDefault();
      establecer_turno(3);
    })
    $("#BtnnocheAD").click(e => {
      e.preventDefault();
      establecer_turno(4);
    })

    $("#bloquear").click(e => {
      $.ajax({
        url: "estados.php",
        type: "GET",
        data: {
          accion: 'establecer_bloqueo_bloquear'
        },
        success: function(response) {
          let json = JSON.parse(response);
          console.log("Estado Actual", json.bloquedo);
          $("#estadoVis").css({
            'background-color': 'red',
            'color': 'white',
            'padding': '5px',
            'text-transform': 'uppercase',
            'font-size': '1.5em'
          }).html(json.bloquedo);

          Swal.fire(
            'Se bloquearon las entradas',
            'Recuerda verificar siempre el estado de bloqueos',
            'success'
          )

          /* alert(`Se bloquearon las entradas`); */
        }
      });
    })
    $("#desbloquear").click(e => {
      $.ajax({
        url: "estados.php",
        type: "GET",
        data: {
          accion: 'establecer_bloqueo_desbloquear'
        },
        success: function(response) {
          let json = JSON.parse(response);
          $("#estadoVis").css({
            'background-color': 'green',
            'color': 'white',
            'padding': '5px',
            'text-transform': 'uppercase',
            'font-size': '1.5em'
          }).html(json.bloquedo);
          console.log("Estado Actual:", json.bloquedo);
          Swal.fire(
            'Se debloquearon las entradas',
            'Recuerda verificar siempre el estado de bloqueos',
            'success'
          )
        }
      });
    })

  });

  function tiempo() {
    $.ajax({
      url: "hora.php",
      type: "GET",
      success: function(response) {
        document.getElementById("marco").innerHTML = response;
      }
    })
  }

  function loop() {
    setInterval(function() {
      tiempo();
      turno_activo();
    }, 1000)
  }
  loop();

  function turno_activo() {
    $.ajax({
      url: "estados.php",
      type: "GET",
      data: {
        accion: 'turno_activo'
      },
      success: function(response) {
        let json = JSON.parse(response);
        let turno = json.turno_activo[0];
        $('#marcoturno').html(turno.turno);
      }
    })
  }
  document.getElementById("icongpo").innerHTML = `<i class="fas fa-sun" style="color:coral; font-size:1.5em;"></i>`;

  function estado_bloqueo() {
    $.ajax({
      url: "estados.php",
      type: "GET",
      data: {
        accion: 'bloqueo_activo'
      },
      success: function(response) {
        let json = JSON.parse(response);
        let bloqueo = parseInt(json.bloqueo);
        if (bloqueo === 1 && estado_de_bloqueo === 0) {
          estado_de_bloqueo = 1;

          $("#estadoVis").css({
            'background-color': 'red',
            'color': 'white',
            'padding': '5px',
            'text-transform': 'uppercase',
            'text-aling': 'center',
            'font-size': '1.5em'
          }).html("Bloqueado");

          Swal.fire(
            'Se bloquearon las entradas',
            'Recuerda verificar siempre el estado de bloqueos',
            'success'
          );
        } else if (bloqueo === 0 || estado_de_bloqueo === 1) {
          estado_de_bloqueo = 0;
          $("#numero").removeAttr('disabled');
          $("#premio").removeAttr('disabled');
          $("#btnEnviar").removeAttr('disabled');
          $("#estadoVis").css({
            'background-color': 'green',
            'color': 'white',
            'padding': '5px',
            'text-transform': 'uppercase',
            'text-aling': 'center',
            'font-size': '1.5em'
          }).html("Desbloqueado");
        }
      }
    });
  }
</script>

<?php
require('includes/footer.php');
?>