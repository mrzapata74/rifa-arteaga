<?php
include('conexion.php');

$json = [
    "error" => "no data"
];

if (isset($_POST['user_id'])) {
    $user_id = $_POST['user_id'];
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);

    $comando = $pdo->prepare("UPDATE usuarios SET password = :password WHERE id = :user_id");
    $comando->bindParam(':user_id', $user_id);
    $comando->bindParam(':password', $password);
    $comando->execute();

    $json = [
        "message" => 'Contraseña actualizada'
    ];
}

echo json_encode($json);
