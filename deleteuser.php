<?php
include('conexion.php');

function removeFile(string $path)
{
    $uploadPath = getcwd() . $path;
    unlink($uploadPath);
}

$base_path = '/img/users/';

$json = [
    "error" => "no data"
];

if (isset($_POST['user_id'])) {
    $user_id = $_POST['user_id'];

    $comando = $pdo->prepare("SELECT imagen FROM usuarios WHERE id = :user_id");
    $comando->bindParam(':user_id', $user_id);
    $comando->execute();

    $user_image = $comando->fetchAll(PDO::FETCH_ASSOC);
    if ($user_image) {
        $image_name = $user_image[0]['imagen'];

        $comando = $pdo->prepare("DELETE FROM usuarios WHERE id = :user_id");
        $comando->bindParam(':user_id', $user_id);
        $comando->execute();

        removeFile($base_path . $image_name);
        $json = [
            "message" => "Usuario removido correctamente"
        ];
    } else {
        $json = [
            "error" => "no valid user id"
        ];
    }
};

echo json_encode($json);
