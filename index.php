<?php
session_start();
include_once('conexion.php');
if (isset($_SESSION['user_id'])) {
    header('Location: home.php');
} else {
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arqueo de Ventas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" href="css/estilos.css?v=index_001">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    
</head>

<div class="container">
    <div class="card card-body" id="loginCard">


        <?php if (!empty($mensaje)) : ?>
            <p><?= $mensaje ?></p>
        <?php endif; ?>

        

        <div class="logo">
            <img src="img/logoArteaga.webp?v=3" alt="Logotipo">
        </div>

        <form id="frmlogin">
            <div class="input-group mb-2" id="wrapuser">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="ace-icon fa fa-user"></i></div>
                </div>
                <input type="text" name="usuario" class="form-control" id="usuario" placeholder="Ingresa tu Usuario">
            </div>


            <div class="input-group mb-2" id="wrappass">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="ace-icon fa fa-lock"></i></div>
                </div>
                <input type="password" name="password" class="form-control" placeholder="Ingresa tu Contraseña" id="password">
            </div>
            <div id="aviso"></div>

            <button type="submit" id="btnIngresar" class="btn btn-primary ">Ingresar</button>
        </form>

    </div>
</div>

<?php
require('includes/footer.php');
?>