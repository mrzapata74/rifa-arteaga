<?php
session_start();
if (!isset($_SESSION['user_id']))
    header('Location: /index.php');
include('conexion.php');
require('includes/header.php');

?>

<h3 id="titleRegister">Registro de Usuario</h3>

<div class="container">
    <div class="card card-body" id="NewUser">


        <?php if (!empty($mensaje)) : ?>
            <p><?= $mensaje ?></p>
        <?php endif; ?>

        <form id="frmregistro">
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingresa tu Nombre" required>
            </div>
            <div class="form-group">
                <label for="cedula">Cédula</label>
                <input type="text" name="cedula" class="form-control" id="cedula" placeholder="Ingresa tu Cédula" required>
            </div>
            <div class="form-group">
                <label for="celular">N° Celular</label>
                <input type="text" name="celular" class="form-control" id="celular" placeholder="Ingresa tu Número Celular" required>
            </div>
            <div class="form-group">
                <label for="correo">Correo</label>
                <input type="email" name="correo" class="form-control" id="correo" placeholder="Ingresa tu Correo" required>
            </div>
            <div class="form-group">
                <label for="direccion">Dirección</label>
                <textarea name="direccion" id="direccion" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="usuario">Usuario</label>
                <input type="text" name="usuario" class="form-control" placeholder="Ingresa tu Usuario" id="usuario" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="text" name="password" class="form-control"  placeholder="Ingresa el Password" id="password" required>
            </div>
            <div class="form-group">
                <label for="tipoUsuario">Tipo</label>
                <select class="form-control" id="tipoUsuario">
                    <option value="1">Administrador</option>
                    <option value="2">Vendedor</option>

                </select>
            </div>
            <button type="submit" class="btn btn-primary btn-lg">Ingresar</button>
        </form>
    </div>
</div>

<?php
require('includes/footer.php');
?>