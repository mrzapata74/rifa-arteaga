<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1))
    header('Location: /index.php');

include('conexion.php');
require('includes/header.php');

?>

<div class="container" id="wraphome">
    <h4 id="tituloDetalleVentas">Detalle de bloqueos</h4>
    <input type="text" class="contanier form-control text-center" name="rangepicker" id="calendario" />
    <br>
    <table id="rptDeVtas" class="display responsive nowrap table table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Fecha</th>
                <th>Usuario</th>
                <th>Turno</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        var dateInputFormat = 'YYYY-MM-DD HH:mm:ss';
        var dateRender = function(url, type, full) {
            var formated = moment(url, dateInputFormat);
            return (formated.isValid() ? formated.format('dddd DD MMMM YYYY | hh:mm:ssa') : url);
        };

        // Date picker
        var date_range = $('input[name="rangepicker"]').daterangepicker({
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Rango Personalizado",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
        }, function(start, end, label) {
            table.ajax.reload();
            console.log(start, end, label);
        });
        var table = $('#rptDeVtas').DataTable({
            fixedHeader: true,
            language: {
                "lengthMenu": "Ver _MENU_ Archivos por Página",
                "zeroRecords": "Lo sentimos, no tenemos resultados",
                "info": "Mostrando Página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin Registros para Mostrar",
                "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
                processing: "Cargando, esto podria tomar unos segundos....",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                },
                "search": "Buscar",
                buttons: {
                    colvisRestore: 'Restaurar',
                }
            },
            processing: true,
            aServerSide: true,
            dom: 'Blfrtip',
            buttons: [{
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel'
                },
                {
                    extend: 'pdf',
                    text: 'PDF'
                },
                {
                    extend: 'print',
                    text: 'Imprimir',
                    messageTop: '<h3 class="text-center">DETALLE DE VENTAS POR VENDEDOR Y FECHA</h3>',
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '12pt')
                            .prepend(
                                '<img src="https://www.mzdevocotal.com/rifaarteaga/img/logoArInforme.png" style="position:absolute; top:0; left:0; opacity:0.8; width:400px; height:250px;" />'
                            );

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                },
                {
                    extend: 'colvis',
                    text: 'Ver/Ocultar',
                    postfixButtons: ['colvisRestore']
                }
            ],
            responsive: {
                details: {
                    type: 'column'
                }
            },
            bDestroy: true,
            iDisplayLength: 10,
            order: [
                [1, "asc"]
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Todos"]
            ],
            ajax: {
                url: 'obtHistorialBloqueo.php',
                type: "POST",
                dataType: "json",
                error: function(e) {
                    console.log(e.responseText);
                },
                data: function(d) {
                    let date_input_format = 'YYYY-MM-DD';
                    var start_date = date_range.data('daterangepicker').startDate.format(date_input_format);
                    var end_date = date_range.data('daterangepicker').endDate.format(date_input_format);

                    console.log(start_date, end_date);

                    d.start_date = start_date;
                    d.end_date = end_date;
                }
            },
            columns: [
                // Responsive
                {
                    className: 'control noVis notexport',
                    data: null,
                    orderable: false,
                    defaultContent: '',
                    width: "5%"
                },
                // bloqueo_date
                {
                    name: 'bloqueo_date',
                    data: 'bloqueo_date',
                    render: dateRender,
                    width: "5%"
                },
                // Nombre
                {
                    name: 'nombre',
                    data: 'nombre'
                },
                // Turno
                {
                    name: 'turno',
                    data: 'turno',
                    className: 'aligtxtc'
                },
                // bloqueo_accion
                {
                    name: 'bloqueo_accion',
                    data: 'bloqueo_accion',
                    className: 'aligtxtc',
                    render: function(url, type, full) {
                        let estado = url;
                        return (estado == 1 ? 'Entradas bloqueadas' : 'Entradas desbloqueadas');
                    }
                }
            ]
        });
    });
</script>


<?php
require('includes/footer.php');
?>