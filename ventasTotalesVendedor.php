<?php
session_start();

if (!isset($_SESSION['user_id']) || (isset($_SESSION['rol']) && $_SESSION['rol'] != 1))
    header('Location: /index.php');

include('conexion.php');
require('includes/header.php');

// SQL Query
$comando = $pdo->prepare("SELECT id, nombre FROM listausuarios");
$comando->execute();

// array data 
$user_list = $comando->fetchAll(PDO::FETCH_ASSOC);

// SQL Query
$comando = $pdo->prepare("SELECT idturno, turno FROM turnos");
$comando->execute();

// array data 
$lista_turno = $comando->fetchAll(PDO::FETCH_ASSOC);

?>

<div class="container" id="wraphome">
    <h4 id="tituloDetalleVentas">Consolidado de Venta por vendedor</h4>
    <form class="form-inline">
        <div class="form-group mb-2" id="selUsers">
            <label for="user_select">
                <i class="fas fa-user"></i> Usuario
            </label>
            <select class="form-control form-control-sm" id="user_select">
                <option value="-1" selected>Todos</option>
                <?php
                foreach ($user_list as $user) {
                    $user_id = $user['id'];
                    $user_name = $user['nombre'];
                    echo "<option value=\"$user_id\">$user_name</option>\r\n";
                }
                ?>
            </select>
            <label for="turno_select">
                <i class="fas fa-user"></i> Turno
            </label>
            <select class="form-control form-control-sm" id="turno_select">
                <?php
                foreach ($lista_turno as $turno) {
                    $turno_id = $turno['idturno'];
                    $turno_name = $turno['turno'];
                    echo "<option value=\"$turno_id\">$turno_name</option>\r\n";
                }
                ?>
            </select>
        </div>
    </form>
    <input type="text" class="contanier form-control text-center" name="rangepicker" id="calendario" />
    <br>
    <div class="container">
        <h4>Numero de Referencia</h4>
        <div class="row">
            <div class="col-6">
                <input type="number" class="contanier form-control text-center" name="numero_ganador" id="numero_ganador" />
            </div>
            <div class="col-6">
                <button type="button" class="btn-sm btn-info col" id="reloadTable"><i class="fas fa-sync"></i> Actualizar reporte</button>
            </div>
        </div>
        <br>
    </div>
    <table id="rptDeVtas" class="display responsive nowrap table table-striped cell-border hover">
        <thead>
            <tr>
                <th></th>
                <th>Nombre</th>
                <th>Precio Venta</th>
                <th>Recaudado C$</th>
                <th>Recaudado C$ (G)</th>
                <th>Restante</th>
                <th>Monto</th>
                <th>Monto (G)</th>
                <th>Restante</th>
            </tr>
        </thead>
        <tbody></tbody>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>

<script type="text/javascript">
    var date_range = false;
    $(function() {
        var numberFormat = $.fn.dataTable.render.number(',', '.', 2, 'C$').display;

        $("#user_select").change(function() {
            table.ajax.reload();
        });
        $("#turno_select").change(function() {
            table.ajax.reload();
        });
        $("#reloadTable").click(function() {
            table.ajax.reload();
        });

        // Date pick
        date_range = $('input[name="rangepicker"]').daterangepicker({
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Rango Personalizado",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
        }, function(start, end, label) {
            table.ajax.reload();
            console.log(start, end, label);
        });

        var table = $('#rptDeVtas').DataTable({
            fixedHeader: true,
            searching: true,
            language: {
                "lengthMenu": "Ver _MENU_ Archivos por Página",
                "zeroRecords": "Lo sentimos, no tenemos resultados",
                "info": "Mostrando Página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin Registros para Mostrar",
                "infoFiltered": "(Filtrado de un Total de _MAX_ Registros)",
                processing: "Cargando, esto podria tomar unos segundos....",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                },
                "search": "Buscar",
                buttons: {
                    colvisRestore: 'Restaurar',
                }
            },
            processing: true,
            aServerSide: true,
            dom: 'Blfrtip',
            buttons: [{
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel',
                    messageTop: function() {
                        let message = `${$('#numero_ganador').val() ? `Numero ganador: ${$('#numero_ganador').val()}` : ''} Turno: ${$('#turno_select option:selected').text()}`;

                        return message;
                    },
                    messageBottom: function() {
                        return `${date_range.data('daterangepicker').startDate.format('DD/MM/YYYY')} - ${date_range.data('daterangepicker').endDate.format('DD/MM/YYYY')}`;
                    },
                    footer: true,
                    filename: `*_REPORTE_VENTAS_${moment().format('DD_MM_YYYY')}`,
                    sheetName: `Ventas`,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    text: 'PDF',
                    download: 'open',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    text: 'Imprimir',
                    footer: true,
                    messageTop: `<h3 class="text-center">CONSOLIDADO DE VENTAS POR VENDEDOR</h3><br><extra_area></extra_area>`,
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '12pt')
                            .prepend();

                        $(win.document.body).find('extra_area')
                            .append(`
                            ${$('#numero_ganador').val() ? `<b>Numero ganador:</b> ${$('#numero_ganador').val()}` : ''}<br>
                            <b>Turno:</b> ${$('#turno_select option:selected').text()}<br>
                            <b>Fecha del reporte:</b> ${moment().format('LLLL')}<br>
                            <br><br>
                            <hr>
                            <h5 class="text-center">Datos de: ${date_range.data('daterangepicker').startDate.format('DD/MM/YYYY')} - ${date_range.data('daterangepicker').endDate.format('DD/MM/YYYY')}</h5><br>
                            `);

                        $(win.document.body).find('div')
                            .addClass('compact')
                            .css('font-size', 'inherit');

                    }
                },
                {
                    extend: 'colvis',
                    text: 'Ver/Ocultar',
                    postfixButtons: ['colvisRestore']
                }
            ],
            responsive: {
                details: {
                    type: 'column'
                }
            },
            bDestroy: true,
            iDisplayLength: -1,
            order: [
                [1, "asc"]
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "Todos"]
            ],
            ajax: {
                url: 'ajax/ventasTotalesVendedor.php',
                type: "POST",
                dataType: "json",
                error: function(e) {
                    console.log(e.responseText);
                },
                data: function(d) {
                    let date_input_format = 'YYYY-MM-DD';
                    var start_date = date_range.data('daterangepicker').startDate.format(date_input_format);
                    var end_date = date_range.data('daterangepicker').endDate.format(date_input_format);

                    var user_id = $('#user_select').val();
                    var turno_id = $('#turno_select').val();
                    var numero_ganador = $('#numero_ganador').val() || -1;

                    d.start_date = start_date;
                    d.end_date = end_date;
                    d.user_id = user_id;
                    d.turno_id = turno_id;
                    d.numero = numero_ganador;
                }
            },
            columns: [
                // 0 | Responsivo
                {
                    className: 'control noVis notexport',
                    data: null,
                    orderable: false,
                    defaultContent: '',
                    width: "5%"
                },
                // Nombre vendedor
                {
                    name: 'nombre',
                    data: 'nombre_vendedor'
                },
                // Precio de venta
                {
                    name: 'precio_venta',
                    data: 'precio_venta',
                    visible: true,
                    render: numberFormat
                },
                // Recaudado
                {
                    name: 'recaudado',
                    data: 'recaudado',
                    render: numberFormat
                },
                // Recaudado Ganador
                {
                    name: 'recaudado_ganador',
                    data: 'recaudado_ganador',
                    visible: false,
                    render: numberFormat
                },
                // Recaudado restante
                {
                    name: 'recaudado_restante',
                    data: function(row, type, val, meta) {
                        let restante = (row.recaudado - row.recaudado_ganador);
                        return restante;
                    },
                    render: numberFormat,
                    visible: false
                },
                // Premio
                {
                    name: 'premio',
                    data: 'premio',
                    render: numberFormat,
                    visible: false
                },
                // Premio Ganador
                {
                    name: 'premio_ganador',
                    data: 'premio_ganador',
                    render: numberFormat
                },
                // Premio restante
                {
                    name: 'premio_restante',
                    data: function(row, type, val, meta) {
                        let restante = (row.premio - row.premio_ganador);
                        return restante;
                    },
                    render: numberFormat,
                    visible: false
                }
            ],
            footerCallback: function(row, data, start, end, display) {
                let colnames = ['premio:name', 'recaudado:name', 'recaudado_ganador:name', 'recaudado_restante:name', 'premio_restante:name', 'premio_ganador:name'];
                var api = this.api(),
                    data;

                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                colnames.forEach(colname => {
                    // Se guarda el objeto de la columna segun el selector en el bucle; se seleccionan unicamente los que tenga un filtro aplicado
                    let column = api.column(colname, {
                        filter: 'applied'
                    });
                    // Total over all pages
                    total = column
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(column.footer()).html(
                        numberFormat(total)
                    );

                });

            }
        });
    });
</script>


<?php
require('includes/footer.php');
?>